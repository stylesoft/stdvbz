<?php
/**
 * The application configuration file
 *
 * This application configuration files contain settings specific to an application
 *
 * LICENSE:
 *
 * @copyright  2012 Monara IT UK Ltd
 * @license    http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    PlusPro V4
 * @since      File available since Release 1.0.0
 */


/* 
$_DB_SERVER         = "localhost";
$_DB_USER           = "penningt_pluspro";
$_DB_PASSWORD       = "pluspro";
$_DB_NAME           = "penningt_db";
 */

$_DB_SERVER         = "localhost";
$_DB_USER           = "root";
$_DB_PASSWORD       = "root";
$_DB_NAME           = "db_stdvibz"; 

$_DOC_ROOT          = $_SERVER['DOCUMENT_ROOT'].'/stdvbz';

$_DISPLAY_ERRORS    = false; // set true or false, True - The system will dispay all the errors


 
$_JS_LIBS_PATH      = "https://secure.pluspro.com/globaladmin2/jlibs/js/";


// the path for the global contents such as css and images
$_GLOBAL_ADMIN_CONTENT_PATH = "https://secure.pluspro.com/globaladmin2/";


// the variable to display the pagination data - admin panel
$_ADMIN_RECORDS_PER_PAGE  = 20;

// the variable to display the pagination data - front-end
$_FRONT_RECORDS_PER_PAGE  = 20;


//=============== Global DB Settings========================================

$_GLOBAL_DB_SERVER         = "localhost";
$_GLOBAL_DB_USER           = "root";
$_GLOBAL_DB_PASSWORD       = "root";
$_GLOBAL_DB_NAME           = "andrewwilkinsonslandscapes.com";//"pluspro_restaurants";//pluspro_restaurants";//pluspro_restaurants";

define('GLOBAL_DB_SERVER', $_GLOBAL_DB_SERVER);
define('GLOBAL_DB_USER', $_GLOBAL_DB_USER);
define('GLOBAL_DB_PASSWORD', $_GLOBAL_DB_PASSWORD);
define('GLOBAL_DB_NAME', $_GLOBAL_DB_NAME);
//======================================================
?>
