-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 29, 2014 at 05:06 PM
-- Server version: 5.5.35
-- PHP Version: 5.3.10-1ubuntu3.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_stdvibz`
--

-- --------------------------------------------------------

--
-- Table structure for table `index_page`
--

CREATE TABLE IF NOT EXISTS `index_page` (
  `ID` tinyint(3) NOT NULL AUTO_INCREMENT,
  `TITLE` tinytext CHARACTER SET latin1,
  `KEYWORDS` tinytext CHARACTER SET latin1,
  `DESCRIPTION` tinytext CHARACTER SET latin1,
  `BODY` text CHARACTER SET latin1,
  `NAME` text CHARACTER SET latin1,
  `content_type` varchar(35) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `index_page`
--

INSERT INTO `index_page` (`ID`, `TITLE`, `KEYWORDS`, `DESCRIPTION`, `BODY`, `NAME`, `content_type`) VALUES
(1, 'Welcome to Foxy Butlers,', 'Welcome to Foxy Butlers,', 'Welcome to Foxy Butlers,', '<p>Come to Tiny Toes Playcentre! A Playcentre with a difference. We are aware that adults want to come and relax in a child orientated environment yet have access to quality food and drink. We have put alot of thought into our menus. For example, our 5 hidden veg pasta is a hit with the kids and we have a good range of food for the adults such as home made pizzas and fresh salads.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Not forgetting fun for the kids. As well as a fantastic playframe, ball pool, ball boggler and toys &ndash; we have lots going on! Songtimes with Sammy Snake, Arts &amp; Craft, Resident children&rsquo;s hairdresser, Saturday morning Dad&rsquo;s sessions and the infamous Tiny Toes Disco.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Tiny Toes has a range of parties to suit all needs and we are flexible about how you would like your party for your child. Our parties start from &pound;99 for 12 children. This includes play, food and a disco!!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>For party bags Tiny Toes has teamed up with Deb&rsquo;s Delights to ensure you get the best deal possible. Please click on the link for more details.</p>\r\n', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `index_pages`
--

CREATE TABLE IF NOT EXISTS `index_pages` (
  `ID` tinyint(3) NOT NULL AUTO_INCREMENT,
  `TITLE` text,
  `KEYWORDS` text,
  `DESCRIPTION` text,
  `BODY` longblob,
  `ListingID` tinyint(4) DEFAULT NULL,
  `NAME` mediumtext,
  `IMAGE_TXT` text NOT NULL,
  `LIVE` text,
  `ISCONTACT` tinytext,
  `ALLOWDELETE` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `ID` tinyint(3) NOT NULL AUTO_INCREMENT,
  `TITLE` text,
  `KEYWORDS` text,
  `DESCRIPTION` text,
  `BODY` longblob,
  `ListingID` tinyint(4) DEFAULT NULL,
  `NAME` mediumtext,
  `LIVE` text,
  `ISCONTACT` tinytext,
  `ALLOWDELETE` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

-- --------------------------------------------------------

--
-- Table structure for table `rln_stdcategory_interest`
--

CREATE TABLE IF NOT EXISTS `rln_stdcategory_interest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `value` char(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rln_stdsupport_list`
--

CREATE TABLE IF NOT EXISTS `rln_stdsupport_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `support_id` int(11) NOT NULL,
  `value` char(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sec_sub_pages`
--

CREATE TABLE IF NOT EXISTS `sec_sub_pages` (
  `ID` tinyint(3) NOT NULL AUTO_INCREMENT,
  `TITLE` text,
  `KEYWORDS` text,
  `DESCRIPTION` text,
  `BODY` longblob,
  `ListingID` tinyint(4) DEFAULT NULL,
  `NAME` mediumtext,
  `LIVE` text,
  `ISCONTACT` tinytext,
  `ALLOWDELETE` tinyint(2) DEFAULT '1',
  `MAIN_PAGE_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `ID` tinyint(2) NOT NULL AUTO_INCREMENT,
  `URL` tinytext CHARACTER SET latin1,
  `SITE_NAME` tinytext CHARACTER SET latin1,
  `SITE_EMAIL` tinytext CHARACTER SET latin1,
  `TEMPLATE` text CHARACTER SET latin1,
  `SIDEBOX` tinyint(4) DEFAULT NULL,
  `BASEURL` text,
  `NEEDPING` int(4) DEFAULT '0',
  `facebook_app_id` varchar(65) DEFAULT NULL,
  `facebook_secret_key` varchar(65) DEFAULT NULL,
  `google_alytics_code` varchar(1000) DEFAULT NULL,
  `consumer_key` varchar(250) DEFAULT NULL,
  `consumer_secret` varchar(250) DEFAULT NULL,
  `user_token` varchar(250) DEFAULT NULL,
  `user_secret` varchar(250) DEFAULT NULL,
  `terms_conditions` varchar(250) DEFAULT NULL,
  `privacy_policy` varchar(250) DEFAULT NULL,
  `max_num_pages` int(11) DEFAULT NULL,
  `pluspro_logo_text` varchar(255) DEFAULT NULL,
  `logo` varchar(150) DEFAULT NULL,
  `favIcon` varchar(150) DEFAULT NULL,
  `homeBanner_height` int(11) DEFAULT NULL,
  `homeBanner_width` int(11) DEFAULT NULL,
  `mobile_site` enum('Yes','No') DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`ID`, `URL`, `SITE_NAME`, `SITE_EMAIL`, `TEMPLATE`, `SIDEBOX`, `BASEURL`, `NEEDPING`, `facebook_app_id`, `facebook_secret_key`, `google_alytics_code`, `consumer_key`, `consumer_secret`, `user_token`, `user_secret`, `terms_conditions`, `privacy_policy`, `max_num_pages`, `pluspro_logo_text`, `logo`, `favIcon`, `homeBanner_height`, `homeBanner_width`, `mobile_site`) VALUES
(1, 'http://localhost/stdvbz/admin/', 'my site studentvibes', 'salman.prs@gmail.com', 'studentvibes', 0, 'http://localhost/stdvbz/', 0, 'ab api id - edited2', '', 'google code ', '', '', '', '', 'feed.xml', 'sitemap.xml', 100, 'zxczxczxczxczxczxc', '1380878203.png', '1359681976.ico', 312, 549, '');

-- --------------------------------------------------------

--
-- Table structure for table `set_biz_category`
--

CREATE TABLE IF NOT EXISTS `set_biz_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(35) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `status` enum('Enabled','Disabled') NOT NULL DEFAULT 'Enabled',
  `lang` enum('en','sv') NOT NULL DEFAULT 'en',
  `created_on` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `last_modified_on` datetime DEFAULT NULL,
  `last_modified_by` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_edu_institute`
--

CREATE TABLE IF NOT EXISTS `set_edu_institute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `last_modified_on` datetime DEFAULT NULL,
  `last_modified_by` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_mail_template`
--

CREATE TABLE IF NOT EXISTS `set_mail_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `from_email` varchar(255) DEFAULT NULL,
  `from_name` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `content` text,
  `enabled` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_study_level`
--

CREATE TABLE IF NOT EXISTS `set_study_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `institute` varchar(150) DEFAULT NULL,
  `level` varchar(150) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `lang` enum('en','sv') NOT NULL DEFAULT 'en',
  `created_on` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `last_modified_on` datetime DEFAULT NULL,
  `last_modified_by` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `set_support_list`
--

CREATE TABLE IF NOT EXISTS `set_support_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(35) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `status` enum('Enabled','Disabled') NOT NULL DEFAULT 'Enabled',
  `lang` enum('en','sv') NOT NULL DEFAULT 'en',
  `created_on` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `last_modified_on` datetime DEFAULT NULL,
  `last_modified_by` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sys_study_area`
--

CREATE TABLE IF NOT EXISTS `sys_study_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(150) DEFAULT NULL,
  `program` varchar(150) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `lang` enum('en','sv') NOT NULL DEFAULT 'en',
  `created_on` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `last_modified_on` datetime DEFAULT NULL,
  `last_modified_by` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contactpage`
--

CREATE TABLE IF NOT EXISTS `tbl_contactpage` (
  `ID` tinyint(3) NOT NULL AUTO_INCREMENT,
  `TITLE` text,
  `KEYWORDS` text,
  `DESCRIPTION` text,
  `BODY` longblob,
  `ListingID` tinyint(4) DEFAULT NULL,
  `NAME` mediumtext,
  `LIVE` text,
  `ISCONTACT` tinytext,
  `ALLOWDELETE` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_contactpage`
--

INSERT INTO `tbl_contactpage` (`ID`, `TITLE`, `KEYWORDS`, `DESCRIPTION`, `BODY`, `ListingID`, `NAME`, `LIVE`, `ISCONTACT`, `ALLOWDELETE`) VALUES
(1, 'how to find us', 'Contact Us', 'Contact Us', 0x3c703e426173656420696e2074686520736f75746820656173742c205365637572697479204d617374657273206f666665722061206e6174696f6e77696465206e6574776f726b206f66207265616374697665206c6f636b736d6974687320616e6420736563757269747920696e7374616c6c6572732e3c2f703e0d0a, 0, 'Contact us', 'Published', 'Yes', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_modules`
--

CREATE TABLE IF NOT EXISTS `tbl_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(50) DEFAULT NULL,
  `Is_Enabled` enum('Yes','No') DEFAULT NULL,
  `module_label` varchar(35) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `tbl_modules`
--

INSERT INTO `tbl_modules` (`id`, `module_name`, `Is_Enabled`, `module_label`) VALUES
(1, 'cms', 'Yes', 'PlusPro CMS'),
(13, 'registration', 'Yes', 'Registration');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_module_links`
--

CREATE TABLE IF NOT EXISTS `tbl_module_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_link_name` varchar(50) DEFAULT NULL,
  `Is_Enabled` enum('Yes','No') DEFAULT NULL,
  `module_link_label` varchar(35) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `tbl_module_links`
--

INSERT INTO `tbl_module_links` (`id`, `module_link_name`, `Is_Enabled`, `module_link_label`, `module_id`, `display_order`) VALUES
(1, 'add_a_page', 'Yes', 'Add a Page', 1, 1),
(2, 'edit_pages', 'Yes', 'Edit Pages', 1, 2),
(3, 'view_all_phrases', 'Yes', 'View all Phrases', 2, 1),
(4, 'add_phrase', 'Yes', 'Add Phrase', 2, 2),
(5, 'view_all_news', 'Yes', 'View All News', 3, 1),
(6, 'add_news', 'Yes', 'Add News', 3, 2),
(7, 'view_galleries', 'Yes', 'View Galleries', 4, 1),
(8, 'add_gallery', 'Yes', 'Add Gallery', 4, 2),
(9, 'view_all_menus', 'Yes', 'View All Menus', 5, 1),
(10, 'add_new_menu', 'Yes', 'Add New Menu', 5, 2),
(11, 'side_bar_contents', 'Yes', 'Side Bar Contents', 7, 1),
(12, 'add_side_bar_contents', 'Yes', 'Add Side Bar Contents', 7, 2),
(13, 'site_contents', 'Yes', 'Site Contents', 7, 3),
(14, 'add_site_contents', 'Yes', 'Add Site Contents', 7, 4),
(15, 'view_all_home_banners', 'Yes', 'View All Home Banners', 7, 5),
(16, 'add_banner', 'Yes', 'Add Banner', 7, 6),
(17, 'product_listing', 'Yes', 'View All Products', 8, 1),
(18, 'add_product', 'Yes', 'Add New Product', 8, 2),
(19, 'view_all_categories', 'Yes', 'View All Categories', 8, 3),
(20, 'add_new_category', 'Yes', 'Add New Category', 8, 4),
(21, 'side_bar_contents_header', 'Yes', 'Header', 7, 1),
(22, 'side_bar_contents_middle_left', 'Yes', 'Middle Left', 7, 2),
(23, 'side_bar_contents_middle_right', 'Yes', 'Middle Right', 7, 3),
(24, 'side_bar_contents_footer', 'Yes', 'Footer', 7, 4),
(25, 'opening_times', 'Yes', 'Opening Times', 7, 5),
(26, 'view_all_orders', 'Yes', 'View all orders', 9, 1),
(27, 'special_offers', 'Yes', 'Special Offers', 8, 3),
(28, 'featured_products', 'Yes', 'Featured Products', 8, 4),
(29, 'manage_customer', 'Yes', 'Manage Customer', 10, 1),
(30, 'View Testimonial', 'Yes', 'View Testimonial', 11, 2),
(31, 'manage_testimonial', 'Yes', 'manage_testimonial', 11, 2),
(32, 'add_new_testimonial', 'Yes', 'add_new_testimonial', 11, 1),
(33, 'registor', 'Yes', 'Registor', 13, 1),
(34, 'view_registration', 'Yes', 'view_registration', 13, 2),
(35, 'manage_time', 'Yes', 'manage_time', 14, 1),
(36, 'product_listing', 'Yes', 'View All Products', 15, 1),
(37, 'add_product', 'Yes', 'Add New Product', 15, 2),
(38, 'view_all_categories', 'Yes', 'View All Categories', 15, 3),
(39, 'add_new_category', 'Yes', 'Add New Category', 15, 4),
(40, 'add_blog', NULL, 'Add blog', 16, 1),
(41, 'edit_blog', 'Yes', 'Edit blog', 16, 2),
(42, 'edit_comment', 'Yes', 'Edit Comment ', 16, 4),
(43, 'add_comment', 'Yes', 'Add comment ', 16, 3),
(44, 'view_all_service', 'Yes', 'view_all_service', 17, 1),
(45, 'add_service', 'Yes', 'add_service', 17, 2),
(46, 'view_all_categories', 'Yes', 'View All Categories', 15, 1),
(47, 'add_new_category', 'Yes', 'Add New Category', 15, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_site_contact_info`
--

CREATE TABLE IF NOT EXISTS `tbl_site_contact_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_name` varchar(120) CHARACTER SET latin1 DEFAULT NULL,
  `address_line_1` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `address_line_2` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `address_line_3` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `contact_number_1` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `contact_number_2` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `google_map_code` text COLLATE utf8_bin,
  `contact_type` enum('CONTACT_1','CONTACT_2') CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_site_contact_info`
--

INSERT INTO `tbl_site_contact_info` (`id`, `contact_name`, `address_line_1`, `address_line_2`, `address_line_3`, `contact_number_1`, `contact_number_2`, `google_map_code`, `contact_type`) VALUES
(1, '', 'info@security-masters.co.uk', 'Based in the south east, Security Masters offer a nationwide network of reactive locksmiths and security installers. ', '0208 8502889', 'Po Box 61661, Londonssssssss dsds ', '020 88508356', 'https://maps.google.com/maps?q=SE9+9AP+map&ie=UTF8&hq=&hnear=SE9+9AP,+United+Kingdom&t=m&z=14&ll=51.449793,0.052685&output=embed', 'CONTACT_1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_social_media`
--

CREATE TABLE IF NOT EXISTS `tbl_social_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `linkedIn` varchar(255) DEFAULT NULL,
  `googlePlus` varchar(255) DEFAULT NULL,
  `other` varchar(255) DEFAULT NULL,
  `pinterest` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_social_media`
--

INSERT INTO `tbl_social_media` (`id`, `facebook`, `twitter`, `linkedIn`, `googlePlus`, `other`, `pinterest`) VALUES
(1, 'zxczxc', 'zxcz', 'xczx', 'czxc', 'zxczxc', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trn_student_info`
--

CREATE TABLE IF NOT EXISTS `trn_student_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(150) DEFAULT NULL,
  `surname` varchar(150) DEFAULT NULL,
  `date_of_birth` varchar(250) DEFAULT NULL,
  `email` char(10) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `living_at` varchar(250) DEFAULT NULL,
  `edu_institute` varchar(250) DEFAULT NULL,
  `level_of_study` varchar(250) DEFAULT NULL,
  `area_of_study` varchar(250) DEFAULT NULL,
  `code` char(20) DEFAULT NULL,
  `status` enum('pending','active','suspend') NOT NULL,
  `lang` enum('en','sv') NOT NULL DEFAULT 'en',
  `created_on` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `last_modified_on` datetime DEFAULT NULL,
  `last_modified_by` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `member_id` smallint(11) NOT NULL AUTO_INCREMENT,
  `firstname` text CHARACTER SET latin1,
  `lastname` text CHARACTER SET latin1,
  `login` text CHARACTER SET latin1,
  `passwd` tinytext CHARACTER SET latin1,
  `isadmin` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`member_id`, `firstname`, `lastname`, `login`, `passwd`, `isadmin`) VALUES
(3, 'PlusPro', 'Support', 'support@pluspro.com', '8435f87baa86c5a8eb5adbcd15bcfe8f', 1),
(4, 'SuperAdmin', 'Pluspro', 'superadmin@pluspro.com', '8435f87baa86c5a8eb5adbcd15bcfe8f', 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
