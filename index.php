<?php

require_once 'bootstrap.php';

// page variables........
$pageId = '';
$pageType = '';
$pageTitle = '';
$pageKeywords = '';
$pageDescription = '';
$pageBody = '';
$pageListingID = '';
$pageName = '';
$pageIsLive = '';
$isContactBoxEnable = '';
$newsId = "";
$contentBanner = "";
$rightTopBanner = "";
$rightbottomBanner = "";
$bottomBanner = "";
$sideBarPageId = '';

// **** page objects ***************
$pageObject = new Page();
$homePageObject = new HomePage();
$subPageObject = new SubPage();
$selectedPageInfo = null;
$contactPageObject = new ContactPage();


if ($_GET) {

    if (isset($_GET['pageId'])) {
        $pageId = $_GET['pageId'];
    }


    if (isset($_GET['page_type'])) {
        $pageType = $_GET['page_type'];
    }


    if ($pageId) {
        $pageId = $_GET['pageId'];


        if ($pageType == 'mainPage' || $pageType == 'shoppingCart' || $pageType == 'registration' || $pageType == 'login' || $pageType == 'checkout' || $pageType == 'shipping-and-billing' || $pageType == 'payment-method' || $pageType == 'myaccount' || $pageType == 'myorder' || $pageType == 'paypal' || $pageType == 'wish_list' || $pageType == 'search') {
            $selectedPageInfo = $pageObject->getPage($pageId);
            $sideBarPageId = 'p' . $pageId;
        } else if ($pageType == 'contactUsPage') {
            $selectedPageInfo = $contactPageObject->getContactPage($pageId);
            $sideBarPageId = 'c' . $pageId;
        } elseif ($pageType == 'sub_pages') {
            $selectedPageInfo = $subPageObject->getSubPage($pageId);
        } elseif ($pageType == 'sec-sub-page') {
            $selectedPageInfo = $secSubPageObject->getSecSubPage($pageId);
        }
        $pageId = $selectedPageInfo->id;
        $pageTitle = $selectedPageInfo->title;
        $pageKeywords = $selectedPageInfo->keywords;
        $pageDescription = $selectedPageInfo->description;
        $pageBody = $selectedPageInfo->body;
        $pageListingID = $selectedPageInfo->listingId;
        $pageName = $selectedPageInfo->name;
        $pageIsLive = $selectedPageInfo->live;
        $isContactBoxEnable = $selectedPageInfo->isContact;
    }
} else {
    $pageId = 1;
    $pageType = 'homePage';
    $selectedPageInfo = $homePageObject->getHomePage($pageId);
    $pageId = $selectedPageInfo->id;
    $pageTitle = $selectedPageInfo->title;
    $pageKeywords = $selectedPageInfo->keywords;
    $pageDescription = $selectedPageInfo->description;
    $pageBody = $selectedPageInfo->body;
    $pageListingID = null; //$selectedPageInfo->listingId;
    $pageName = $selectedPageInfo->name;
    $pageIsLive = null; //$selectedPageInfo->live;
    $isContactBoxEnable = null; //$selectedPageInfo->isContact;

    $sideBarPageId = 'h' . $pageId;
}

$CONTENT = '';
if ($pageType == 'homePage') {
    $CONTENT = FRONT_LAYOUT_VIEW_PATH . "index/index.tpl.php";
} else {
    $CONTENT = FRONT_LAYOUT_VIEW_PATH . "index/default.tpl.php";
}
$LAYOUT = FRONT_LAYOUT_PATH . "default_layout.tpl.php";
require_once $LAYOUT;
?>