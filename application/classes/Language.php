<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Language extends Core_Database{
    
    public $id;
    public $lang_name;
    public $is_enabled;
    
    public function __construct() {
         try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }

    }
    
     /** '
     * @name         :   getLanguage
     * @param        :   id, is_enabled
     * @desc         :   The function is to get  Language
     * Added By      :   Gayan Chathuranga
     * Added On      :   07-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */
    public function getLanguage($id,$is_enabled){
        $objLang = new stdClass();
        try {
                $colums = '*';
                $where = 'id = ' . $id ." AND is_enabled = '$is_enabled'";
                $this->select($this->tb_name, $colums, $where);
                $langInfo = $this->getResult();

                $objLang->id = $langInfo['id'];
                $objLang->lang_name = $langInfo['lang_name'];


            return $objLang;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    /** '
     * @name         :   getLanguageName
     * @param        :   id, is_enabled
     * @desc         :   The function is to get  Language
     * Added By      :   Gayan Chathuranga
     * Added On      :   07-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */
    public function getLanguageName($id,$is_enabled){       
        try {
                $colums = '*';
                $where = 'id = ' . $id ." AND is_enabled = '$is_enabled'";
                $this->select($this->tb_name, $colums, $where);
                $langInfo = $this->getResult();
                
            return $langInfo['lang_name'];
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    /** '
     * @name         :   getAllLanguages
     * @param        :   
     * @desc         :   The function is to get all news details
     * @return       :   Array (Array Of Language Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   07-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllLanguages($is_enabled) {
        $arrLanguage = array();
        try {
                $colums = '*';
                $where = "is_enabled = '$is_enabled'";
                $orderBy = "lang_name Asc";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $langResult = $this->getResult();
                foreach ($langResult As $langRow) {
                    $langId = $langRow['id'];
                    $langInfo = $this->getLanguage($langId,$is_enabled);
                    array_push($arrLanguage, $langInfo);
                }
            return $arrLanguage;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
}
?>
