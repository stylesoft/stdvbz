<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Fields extends Core_Database {

    //news propoerties
    public $id;
    public $name;
    public $fieldValue;
    public $filedObjectType;
    
    

    //constructor
    public function Fields() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /*     * '
     * @name         :   addFields
     * @param        :   categoryObject
     * Description   :   The function is to category details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function addFields() {
        $recordId = null;
        try {
               
            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   editNews
     * @param        :   NewsObject
     * Description   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editFields() {
        $isUpdated = false;
        try {
                
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   deleteFields
     * @param        :   FieldsObject
     * Description   :   The function is to delete Fields details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deleteFields() {
        $isDeleted = false;
        try {
            
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getFields
     * @param        :   Integer (Fields ID)
     * Description   :   The function is to get a Fields details
     * @return       :   Fields Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getFields($currencyId) {
        $objFields = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' .$currencyId;
                $this->select('tbl_fields', $colums, $where);
                $fieldsInfo = $this->getResult();
                if($fieldsInfo){
                    $objFieldValue = new FieldValue();
                    $objFieldValue->tb_name = 'tbl_field_value';
                    $fieldId       = $fieldsInfo['id'];
                    $objFields->id = $fieldId;
                    $objFields->name = $fieldsInfo['name'];
                    $objFields->filedObjectType = $fieldsInfo['filed_object_type'];
                    $objFields->fieldValue = $objFieldValue->getFieldValueByFieldId($fieldId);
                } 
            }
            return $objFields;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getAll
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAll() {
        $arrFields = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = '';
                $orderBy = "id ASC";
                $this->select('tbl_fields', $colums, $where, $orderBy);
                $fieldsResult = $this->getResult();
                foreach ($fieldsResult As $fieldRow) {
                    $fieldId = $fieldRow['id'];
                    $fieldInfo = $this->getFields($fieldId);
                    array_push($arrFields,$fieldInfo);
                }
            }

            return $arrFields;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    
        /*     * '
     * @name         :   getAllByType
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllByType($filed_object) {
        $arrFields = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = "filed_object_type = '" . $filed_object . "'";
                $orderBy = "display_order ASC";
                $this->select('tbl_fields', $colums, $where, $orderBy);
                $fieldsResult = $this->getResult();
                foreach ($fieldsResult As $fieldRow) {
                    $fieldId = $fieldRow['id'];
                    $fieldInfo = $this->getFields($fieldId);
                    array_push($arrFields,$fieldInfo);
                }
            }

            return $arrFields;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

}
?>