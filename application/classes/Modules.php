<?php
/**
 * @name        Modules
 * @copyright   2012 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1
 * @author     	Iyngaran Iyathurai
 *              Developer -  Monara IT UK Ltd
 *              iyngaran.iyathurai@monara.com
 *
 */

class Modules extends Core_Database{


	public $id;
	public $moduleName;
	public $isEnabled;
	public $moduleLabel;
	public $displayOrder;
        public $subModules;



	/**'
	 * @name         :   addModules
	 * @param        :   ModulesObject
	 * Description   :   The function is to add a new page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function addModules($objModules){
		$recordId = null;
		try{
			if($this->connect()){
				$id 			  = $objModules->id;
				$moduleName       = $objModules->moduleName;
				$isEnabled        = $objModules->isEnabled;
				$moduleLabel      = $objModules->moduleLabel;
				$displayOrder     = $objModules->displayOrder;

				$inserted = $this->insert('tbl_modules',array($id,$moduleName,$isEnabled,$moduleLabel,$displayOrder));
				if($inserted){
					$recordId = $this->getLastInsertedId();
				}
			}
			return $recordId;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Modules</em>, <strong>Function -</strong> <em>addModules()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}


	/**'
	 * @name         :   editModules
	 * @param        :   ModulesObject
	 * Description   :   The function is to edit a page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function editModules($objModules){
		$isUpdated = false;
		try{
			if($this->connect()){
				$id 			  = $objModules->id;
				$moduleName       = $objModules->moduleName;
				$isEnabled        = $objModules->isEnabled;
				$moduleLabel      = $objModules->moduleLabel;
				$displayOrder     = $objModules->displayOrder;

				$arrayData        = array('module_name'=>$moduleName,
										'Is_Enabled'=>$isEnabled,
										'module_label'=>$moduleLabel,
										'display_order'=>$displayOrder);
				//$this->update(' tbl_modules',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("id = '" . $id . "'");
				$isUpdated = $this->update('tbl_modules',$arrayData,$arrWhere);
			}
			return $isUpdated;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Modules</em>, <strong>Function -</strong> <em>addModules()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}

	/**'
	 * @name         :   deleteModules
	 * @param        :   ModulesObject
	 * Description   :   The function is to add a new page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function deleteModules($objModules){
		$isDeleted    = false;
		try {
			if($this->connect()){
				$id 				= $objModules->id;
				$arrWhere  = array("id = '" . $id . "'");
				$isDeleted = $this->delete('tbl_modules',$arrWhere);
			}
			return $isDeleted;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Modules</em>, <strong>Function -</strong> <em>addModules()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}

	}


	/**'
	 * @name         :   getModules
	 * @param        :   Integer (Modules ID)
	 * Description   :   The function is to get a page details
	 * @return       :   Modules Object
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getModules($menuId){

		$objModules         = new stdClass();
		$objImage           = new Image();
                $objModuleLink      = new ModuleLink();
                
		try{
			if($this->connect()){
				$colums = '*';
				$where  = 'id = '.$menuId;
				$this->select('tbl_modules',$colums,$where);
				$menuInfo = $this->getResult();
				if(count($menuInfo)>0){
					$objModules->id 			= $menuInfo['id'];
					$objModules->moduleName 		= $menuInfo['module_name'];
					$objModules->isEnabled 			= $menuInfo['Is_Enabled'];
					$objModules->moduleLabel 		= $menuInfo['module_label'];  
                                        $moduleId                               = $menuInfo['id'];
                                        $objModules->subModules                 = $objModuleLink->getAllByModuleId($moduleId);
				}
			}
			return $objModules;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Modules</em>, <strong>Function -</strong> <em>getModules()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}

	}

	/**'
	 * @name         :   getAll
	 * @param        :
	 * Description   :   The function is to get all page details
	 * @return       :   Array (Array Of Modules Object)
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getAll(){
		$arrModuless       = array();
		try{
			if($this->connect()){
				$colums = 'id';
				$where  = null;
				$orderBy = " id Asc";
				$this->select('tbl_modules',$colums,$where,$orderBy);
				$menuRes = $this->getResult();
				foreach($menuRes As $menuRow){
					$menuId = $menuRow['id'];
					$menuInfo = $this->getModules($menuId);
					array_push($arrModuless,$menuInfo);
				}
					
			}
			return $arrModuless;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Modules</em>, <strong>Function -</strong> <em>getModules()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}





	/**'
	 * @name         :   getEnabledMenus
	 * @param        :
	 * Description   :   The function is to get all ebabled modules
	 * @return       :   Array (Array Of Modules Object)
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getEnabledMenus(){
		$arrModuless       = array();
		try{
			if($this->connect()){
			$SQL = "SELECT id FROM tbl_modules WHERE Is_Enabled = 'Yes' ORDER BY display_order Asc";
            $dbResult = $this->executeSelectQuery($SQL);
            $moduleRes = $this->getResult();
		            foreach ($moduleRes As $moduleRow) {
		                $moduleId = $moduleRow['id'];
		                $moduleInfo = $this->getModules($moduleId);
		                array_push($arrModuless, $moduleInfo);
		            }		
			}
			return $arrModuless;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Modules</em>, <strong>Function -</strong> <em>getModules()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}
	
	
	
		/**'
	 * @name         :   getEnabledMenus
	 * @param        :
	 * Description   :   The function is to get all ebabled modules
	 * @return       :   Array (Array Of Modules Object)
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function isEnabled($moduleName){
		$isEnabled = false;
		
		try{
			
			if($this->connect()){
			$SQL = "SELECT id FROM tbl_modules WHERE module_name = '".$moduleName."' AND Is_Enabled = 'Yes'";
                        $dbResult = $this->executeSelectQuery($SQL);
                        $moduleRes = $this->getResult();

                            if($moduleRes){
                                $isEnabled = true;
                            } else {
                                $isEnabled = false;
                            }
            
			}
			
			return $isEnabled;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Modules</em>, <strong>Function -</strong> <em>getModules()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}
	
        
        
        /**'
	 * @name         :   updateStatus
	 * @param        :   Module Object
	 * Description   :   The function is to edit a page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function updateStatus($objModule){
		$isUpdated = false;
		try{
			if($this->connect()){
				$id 				= $objModule->id;
				$isEnabled 			= $objModule->isEnabled;
				$arrayData          = array('Is_Enabled'=>$isEnabled);
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("id = '" . $id . "'");
				$isUpdated = $this->update('tbl_modules',$arrayData,$arrWhere);
			}
			return $isUpdated;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}
	
        
        
        
        /**'
	 * @name         :   disableAll()
	 * @param        :   Module Object
	 * Description   :   The function is to edit a page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function disableAll(){
		$isUpdated = false;
		try{
			if($this->connect()){
				$isEnabled 	    = 'No';
				$arrayData          = array('Is_Enabled'=>$isEnabled);
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("id != ''");
				$isUpdated = $this->update('tbl_modules',$arrayData,$arrWhere);
			}
			return $isUpdated;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}

}
?>
