<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class PhraseTranslation extends Core_Database{
    
    
    public $id; //phrase id in tbl_phrases
    public $fieldText;
    public $phrase_type;
    public $phrase_name;
    public $phrase_text;
    public $content_tbl_name;
    public $tbl_content_table_name;
    public $lang_id;
    public $lang_name;
    public $content_id;
    public $error;
   
   
    
    /*
     * Constructor: connect to the database 
     */
    public function __construct() {
        try {
            parent::connect();
        } catch (Exception $exc) {
             throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }
    
    /** 
     * @name         :   addSiteTranslation
     * @param        :   Translation Object
     * @desc   :   The function is Translate to site static texts
     * @return       :   boolean
     * Table         :   tbl_phrases 
     * Added By      :   Gayan Chathuranga
     * Added On      :   03-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */
    public function addSiteTranslation() {
        $recordId = null;
        try {
                $id          = $this->id;
                $tbl_lang_id = $this->lang_id;
                $phrase_type = $this->phrase_type;  //tbl_lang_fields
                $phrase_name = $this->phrase_name;
                $phrase_text = $this->phrase_text;
                $tbl_content_table_name = $this->content_tbl_name; //tbl_lang_fields
                $tbl_content_id = $this->content_id;

                $inserted = $this->insert($this->tb_name, array($id,$tbl_lang_id, $phrase_type,$phrase_name,$phrase_text,$tbl_content_table_name,$tbl_content_id));
                if ($inserted) {
                    $recordId = $this->getLastInsertedId();
                }
            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
     /** 
     * @name         :   addSiteTranslation
     * @param        :   Translation Object
     * @desc   :   The function edit translated static text
     * @return       :   boolean
     * Table         :   tbl_phrases 
     * Added By      :   Gayan Chathuranga
     * Added On      :   03-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */
    public function editSiteTranslation(){
        $isUpdated = false;
        try {
                $id          = $this->id;
                $tbl_lang_id = $this->lang_id;
                $phrase_type = $this->phrase_type;  //tbl_lang_fields
                $phrase_name = $this->phrase_name;
                $phrase_text = $this->phrase_text;
                $tbl_content_table_name = $this->content_tbl_name; //tbl_lang_fields
                $tbl_content_id = $this->content_id;

                $arrayData = array(
                    'phrase_text' => $phrase_text);
                //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
                $arrWhere = array("id = '" . $id . "'");
                $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>User</em>, <strong>Function -</strong> <em>addUser()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
     /** '
     * @name         :   deleteTranslation
     * @param        :   --
     * @desc   :   The function is to add a new page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   03-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deleteTranslation() {
        $isDeleted = false;
        try {
                $id = $this->id;
                $arrWhere = array("id = '" . $id . "'");
                $isDeleted = $this->delete($this->tb_name, $arrWhere);

            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>User</em>, <strong>Function -</strong> <em>addUser()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    
    /** '
     * @name         :   getAllPhrases
     * @param        :   --
     * @desc         :   The function is to get all prashes
     * @table        :   tbl_lang_fields
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   03-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */
    public function deletePhraseByContentId($content_id){
        
         $isDeleted = false;
        try {
                $id = $this->id;
                $arrWhere = array("tbl_content_id = '" . $content_id . "'");
                $isDeleted = $this->delete($this->tb_name, $arrWhere);

            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>User</em>, <strong>Function -</strong> <em>addUser()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");

        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
     /** '
     * @name         :   getEnabledTranslation
     * @param        :   --
     * @desc         :   The function is to get enabled translation to the phrases 
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   03-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */
    
    public function getPhraseTranslation($id){
         $objTrans = new stdClass();
         $objLanguage = new Language();
         $objLanguage->tb_name = 'tbl_lang';
        try {

                $colums = '*';
                $where = 'id = ' . $id;
                $this->select($this->tb_name, $colums, $where);
                $phrRow = $this->getResult();

                $this->lang_id = $phrRow['tbl_lang_id'];
                
                $objTrans->id = $phrRow['id'];
                $objTrans->lang_name = $objLanguage->getLanguageName($this->lang_id,'Yes');
                $objTrans->phrase_type = $phrRow['phrase_type'];
                $objTrans->phrase_name = $phrRow['phrase_name'];
                $objTrans->phrase_text = $phrRow['phrase_text'];
                $objTrans->tbl_content_table_name = $phrRow['tbl_content_table_name'];
                $objTrans->content_id = $phrRow['tbl_content_id'];
                
                return $objTrans;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    /** '
     * @name         :   getPhrasetranslation
     * @param        :   Integer (Category ID)
     * @desc         :   The function is to get a Category details
     * @return       :   Category Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */
    public function getAllPhraseTranslation($id,$content_tbl_name) {
        $arrTransPhrase = array();
        try {
                $colums = 'id';
                $where = 'tbl_content_id = ' . $id. ' AND tbl_content_table_name = "'.$content_tbl_name.'" ';
                $this->selectRow($this->tb_name, $colums, $where);
                $PhrRslt = $this->getResult();
                //echo 'p';print_r($PhrRslt);
                foreach ($PhrRslt As $PhrRow) {
                    $id = $PhrRow['id'];
                    $phrTrnInfo = $this->getPhraseTranslation($id);
                    array_push($arrTransPhrase, $phrTrnInfo);
                }
            return $arrTransPhrase;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
}
?>
