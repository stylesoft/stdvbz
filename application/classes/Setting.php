<?php
/**
 * @name        Setting
 * @copyright   2012 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1
 * @author     	Iyngaran Iyathurai
 *              Developer -  Monara IT UK Ltd
 *              iyngaran.iyathurai@monara.com
 *
 */

class Setting extends Core_Database{


	/**'
	 * @name         :   addSetting
	 * @param        :   SettingObject
	 * Description   :   The function is to add a new page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function addSetting($objSetting){
		$recordId = null;
		try{
			if($this->connect()){
				$id 				= $objSetting->id;
				$url 				= $objSetting->url;
                                $siteName			= $objSetting->siteName;
                                $siteEmail			= $objSetting->siteEmail;
                                $template			= $objSetting->template;
                                $sideBox			= $objSetting->sideBox;
                                $baseUrl			= $objSetting->baseUrl;
                                $needPing			= $objSetting->needPing;
                                $facebook_app_id                = $objSetting->facebook_app_id;
                                $facebook_secret_key            = $objSetting->facebook_secret_key;
                                $consumer_key                   = $objSetting->consumer_key;
                                $consumer_secret                = $objSetting->consumer_secret;
                                $user_token                     = $objSetting->user_token;
                                $user_secret                    = $objSetting->user_secret;
                                $terms_conditions               = $objSetting->terms_conditions;
                                $privacy_policy                 = $objSetting->privacy_policy;
                                $max_num_pages                  = $objSetting->max_num_pages;
                                $pluspro_logo_text              = $objSetting->pluspro_logo_text;
                                $site_logo                      = $objSetting->logo;
                                $favIcon                        = $objSetting->favIcon;
                                $homeBannerHeight               = $objSetting->homeBannerHeight;
                                $homeBannerWidth                = $objSetting->homeBannerWidth;
                                $mobileSite                     = $objSetting->mobileSite;
                                
				$inserted = $this->insert('settings',array($id,$url,$siteName,$siteEmail,$template,$sideBox,$baseUrl,$needPing,$facebook_app_id,$facebook_secret_key,$consumer_key,$consumer_secret,$user_token,$user_secret,$terms_conditions,$privacy_policy,$max_num_pages,$pluspro_logo_text,$site_logo,$favIcon,$homeBannerHeight,$homeBannerWidth,$mobileSite)); 
				if($inserted){
					$recordId = $this->getLastInsertedId();
				} 
			}
			return $recordId;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Setting</em>, <strong>Function -</strong> <em>addSetting()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}


	/**'
	 * @name         :   editSetting
	 * @param        :   SettingObject
	 * Description   :   The function is to edit a page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function editSetting($objSetting){
		$isUpdated = false;
		try{
			if($this->connect()){
				$id 				= $objSetting->id;
				$url 				= $objSetting->url;
                                $siteName			= $objSetting->siteName;
                                $siteEmail			= $objSetting->siteEmail;
                                $template			= $objSetting->template;
                                $sideBox			= $objSetting->sideBox;
                                $baseUrl			= $objSetting->baseUrl;
                                $needPing			= $objSetting->needPing;
                                $facebook_app_id                = $objSetting->facebook_app_id;
                                $facebook_secret_key            = $objSetting->facebook_secret_key;
                                $consumer_key                   = $objSetting->consumer_key;
                                $consumer_secret                = $objSetting->consumer_secret;
                                $user_token                     = $objSetting->user_token;
                                $user_secret                    = $objSetting->user_secret;
                                $terms_conditions               = $objSetting->terms_conditions;
                                $privacy_policy                 = $objSetting->privacy_policy;
                                $max_num_pages                  = $objSetting->max_num_pages;
                                $pluspro_logo_text              = $objSetting->pluspro_logo_text;
                                $site_logo                      = $objSetting->logo;
                                $favIcon                        = $objSetting->favIcon;
                                $homeBannerHeight               = $objSetting->homeBannerHeight;
                                $homeBannerWidth                = $objSetting->homeBannerWidth;
                                $mobileSite                     = $objSetting->mobileSite;
                                
                                if($pluspro_logo_text == ''){
                                    $arrayData          = array('URL'=>$url,
                                                                                        'SITE_NAME'=>$siteName,
                                                                                        'SITE_EMAIL'=>$siteEmail,
                                                                                        'TEMPLATE'=>$template,
                                                                                        'SIDEBOX'=>$sideBox,
                                                                                        'BASEURL'=>$baseUrl,
                                                                                        'NEEDPING'=>$needPing,
                                                                                        'facebook_app_id'=>$facebook_app_id,
                                                                                        'facebook_secret_key'=>$facebook_secret_key,
                                                                                        'consumer_key'=>$consumer_key,
                                                                                        'consumer_secret'=>$consumer_secret,
                                                                                        'user_token'=>$user_token,
                                                                                        'user_secret'=>$user_secret,
                                                                                        'terms_conditions' => $terms_conditions,
                                                                                        'privacy_policy' => $privacy_policy,                                        
                                                                                        'logo' => $site_logo,
                                                                                        'favIcon' => $favIcon,
                                                                                        'homeBanner_height' => $homeBannerHeight,
                                                                                        'homeBanner_width' => $homeBannerWidth,
                                                                                        'mobile_site' => $mobileSite
                                        
                                            );
                                } else {
                                        $arrayData          = array('URL'=>$url,
                                                                                        'SITE_NAME'=>$siteName,
                                                                                        'SITE_EMAIL'=>$siteEmail,
                                                                                        'TEMPLATE'=>$template,
                                                                                        'SIDEBOX'=>$sideBox,
                                                                                        'BASEURL'=>$baseUrl,
                                                                                        'NEEDPING'=>$needPing,
                                                                                        'facebook_app_id'=>$facebook_app_id,
                                                                                        'facebook_secret_key'=>$facebook_secret_key,
                                                                                        'consumer_key'=>$consumer_key,
                                                                                        'consumer_secret'=>$consumer_secret,
                                                                                        'user_token'=>$user_token,
                                                                                        'user_secret'=>$user_secret,
                                                                                        'terms_conditions' => $terms_conditions,
                                                                                        'privacy_policy' => $privacy_policy,
                                                                                        'max_num_pages' => $max_num_pages,
                                                                                        'pluspro_logo_text' => $pluspro_logo_text,
                                                                                        'logo' => $site_logo,
                                                                                        'favIcon' => $favIcon,
                                                                                        'homeBanner_height' => $homeBannerHeight,
                                                                                        'homeBanner_width' => $homeBannerWidth,
                                                                                        'mobile_site' => $mobileSite
                                            );
                                }
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("ID = '" . $id . "'");
				$isUpdated = $this->update('settings',$arrayData,$arrWhere);
			}
			return $isUpdated;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Setting</em>, <strong>Function -</strong> <em>addSetting()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}

	/**'
	 * @name         :   deleteSetting
	 * @param        :   SettingObject
	 * Description   :   The function is to add a new page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function deleteSetting($objSetting){
		$isDeleted    = false;
		try {
			if($this->connect()){
				$id 				= $objSetting->id;
				$arrWhere  = array("ID = '" . $id . "'");
				$isDeleted = $this->delete('settings',$arrWhere);
			}
			return $isDeleted;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Setting</em>, <strong>Function -</strong> <em>addSetting()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
		
	}


	/**'
	 * @name         :   getSetting
	 * @param        :   Integer (Setting ID)
	 * Description   :   The function is to get a page details
	 * @return       :   Setting Object
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getSetting($settingId){
		$objSetting = new stdClass();
		try{
			if($this->connect()){
				$colums = '*';
				$where  = 'ID = '.$settingId;
				$this->select('settings',$colums,$where);
				$pageInfo = $this->getResult();
					
				$objSetting->id = $pageInfo['ID'];
				$objSetting->url = $pageInfo['URL'];
				$objSetting->siteName = $pageInfo['SITE_NAME'];
				$objSetting->siteEmail = $pageInfo['SITE_EMAIL'];
				$objSetting->template = $pageInfo['TEMPLATE'];
				$objSetting->sideBox = $pageInfo['SIDEBOX'];
				$objSetting->baseUrl = $pageInfo['BASEURL'];
				$objSetting->needPing = $pageInfo['NEEDPING'];
                                $objSetting->facebook_app_id = $pageInfo['facebook_app_id'];
                                $objSetting->facebook_secret_key = $pageInfo['facebook_secret_key'];
                                $objSetting->consumer_key = $pageInfo['consumer_key'];
                                $objSetting->consumer_secret = $pageInfo['consumer_secret'];
                                $objSetting->user_token = $pageInfo['user_token'];
                                $objSetting->user_secret = $pageInfo['user_secret'];
                                $objSetting->terms_conditions = $pageInfo['terms_conditions'];
                                $objSetting->privacy_policy = $pageInfo['privacy_policy'];
                                $objSetting->max_num_pages = $pageInfo['max_num_pages'];
                                $objSetting->pluspro_logo_text = $pageInfo['pluspro_logo_text'];
                                $objSetting->logo = $pageInfo['logo'];
                                $objSetting->favIcon = $pageInfo['favIcon'];
                                $objSetting->homeBannerHeight = $pageInfo['homeBanner_height'];
                                $objSetting->homeBannerWidth = $pageInfo['homeBanner_width'];
                                $objSetting->mobileSite = $pageInfo['mobile_site'];
                             
			}
			return $objSetting;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Setting</em>, <strong>Function -</strong> <em>getSetting()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}

	}
 
}
?>