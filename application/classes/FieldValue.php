<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class FieldValue extends Core_Database {

    //news propoerties
    public $id;
    public $fieldsId;
    public $subFieldsId;
    public $fieldValue;
    public $fieldAttributeValue1;
    
    
    

    //constructor
    public function FieldValue() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /*     * '
     * @name         :   addFieldValue
     * @param        :   categoryObject
     * Description   :   The function is to category details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function addFieldValue() {
       $recordId = null;
        try {
                $id 			= $this->id;
                $fieldsId 		= $this->fieldsId;
                $subFieldsId 		= $this->subFieldsId;
                $fieldValue 		= $this->fieldValue;
                $fieldAttributeValue1	= $this->fieldAttributeValue1;

                $inserted = $this->insert('tbl_field_value', array($id,$fieldsId,$subFieldsId,$fieldValue,$fieldAttributeValue1));
                if ($inserted) {
                    $recordId = $this->getLastInsertedId();
            }
            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   editNews
     * @param        :   NewsObject
     * Description   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editFieldValue() {
         $isUpdated = false;
        try {
                $id 			= $this->id;
                $fieldsId 		= $this->fieldsId;
                $subFieldsId 		= $this->subFieldsId;
                $fieldValue 		= $this->fieldValue;
                $fieldAttributeValue1	= $this->fieldAttributeValue1;

                $arrayData = array(
                    'tbl_fields_id' => $fieldsId,
                    'tbl_sub_fields_id' => $subFieldsId,
                    'field_value' => $fieldValue,
                    'field_attribute_value_1' => $fieldAttributeValue1
                );
                $arrWhere = array("id = '" . $id . "'");
                $isUpdated = $this->update('tbl_field_value', $arrayData, $arrWhere);
                return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    

    /*     * '
     * @name         :   deleteFieldValue
     * @param        :   FieldValueObject
     * Description   :   The function is to delete FieldValue details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deleteFieldValue() {
        $isDeleted = false;
        try {
            
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getFieldValue
     * @param        :   Integer (FieldValue ID)
     * Description   :   The function is to get a FieldValue details
     * @return       :   FieldValue Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getFieldValue($id) {
        $objFieldValue = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' .$id;
                $this->select('tbl_field_value', $colums, $where);
                $fieldsInfo = $this->getResult();
                
                if($fieldsInfo){
                    $objFieldValue->id = $fieldsInfo['id'];
                    $objFieldValue->fieldsId = $fieldsInfo['tbl_fields_id'];
                    $objFieldValue->subFieldsId = $fieldsInfo['tbl_sub_fields_id'];
                    $objFieldValue->fieldValue = $fieldsInfo['field_value'];
                    $objFieldValue->fieldAttributeValue1 = $fieldsInfo['field_attribute_value_1'];
                } else {
                    return null;
                }
            }
            return $objFieldValue;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

   
    /*     
     * * '
     * @name         :   getAllByType
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getFieldValueByFieldId($fieldId) {
        $fieldValueInfo = '';
        try {
            if ($this->connect()) {
                
                $SQL = "SELECT id FROM tbl_field_value WHERE tbl_fields_id = '".$fieldId."'";
                $dbResult = $this->executeSelectQuery($SQL);
                $fieldValueRes = $this->getResult();
                
                foreach ($fieldValueRes As $fieldRow) {
                    $id = $fieldRow['id'];
                    $fieldValueInfo = $this->getFieldValue($id);
                }
            }

            return $fieldValueInfo;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    
       /*     
     * * '
     * @name         :   getAllByType
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getFieldValueByFieldIdAndSubFieldId($fieldId,$subFieldId) {
        $fieldValueInfo = '';
        try {
            if ($this->connect()) {
                                
                $SQL = "SELECT id FROM tbl_field_value WHERE tbl_fields_id = '" . $fieldId . "' AND tbl_sub_fields_id = '".$subFieldId."'";
                $dbResult = $this->executeSelectQuery($SQL);
                $fieldsResult = $this->getResult();
           
                foreach ($fieldsResult As $fieldRow) {
                    $id = $fieldRow['id'];
                    $fieldValueInfo = $this->getFieldValue($id);
                }
            }
            return $fieldValueInfo;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    /*     
     * * '
     * @name         :   isFieldIdExists
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */
    public function isFieldIdExists($fieldId) {
        $isFieldExists = false;
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = "tbl_fields_id = '" . $fieldId . "'";
                $orderBy = "id ASC";
                $this->select('tbl_field_value', $colums, $where, $orderBy);
                $fieldsResult = $this->getResult();
                foreach ($fieldsResult As $fieldRow) {
                    $isFieldExists = true;
                    
                    // break foreach here....
                }
            }

            return $isFieldExists;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
      /*     
     * * '
     * @name         :   isSubFieldIdExists
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */
    public function isSubFieldIdExists($subFieldId) {
        $isFieldExists = false;
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = "tbl_sub_fields_id = '" . $subFieldId . "'";
                $orderBy = "id ASC";
                $this->select('tbl_field_value', $colums, $where, $orderBy);
                $fieldsResult = $this->getResult();
                foreach ($fieldsResult As $fieldRow) {
                    $isFieldExists = true;
                    
                    // break foreach here....
                }
            }

            return $isFieldExists;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }


}
?>