<?php
/**
 * @name        SiteContact
 * @copyright   2012 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1
 * @author     	Gayan Chathuranga
 *              Developer -  Monara IT UK Ltd
 *              gayan@monara.com
 *
 */
class SiteContact extends Core_Database {
    //put your code here
    //properties
    public $id;
    public $contact_name = '';
    public $address_line_1 = '';
    public $address_line_2 = '';
    public $address_line_3 = '';
    public $contact_number_1 = '';
    public $contact_number_2 = '';
    public $google_map_code = '';
    public $contact_type = '';
    
    
    /**'
	 * @name         :   __construct
	 * @param        :   -
	 * Description   :   contruct
	 * @return       :   boolean
	 * Added By      :   Gayan Chathuranga
	 * Added On      :   2011-11-19
	 * Modified By   :   -
	 * Modified On   :   -
	 */
    public function __construct() {
        try {
             parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>SiteContact</em>, <strong>Function -</strong> <em>__construct()</em>, <strong>Exception -</strong> <em>".$exc->getMessage()."</em>");
        }

       
    }
    
    
    /** '
     * @name         :   addSiteContact
     * @param        :   SiteContactObject
     * Description   :   The function is to add a new site contact
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   2011-11-19
     * Modified By   :   -
     * Modified On   :   -
     */

    public function addSiteContact() {
        $recordId = null;
        try {
                $id = $this->id;
                $contact_name = $this->contact_name;
                $address_line_1 = $this->address_line_1;
                $address_line_2 = $this->address_line_2;
                $address_line_3 = $this->address_line_3;
                $contact_number_1 = $this->contact_number_1;
                $contact_number_2 = $this->contact_number_2;
                $google_map_code = $this->google_map_code;
                $contact_type = $this->contact_type;

                $inserted = $this->insert('tbl_site_contact_info', array($id, $contact_name, $address_line_1, $address_line_2, $address_line_3, $contact_number_1, $contact_number_2, $google_map_code, $contact_type));
                if ($inserted) {
                    $recordId = $this->getLastInsertedId();
                }
            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>SiteContact</em>, <strong>Function -</strong> <em>addSiteContact()</em>, <strong>Exception -</strong> <em>".$exc->getMessage()."</em>");
        }
    }
    
    
    /** '
     * @name         :   editSiteContact
     * @param        :   SiteContactObject
     * Description   :   The function is to edit a new site contact
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   2011-11-19
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editSiteContact($id) {
        $isUpdated = false;
        try {
            $contact_name = $this->contact_name;
            $address_line_1 = $this->address_line_1;
            $address_line_2 = $this->address_line_2;
            $address_line_3 = $this->address_line_3;
            $contact_number_1 = $this->contact_number_1;
            $contact_number_2 = $this->contact_number_2;
            $google_map_code = $this->google_map_code;
            $contact_type = $this->contact_type;

            $arrayData = array('contact_name' => $contact_name,
                'address_line_1' => $address_line_1,
                'address_line_2' => $address_line_2,
                'address_line_3' => $address_line_3,
                'contact_number_1' => $contact_number_1,
                'contact_number_2' => $contact_number_2,
                'google_map_code' => $google_map_code,
                'contact_type' => $contact_type
            );
            //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
            $arrWhere = array("id = '" . $id . "'");
            $isUpdated = $this->update('tbl_site_contact_info', $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>SiteContact</em>, <strong>Function -</strong> <em>editSiteContact()</em>, <strong>Exception -</strong> <em>".$exc->getMessage()."</em>");
        }
    }
    
    
      /**'
	 * @name         :   deleteSiteContact
	 * @param        :   id
	 * Description   :   The function is to delete a siteContact
	 * @return       :   boolean
	 * Added By      :   Gayan Chathuranga
	 * Added On      :   2012-11-20
	 * Modified By   :   -
	 * Modified On   :   -
	 */
	public function deleteSiteContact($id) {
        $isDeleted = false;
        try {
            $arrWhere = array("id = '" . $id . "'");
            $isDeleted = $this->delete('tbl_site_contact_info', $arrWhere);
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>SiteContact</em>, <strong>Function -</strong> <em>deleteSiteContact()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    
        /**'
	 * @name         :   getSiteContact
	 * @param        :   id
	 * Description   :   The function is to get one siteContact detail
	 * @return       :   boolean
	 * Added By      :   Gayan Chathuranga
	 * Added On      :   2012-11-20
	 * Modified By   :   -
	 * Modified On   :   -
	 */
    public function getSiteContact($id,$type='CONTACT_1'){
        $objSiteContact = new stdClass();
        try {
            $colums = '*';
            $where  = 'id = "'.$id.'" AND contact_type = "'.$type.'" ';
            $this->select('tbl_site_contact_info',$colums,$where);
            $siteContactInfo = $this->getResult();
            
            $objSiteContact->id = $siteContactInfo['id'];
            $objSiteContact->contact_name  = $siteContactInfo['contact_name'];
            $objSiteContact->address_line_1  = $siteContactInfo['address_line_1'];
            $objSiteContact->address_line_2  = $siteContactInfo['address_line_2'];
            $objSiteContact->address_line_3  = $siteContactInfo['address_line_3'];
            $objSiteContact->contact_number_1  = $siteContactInfo['contact_number_1'];
            $objSiteContact->contact_number_2  = $siteContactInfo['contact_number_2'];
            $objSiteContact->google_map_code  = $siteContactInfo['google_map_code'];
            $objSiteContact->contact_type  = $siteContactInfo['id'];
            
            return $objSiteContact;
            
        } catch (Exception $exc) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>SiteContact</em>, <strong>Function -</strong> <em>getSiteContact()</em>, <strong>Exception -</strong> <em>" . $exc->getMessage() . "</em>");
        }
        }
        
        
        
        /**'
	 * @name         :   getAllSiteContact
	 * @param        :   -
	 * Description   :   The function is to get one siteContact detail
	 * @return       :   boolean
	 * Added By      :   Gayan Chathuranga
	 * Added On      :   2012-11-20
	 * Modified By   :   -
	 * Modified On   :   -
	 */
    public function getAllSiteContact(){
        $objSiteContact = new stdClass();
        $arrSiteContact = array();
        try {
            $colums = '*';
            $where  = 'id =';
            $this->select('tbl_site_contact_info',$colums,$where);
            $siteContactInfo = $this->getResult();
            
            if(count($siteContactInfo)){
                foreach($siteContactInfo as $rowData){
                    $arrSiteContact = $this->getSiteContact($rowData['id']);
                    array_push($objSiteContact, $arrSiteContact);
                }
                
            }
            
            return $objSiteContact;
            
        } catch (Exception $exc) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>SiteContact</em>, <strong>Function -</strong> <em>getSiteContact()</em>, <strong>Exception -</strong> <em>" . $exc->getMessage() . "</em>");
        }
        }
    
}

?>
