<?php

class MailTemplate extends Core_Database {

    //class propoerties
    public $id;
    public $name;
    public $fromMail;
    public $fromName;
    public $mailSubject;
    public $mailText;
    public $isEnabled;

    //construct
    public function __construct() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /** '
     * @name         :   getComment
     * @param        :   Integer (Page ID)
     * @desc         :   The function is to get a post
     * @return       :   PostObject
     * Added By      :   Gayan Chathuranga
     * Added On      :   19-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */
    public function getTemplate($id) {
        $objPost = new stdClass();
        try {

            $colums = '*';
            $where = 'mt_id = ' . $id;
            $this->select('tbl_mail_template', $colums, $where);
            $dataInfo = $this->getResult();

            $objPost->id = $dataInfo['mt_id'];
            $objPost->name = $dataInfo['mt_name'];
            $objPost->fromMail = $dataInfo['mt_from_email'];
            $objPost->fromName = $dataInfo['mt_from_name'];
            $objPost->mailSubject = $dataInfo['mt_subject'];
            $objPost->mailText = $dataInfo['mt_text'];
            $objPost->isEnabled = $dataInfo['mt_enabled'];

            return $objPost;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

}

?>