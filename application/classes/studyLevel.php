<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class studyLevel extends Core_Database {

    //news propoerties
public $id;
public $institute;
public $level;
public $description;
public $lang;

public $created_on;
public $created_by;
public $searchStr;
public $last_modified_on;
public $last_modified_by;


    public $error = array();
    public $data_array = array();

    //constructor
   public function studyLevel() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /*     * '
     * @name         :   Add Level
     * @param        :   
     * Description   :   
     * @return       :   boolean
     * Added By      :   zumry Deen
     * Added On      :   10-02-2013
     * Modified By   :   -
     * Modified On   :   -
     */


    public function addstudyLevel() {
    	$recordId = null;
    	try {
    
    		$id =$this->id;
    		$institute =$this->institute;
    		$level = $this->level;
    		$lang = $this->lang;
    		$description=$this->description;
    		$created_on=$this->created_on;
    		$created_by=$this->created_by;
    		$last_modified_on=$this->last_modified_on;
    		$last_modified_by=$this->last_modified_by;
   
    		$inserted = $this->insert($this->tb_name, array($id,$institute,$level,$description,$lang,$created_on,$created_by,$last_modified_on,$last_modified_by));
    
    		if ($inserted) {
    			 
    			$recordId = $this->getLastInsertedId();
    		}
    		return $recordId;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    
    // Get all study level 
    
    
    public function getallLevel(){
    			
    	$arrLevel = array();
    	$arrWhere = array();
    	try {
    		$SQL = "SELECT id FROM set_study_level ";
    		if ($this->searchStr != '') {
    			
    			array_push($arrWhere, "level LIKE '" . "%" . $this->searchStr . "%" . "' ");
    		}
    		
    		if (count($arrWhere) > 0)
    			
    			$SQL.= "WHERE " . implode(' AND ', $arrWhere);
    		
    		if ($this->listingOrder) {
				$SQL.= ' ORDER BY ' . $this->listingOrder;
			}
			

    		$this->executeSelectQuery($SQL);
    		$rsltData = $this->getResult();
    		foreach ($rsltData As $dataRow) {
    			$id = $dataRow['id'];
    			$dataInfo = $this->getLevel($id);
    			array_push($arrLevel, $dataInfo);
    		}
    		return $arrLevel;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    
    

    
    
    
    /* Get institute data
     */
    public function getLevel($id){
    	$objStudyLevel = new stdClass();
    	try {
    		
    	
    		$SQL = "SELECT * FROM set_study_level";
    	
    		$SQL.= " WHERE id = ".$id;

    		$this->executeSelectQuery($SQL);
    		$dataInfo = $this->getResult();
    		foreach ($dataInfo as $row => $dataRow) {
    			$objStudyLevel->id = $dataRow['id'];
    			$objStudyLevel->institute = $dataRow['institute'];
    			$objStudyLevel->level = $dataRow['level'];
    			$objStudyLevel->description = $dataRow['description'];
    		}
    		return $objStudyLevel;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    
    public function updatestudyLevel($id){

    	$isUpdated = false;
    	try {
    	
    
    		$id =$this->id;
    		$institute =$this->institute;
    		$description=$this->description;
    		//$lang= $this->lang;
    		$level= $this->level;
    		//$created_by=$this->created_by;
    		$last_modified_on=$this->last_modified_on;
    		//$last_modified_by=$this->last_modified_by;
    		
    		
    		$arrayData = array(
    				'id' => $id,
    				'institute' => $institute,
    				//'lang' => $lang,
    				'description' => $description,
    				'level' => $level,
    				'last_modified_on' => $last_modified_on
    				 
    		);
    		//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
    		$arrWhere = array("id = '" . $id . "'");
    		$isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
    		return $isUpdated;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    	
    	
    	
    	
    	
    }
   
    
    public function  deleteLevel() {
    	 
    
    	 
    	$isDeleted = false;
    	try {
    		if ($this->connect()) {
    			$id = $this->id;
    			$arrWhere = array("id = '" . $id . "'");
    			$isDeleted = $this->delete($this->tb_name, $arrWhere);
    		}
    		return $isDeleted;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    
    
}
?>
