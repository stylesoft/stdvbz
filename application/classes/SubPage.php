<?php
/**
 * @name        SubPage
 * @copyright   2012 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1
 * @author     	Iyngaran Iyathurai
 *              Developer -  Monara IT UK Ltd
 *              iyngaran.iyathurai@monara.com
 *
 */

class SubPage extends Core_Database{


	/**'
	 * @name         :   addSubPage
	 * @param        :   SubPageObject
	 * Description   :   The function is to add a new page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function addSubPage($objSubPage){
		$recordId = null;
		try{
			if($this->connect()){
				$id 				= $objSubPage->id;
				$title 				= $objSubPage->title;
				$keywords 			= $objSubPage->keywords;
				$description                    = $objSubPage->description;
				$body 				= $objSubPage->body;
				$listingId 			= $objSubPage->listingId;
				$name 				= $objSubPage->name;
				$live 				= $objSubPage->live;
				$isContact 			= $objSubPage->isContact;
				$allowToDelete                  = $objSubPage->allowToDelete;
                                $mainPageId                     = $objSubPage->mainPageId;
				$inserted = $this->insert('sub_pages',array($id,$title,$keywords,$description,$body,$listingId,$name,$live,$isContact,$allowToDelete,$mainPageId)); 
				if($inserted){
					$recordId = $this->getLastInsertedId();
				} 
			}
			return $recordId;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>SubPage</em>, <strong>Function -</strong> <em>addSubPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}


	/**'
	 * @name         :   editSubPage
	 * @param        :   SubPageObject
	 * Description   :   The function is to edit a page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function editSubPage($objSubPage){
		$isUpdated = false;
		try{
			if($this->connect()){
				$id 				= $objSubPage->id;
				$title 				= $objSubPage->title;
				$keywords 			= $objSubPage->keywords;
				$description 		= $objSubPage->description;
				$body 				= $objSubPage->body;
				$listingId 			= $objSubPage->listingId;
				$name 				= $objSubPage->name;
				$live 				= $objSubPage->live;
				$isContact 			= $objSubPage->isContact;
				$allowToDelete 		= $objSubPage->allowToDelete;
				$arrayData          = array('TITLE'=>$title,
										'KEYWORDS'=>$keywords,
										'DESCRIPTION'=>$description,
										'BODY'=>$body,
										'ListingID'=>$listingId,
										'NAME'=>$name,
										'LIVE'=>$live,
										'ISCONTACT'=>$isContact,
										'ALLOWDELETE'=>$allowToDelete);
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("ID = '" . $id . "'");
				$isUpdated = $this->update('sub_pages',$arrayData,$arrWhere);
			}
			return $isUpdated;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>SubPage</em>, <strong>Function -</strong> <em>addSubPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}

	/**'
	 * @name         :   deleteSubPage
	 * @param        :   SubPageObject
	 * Description   :   The function is to add a new page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function deleteSubPage($objSubPage){
		$isDeleted    = false;
		try {
			if($this->connect()){
				$id 				= $objSubPage->id;
				$arrWhere  = array("ID = '" . $id . "'");
				$isDeleted = $this->delete('sub_pages',$arrWhere);
			}
			return $isDeleted;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>SubPage</em>, <strong>Function -</strong> <em>addSubPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
		
	}


	/**'
	 * @name         :   getSubPage
	 * @param        :   Integer (SubPage ID)
	 * Description   :   The function is to get a page details
	 * @return       :   SubPage Object
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getSubPage($pageId){
		$objSubPage = new stdClass();
                $objSecSubPage = new SecSubPage();
		try{
			if($this->connect()){
				$colums = '*';
				$where  = 'ID = '.$pageId;
				$this->select('sub_pages',$colums,$where);
				$pageInfo = $this->getResult();
					
				$objSubPage->id = $pageInfo['ID'];
				$objSubPage->title = $pageInfo['TITLE'];
				$objSubPage->keywords = $pageInfo['KEYWORDS'];
				$objSubPage->description = $pageInfo['DESCRIPTION'];
				$objSubPage->body = $pageInfo['BODY'];
				$objSubPage->listingId = $pageInfo['ListingID'];
				$objSubPage->name = $pageInfo['NAME'];
				$objSubPage->live = $pageInfo['LIVE'];
				$objSubPage->isContact = $pageInfo['ISCONTACT'];
				$objSubPage->allowToDelete = $pageInfo['ALLOWDELETE'];
                                $objSubPage->mainPageId = $pageInfo['MAIN_PAGE_ID'];
                                $objSubPage->subPages = $objSecSubPage->getAllByMainPageId($pageId);
			}
			return $objSubPage;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>SubPage</em>, <strong>Function -</strong> <em>getSubPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}

	}

	/**'
	 * @name         :   getAll
	 * @param        :
	 * Description   :   The function is to get all page details
	 * @return       :   Array (Array Of SubPage Object)
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getAll(){
		$arrSubPages       = array();
		try{
			if($this->connect()){
				$colums = 'ID';
				$where  = null;
                                $orderBy = " ListingID Asc";
				$this->select('sub_pages',$colums,$where,$orderBy);
				$pageRes = $this->getResult();
					
				foreach($pageRes As $pIndex=>$pageRow){
					$pageId = $pageRow['ID'];
					$pageInfo = $this->getSubPage($pageId);
					array_push($arrSubPages,$pageInfo);
				}
					
			}
			return $arrSubPages;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>SubPage</em>, <strong>Function -</strong> <em>getSubPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}
        
        
        /**'
	 * @name         :   updateOrder
	 * @param        :   SubPageObject
	 * Description   :   The function is to edit a page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function updateOrder($objSubPage){
		$isUpdated = false;
		try{
			if($this->connect()){
				$id 				= $objSubPage->id;
				$listingId 			= $objSubPage->listingId;
				$arrayData          = array('ListingID'=>$listingId);
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("ID = '" . $id . "'");
				$isUpdated = $this->update('sub_pages',$arrayData,$arrWhere);
			}
			return $isUpdated;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>SubPage</em>, <strong>Function -</strong> <em>addSubPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}
        
        
        /**'
	 * @name         :   getAll
	 * @param        :
	 * Description   :   The function is to get all page details
	 * @return       :   Array (Array Of SubPage Object)
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getAllByMainPageId($mainPageId){
		$arrSubPages       = array();
		try{
			if($this->connect()){
				
				
                                 $SQL = "SELECT * FROM sub_pages WHERE MAIN_PAGE_ID = '".$mainPageId."' AND LIVE = 'Published' ORDER BY ListingID Asc";
                                 //print($SQL); exit;
                                 $dbResult = $this->executeSelectQuery($SQL);
                                 $pageRes = $this->getResult();
				if($pageRes){	
                                    foreach($pageRes As $pIndex=>$pageRow){
                                            $pageId = $pageRow['ID'];
                                            $pageInfo = $this->getSubPage($pageId);
                                            array_push($arrSubPages,$pageInfo);
                                    }
                                }
					
			}
			return $arrSubPages;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>SubPage</em>, <strong>Function -</strong> <em>getSubPage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}
	 
	 
}
?>