<?php
/**
 * @name        User
 * @copyright   2012 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1
 * @author     	Iyngaran Iyathurai
 *              Developer -  Monara IT UK Ltd
 *              iyngaran.iyathurai@monara.com
 *
 */

class User extends Core_Database{


	/**'
	 * @name         :   addUser
	 * @param        :   UserObject
	 * Description   :   The function is to add a new page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function addUser($objUser){
		$recordId = null;
		try{
			if($this->connect()){
				$member_id 		= $objUser->member_id;
                                $firstname		= $objUser->firstname;
                                $lastname		= $objUser->lastname;
                                $login			= $objUser->login;
                                $passwd			= $objUser->passwd;
                                $isadmin		= $objUser->isadmin;
				$inserted = $this->insert('users',array($member_id,$firstname,$lastname,$login,$passwd,$isadmin)); 
				if($inserted){
					$recordId = $this->getLastInsertedId();
				} 
			}
			return $recordId;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>User</em>, <strong>Function -</strong> <em>addUser()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}


	/**'
	 * @name         :   editUser
	 * @param        :   UserObject
	 * Description   :   The function is to edit a page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function editUser($objUser){
		$isUpdated = false;
		try{
			if($this->connect()){
				$member_id 		= $objUser->member_id;
                                $firstname		= $objUser->firstname;
                                $lastname		= $objUser->lastname;
                                $login			= $objUser->login;
                                $passwd			= $objUser->passwd;
                                $isadmin		= $objUser->isadmin;
                                
                                if($objUser->passwd){
				$arrayData          = array('firstname'=>$firstname,
										'lastname'=>$lastname,
										'login'=>$login,
										'passwd'=>$passwd,
										'isadmin'=>$isadmin);
                                } else {
                                    
                                    $arrayData          = array('firstname'=>$firstname,
										'lastname'=>$lastname,
										'login'=>$login,
										'isadmin'=>$isadmin);
                                }
                                
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("member_id = '" . $member_id . "'");
				$isUpdated = $this->update('users',$arrayData,$arrWhere);
			}
			return $isUpdated;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>User</em>, <strong>Function -</strong> <em>addUser()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}

	/**'
	 * @name         :   deleteUser
	 * @param        :   UserObject
	 * Description   :   The function is to add a new page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function deleteUser($objUser){
		$isDeleted    = false;
		try {
			if($this->connect()){
				$id 				= $objUser->member_id;
				$arrWhere  = array("member_id = '" . $id . "'");
				$isDeleted = $this->delete('users',$arrWhere);
			}
			return $isDeleted;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>User</em>, <strong>Function -</strong> <em>addUser()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
		
	}


	/**'
	 * @name         :   getUser
	 * @param        :   Integer (User ID)
	 * Description   :   The function is to get a page details
	 * @return       :   User Object
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getUser($userId){
		$objUser = new stdClass();
		try{
			if($this->connect()){
				$colums = '*';
				$where  = 'member_id = '.$userId;
				$this->select('users',$colums,$where);
				$userInfo = $this->getResult();
					
				$objUser->member_id = $userInfo['member_id'];
                                $objUser->firstname = $userInfo['firstname'];
                                $objUser->lastname = $userInfo['lastname'];
                                $objUser->login = $userInfo['login'];
                                $objUser->passwd = $userInfo['passwd'];
                                $objUser->isadmin = $userInfo['isadmin'];
				
			}
			return $objUser;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>User</em>, <strong>Function -</strong> <em>getUser()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}

	}
        
        
        
        
        /** '
     * @name         :   getAll
     * @param        :
     * @desc   :   The function is to get all news details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAll() {
        $users = array();
        try {
            if ($this->connect()) {
                $colums = 'member_id';
                $where  = "isadmin != '2'";
                $orderBy = " member_id Asc";
                $this->select('users', $colums, $where, $orderBy);
                $userResult = $this->getResult();
                foreach ($userResult As $userRow) {
                    $userId = $userRow['member_id'];
                    $userInfo = $this->getUser($userId);
                    array_push($users, $userInfo);
                }
            }

            return $users;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

	
         /**'
	 * @name         :   loginUser
	 * @param        :   Integer (User ID)
	 * Description   :   The function is to get a page details
	 * @return       :   User Object
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function loginUser($username,$password,$userType = ''){
		$objUser = new stdClass();
		try{
			if($this->connect()){
				$colums = '*';
                                if($userType != ''){
                                    $where  = "login = '".$username."' AND passwd = '".  md5($password)."' AND isadmin = '".$userType."'";
                                } else {
                                    $where  = "login = '".$username."' AND passwd = '".  md5($password)."'";
                                }
                                $this->select('users',$colums,$where);
				$userInfo = $this->getResult();
				if($userInfo){	
                                    $objUser->id = $userInfo['member_id'];
                                    $objUser->firstName = $userInfo['firstname'];
                                    $objUser->lastName = $userInfo['lastname'];
                                    $objUser->login = $userInfo['login'];
                                    $objUser->isAdmin = $userInfo['isadmin'];
                                } else {
                                    return null;
                                }
			}
			return $objUser;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>User</em>, <strong>Function -</strong> <em>getUser()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}

	}
        
        
        
        
        /**'
	 * @name         :   editUser
	 * @param        :   UserObject
	 * Description   :   The function is to edit a page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function changePassword($objUser){
		$isUpdated = false;
		try{
			if($this->connect()){
				$member_id 		= $objUser->member_id;
                                $firstname		= $objUser->firstname;
                                $lastname		= $objUser->lastname;
                                $login			= $objUser->login;
                                $passwd			= $objUser->passwd;
                                $isadmin		= $objUser->isadmin;
                                
                                $arrayData          = array('passwd'=>$passwd);
                                
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("member_id = '" . $member_id . "'");
				$isUpdated = $this->update('users',$arrayData,$arrWhere);
			}
			return $isUpdated;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>User</em>, <strong>Function -</strong> <em>addUser()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}



  
	 
	
	
	
/**'
	 * @name         :   loginUser
	 * @param        :   Integer (User ID)
	 * Description   :   The function is to get a page details
	 * @return       :   User Object
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getUserByEmail($email_address){
		$objUser = new stdClass();
		try{
			if($this->connect()){
				$colums = '*';
				$where  = "login = '".$email_address."'";
				$this->select('users',$colums,$where);
				$userInfo = $this->getResult();
				if($userInfo){	
                                    $objUser->id = $userInfo['member_id'];
                                    $objUser->firstName = $userInfo['firstname'];
                                    $objUser->lastName = $userInfo['lastname'];
                                    $objUser->login = $userInfo['login'];
                                    $objUser->isAdmin = $userInfo['isadmin'];
                                } else {
                                    return null;
                                }
			}
			return $objUser;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>User</em>, <strong>Function -</strong> <em>getUser()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}

	}
        
        
        
        
        /** '
     * @name         :   getAll
     * @param        :
     * @desc   :   The function is to get all news details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllSystemUsers() {
        $users = array();
        try {
            if ($this->connect()) {
                $colums = 'member_id';
                $where = '';
                $orderBy = " member_id Asc";
                $this->select('users', $colums, $where, $orderBy);
                $userResult = $this->getResult();
                foreach ($userResult As $userRow) {
                    $userId = $userRow['member_id'];
                    $userInfo = $this->getUser($userId);
                    array_push($users, $userInfo);
                }
            }

            return $users;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
	 
	 
}
?>
