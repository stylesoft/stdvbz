<?php

/**
 * Description of BlogPost
 *
 * @author Gayan Chathuranga <gayan.chathuranga@monara.com>
 */
class BlogPost extends Core_Database {

    //class propoerties
    public $id;
    public $category_id;
    public $added_date;
    public $post_content;
    public $post_title;
    public $post_modified;
    public $post_author;
    public $post_status;
    public $post_keywords;
    public $post_description;
    public $searchStr;
    public $limit;
    public $listingOrder;
    

    //construct
    public function __construct() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /** '
     * @name         :   addPost
     * @param        :   postObject
     * Description   :   The function is to add a new post to the blog
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   19-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */
    public function addPost() {
 
        $recordId = null;
        try {
            $id = $this->id;
      
            $added_date = $this->added_date;
            $post_content = $this->post_content;
            $post_title = $this->post_title;
            $post_modified = $this->post_modified;
            $post_author = $this->post_author;
            $post_status = $this->post_status;
            $post_keywords = $this->post_keywords;
            $post_description = $this->post_description;

            $inserted = $this->insert($this->tb_name, array($id,  $added_date, $post_content, $post_title, $post_modified, $post_author, $post_status,$post_keywords,$post_description));
            if ($inserted) {
                $recordId = $this->getLastInsertedId();
            }

            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Image</em>, <strong>Function -</strong> <em>addImage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   editPost
     * @param        :   ImageObject
     * Description   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   16-08-2012
     * Modified By   :   Iyngaran Iyathurai
     * Modified On   :   16-08-2012
     */

    public function editPost($id) {
        $isUpdated = false;
        try {
            $id = $this->id;
           
            $added_date = $this->added_date;
            $post_content = $this->post_content;
            $post_title = $this->post_title;
            $post_modified = $this->post_modified;
            $post_author = $this->post_author;
            $post_status = $this->post_status;
            $post_keywords = $this->post_keywords;
            $post_description = $this->post_description;

            $arrayData = array(
                'added_date' => $added_date,
                'post_content' => $post_content,
                'post_title' => $post_title,
                'post_modified' => $post_modified,
                'post_author' => $post_author,
                'post_status' => $post_status,
                'post_keywords' => $post_keywords,
                'post_description' => $post_description
            );
            $arrWhere = array("id = '" . $id . "'");
            $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Image</em>, <strong>Function -</strong> <em>addImage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   deletePost
     * @param        :   PostObject
     * @desc        :   The function is to delete a post from blog
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   19-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */
    public function deletePost() {
        $isDeleted = false;
        try {
            $id = $this->id;
            $arrWhere = array("id = '" . $id . "'");
            $isDeleted = $this->delete($this->tb_name, $arrWhere);

            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    /** '
     * @name         :   getPost
     * @param        :   Integer (Page ID)
     * @desc         :   The function is to get a post
     * @return       :   PostObject
     * Added By      :   Gayan Chathuranga
     * Added On      :   19-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getPost($id) {
        $objPost = new stdClass();
        try {

                $colums = '*';
                $where = 'id = ' . $id;
                $this->select($this->tb_name, $colums, $where);
                $postInfo = $this->getResult();

                $objPost->id = $postInfo['id'];
                $objPost->added_date = $postInfo['added_date'];
                $objPost->post_content = $postInfo['post_content'];
                $objPost->post_title = $postInfo['post_title'];
                $objPost->post_modified = $postInfo['post_modified'];
                $objPost->post_author = $postInfo['post_author'];
                $objPost->post_status = $postInfo['post_status'];
                $objPost->post_keywords = $postInfo['post_keywords'];
                $objPost->post_description = $postInfo['post_description'];
                

            return $objPost;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    /** '
     * @name         :   getAll
     * @param        :
     * @desc         :   The function is to get all Post
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   19-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAll() {
        $arrPost = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = 'blog_status = 1';
                $orderBy = " added_date, blog_modified Asc";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $postResult = $this->getResult();
                foreach ($postResult As $postRow) {
                    $id = $postRow['id'];
                    $postInfo = $this->getPost($id);
                    array_push($arrPost, $postInfo);
                }
            }

            return $arrPost;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    /** '
     * @name         :   countRec
     * @param        :   PostObject
     * @desc         :   The function is to count the No of posts
     * @return       :   Integer (Total number Of News)
     * Added By      :   Gayan Chathuranga
     * Added On      :   19-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function countRec() {
        $totalNumberOfRec = 0;
       
        $arrWhere = array("post_status = 1");
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "post_title LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            $dbResult = $this->executeSelectQuery($SQL);
            $dataRes = $this->getResult();
            $totalNumberOfRec = count($dataRes);
            return $totalNumberOfRec;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
     /** '
     * @name         :   search
     * @param        :
     * Description   :   The function is to search  post details
     * @return       :   Array (Array Of RestaurantMenu Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   19-09-2012
     * Modified By   :   Iyngaran Iyathurai
     * Modified On   :   22-08-2012
     */

    public function search() {
        $arrPost = array();
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "post_title LIKE '"."%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            if ($this->listingOrder) {
                $SQL.= ' ORDER BY ' . $this->listingOrder;
            }

            if ($this->limit) {
                $SQL.= $this->limit;
            }
            //echo $SQL;
            $dbResult = $this->executeSelectQuery($SQL);
            $dataRes = $this->getResult();
            foreach ($dataRes As $dataRow) {
                $id = $dataRow['id'];
                $newsInfo = $this->getPost($id);
                array_push($arrPost, $newsInfo);
            }
            return $arrPost;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    /** '
     * @name         :   search by status 
     * @param        :
     * Description   :   The function is to search  post details
     * @return       :   Array (Array Of RestaurantMenu Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   14-06-2013
     * Modified By   :   DEEN
     * 
     */
    
    public function searchBystatus($status) {
    	$arrPost = array();
    	$arrWhere = array("post_status = '" . $status . "'");
    	try {
    		$SQL = "SELECT * FROM $this->tb_name ";
    		if ($this->searchStr != '') {
    			array_push($arrWhere, "post_title LIKE '"."%" . $this->searchStr . "%" . "'");
    		}
    
    		if (count($arrWhere) > 0)
    			$SQL.= "WHERE " . implode(' AND ', $arrWhere);
    
    
    		if ($this->listingOrder) {
    			$SQL.= ' ORDER BY ' . $this->listingOrder;
    		}
    
    		if ($this->limit) {
    			$SQL.= $this->limit;
    		}
    		//echo $SQL;
    		$dbResult = $this->executeSelectQuery($SQL);
    		$dataRes = $this->getResult();
    		foreach ($dataRes As $dataRow) {
    			$id = $dataRow['id'];
    			$newsInfo = $this->getPost($id);
    			array_push($arrPost, $newsInfo);
    		}
    		return $arrPost;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    

}

?>