<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Institute extends Core_Database {

    //news propoerties
public $id;
public $name ;
public $description ;
public $created_on;
public $created_by;
public $searchStr;
public $last_modified_on;
public $last_modified_by;


    public $error = array();
    public $data_array = array();

    //constructor
   public function Institute() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /*     * '
     * @name         :   Add Institute
     * @param        :   
     * Description   :   
     * @return       :   boolean
     * Added By      :   zumry Deen
     * Added On      :   10-02-2013
     * Modified By   :   -
     * Modified On   :   -
     */


    public function addInstitute() {
    	$recordId = null;
    	try {
    
    		$id =$this->id;
    		$name =$this->name;
    		$description=$this->description;
    		$created_on=$this->created_on;
    		$created_by=$this->created_by;
    		$last_modified_on=$this->last_modified_on;
    		$last_modified_by=$this->last_modified_by;
    
    		$inserted = $this->insert($this->tb_name, array($id,$name,$description,$created_on,$created_by,$last_modified_on,$last_modified_by));
    
    		if ($inserted) {
    			 
    			$recordId = $this->getLastInsertedId();
    		}
    		return $recordId;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    
    
    public function getAllInstitues(){
    	$arrInstitute = array();
    	$arrWhere = array();
    	try {
    		$SQL = "SELECT id FROM set_edu_institute ";
    		
    		
    		
    		if ($this->searchStr != '') {
    			
    			array_push($arrWhere, "name LIKE '" . "%" . $this->searchStr . "%" . "' ");
    		}
    		
    		if (count($arrWhere) > 0)
    			
    			$SQL.= "WHERE " . implode(' AND ', $arrWhere);
    		
    		if ($this->listingOrder) {
				$SQL.= ' ORDER BY ' . $this->listingOrder;
			}
    		$this->executeSelectQuery($SQL);
    		$rsltData = $this->getResult();
    		foreach ($rsltData As $dataRow) {
    			$id = $dataRow['id'];
    			$dataInfo = $this->getInstitute($id);
    			array_push($arrInstitute, $dataInfo);
    		}
    		return $arrInstitute;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    
    
    /* Get institute data
     */
    public function getInstitute($id){
    	$objInstitute = new stdClass();
    	try {
    		$SQL = "SELECT * FROM set_edu_institute";
    
    		 
    		$SQL.= " WHERE id = ".$id;
    
    		$this->executeSelectQuery($SQL);
    		$instituteInfo = $this->getResult();
    		foreach ($instituteInfo as $row => $dataRow) {
    			$objInstitute->id = $dataRow['id'];
    			$objInstitute->name = $dataRow['name'];
    			$objInstitute->description = $dataRow['description'];
    		}
    		return $objInstitute;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    
    public function updateInstitute($id){

    	$isUpdated = false;
    	try {
    	
    
    		$id =$this->id;
    		$name =$this->name;
    		$description=$this->description;
    		//$created_on=$this->created_on;
    		//$created_by=$this->created_by;
    		$last_modified_on=$this->last_modified_on;
    		//$last_modified_by=$this->last_modified_by;
    		
    		
    		$arrayData = array(
    				'id' => $id,
    				'name' => $name,
    				'description' => $description,
    				'last_modified_on' => $last_modified_on
    				 
    		);
    		//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
    		$arrWhere = array("id = '" . $id . "'");
    		$isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
    		return $isUpdated;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    	
    	
    	
    	
    	
    }
   
    
    public function  deleteInstitute() {
    	 
    
    	 
    	$isDeleted = false;
    	try {
    		if ($this->connect()) {
    			$id = $this->id;
    			$arrWhere = array("id = '" . $id . "'");
    			$isDeleted = $this->delete($this->tb_name, $arrWhere);
    		}
    		return $isDeleted;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    
    
}
?>
