<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Registration extends Core_Database {

    
public $id ;
public $firstName ;
public $surname ;
public $date_of_birth;
public $email ;
public $password;
//public $postCode;
public $living_at;
public $edu_institute ;
public $level_of_study ;
public $area_of_study;
public $code;
public $status;
public $lang;
public $created_on;
public $created_by;
public $last_modified_on;
public $last_modified_by;


    public $error = array();
    public $data_array = array();

    //constructor
   public function Registration() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /*     * '
     * @name         :   addBrand
     * @param        :   categoryObject
     * Description   :   The function is to category details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */


    
    public function addStudent() {
        $recordId = null;
        try {

        	  $id =$this->id;
        	  $firstName =$this->firstName;
        	 $surname =$this->surname;
        	 $date_of_birth =$this->date_of_birth;
        	 $email =$this->email ;
        	 $password=$this->password;
        	 $living_at=$this->living_at;
        	 $edu_institute=$this->edu_institute ;
        	 $level_of_study=$this->level_of_study ;
        	 $area_of_study=$this->area_of_study;
        	  $code=$this->code;
        	 $status=$this->status;
        	 $lang=$this->lang;
        	 $created_on=$this->created_on;
        	 $created_by=$this->created_by;
        	 $last_modified_on=$this->last_modified_on;
        	 $last_modified_by=$this->last_modified_by;

                $inserted = $this->insert($this->tb_name, array($id,$firstName,$surname,$date_of_birth,$email,$password,$living_at,$edu_institute,$level_of_study,$area_of_study,$code,$status,$lang,$created_on,$created_by,$last_modified_on,$last_modified_by));
                
                if ($inserted) {
                	
                    $recordId = $this->getLastInsertedId();
            }
            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }  
    
    
    

    /*     * '
     * @name         :   editNews
     * @param        :   NewsObject
     * Description   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Deen
     * Added On      :   
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editStudent() {
        $isUpdated = false;
        try {
                
          $id =$this->id;
        	  $firstName =$this->firstName;
        		
        		
        	$surname =$this->surname;
        	 
    
        	$date_of_birth =$this->date_of_birth;
        	 $email =$this->email ;
        	 $password=$this->password;
        	 $living_at=$this->living_at;
        	 $edu_institute=$this->edu_institute ;
        	 $level_of_study=$this->level_of_study ;
        	 $area_of_study=$this->area_of_study;
        	  $code=$this->code;
        	 $status=$this->status;
        	// $lang=$this->lang;
        	// $created_on=$this->created_on;
        	// $created_by=$this->created_by;
        	// $last_modified_on=$this->last_modified_on;
        	// $last_modified_by=$this->last_modified_by;

                $arrayData = array(
                    'id' => $id,
                    'first_name' => $firstName,
                		'surname' => $surname,
                		'email' => $email,
                		'password' => $password,
                		'date_of_birth' => $date_of_birth,
                		'living_at' => $living_at,
                		'edu_institute' => $edu_institute,
                		'level_of_study' => $level_of_study,	
                    'area_of_study' => $area_of_study,
                    'living_at' => $living_at,
                    'code' => $code
                   
                );
                //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
                $arrWhere = array("id = '" . $id . "'");
                $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   deleteCategory
     * @param        :   CategoryObject
     * Description   :   The function is to delete Category details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deleteStudent() {
    	
    
    	
        $isDeleted = false;
        try {
            if ($this->connect()) {
                $id = $this->id;
                $arrWhere = array("id = '" . $id . "'");
                $isDeleted = $this->delete($this->tb_name, $arrWhere);
            }
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getCategory
     * @param        :   Integer (Category ID)
     * Description   :   The function is to get a Category details
     * @return       :   Category Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getsStudent($stdId) {
        $objReg = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' . $stdId;
                $this->select('trn_student_info', $colums, $where);
                $studentInfo = $this->getResult();
                $objReg->id = $studentInfo['id'];
                
                $objReg->firstName = $studentInfo['first_name'];
                $objReg->surname = $studentInfo['surname'];
                $objReg->date_of_birth  = $studentInfo['date_of_birth'];
                $objReg->email = $studentInfo['email'];
                $objReg->password = $studentInfo['password'];
                $objReg->living_at = $studentInfo['living_at'];
                $objReg->edu_institute = $studentInfo['edu_institute'];
                $objReg->level_of_study = $studentInfo['level_of_study'];
                $objReg->area_of_study = $studentInfo['area_of_study'];
                $objReg->code  = $studentInfo['code'];
                $objReg->status  = $studentInfo['status'];
                $objReg->lang = $studentInfo['lang'];
                $objReg->created_on  = $studentInfo['created_on'];
                $objReg->last_modified_on  = $studentInfo['last_modified_on'];
                $objReg->last_modified_by  = $studentInfo['last_modified_by'];
                
                
                

         
            }
            return $objReg;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getAll
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAll() {
        $arrBrand = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = 'status = 1';
                $orderBy = "first_name ASC";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $brandResult = $this->getResult();
                foreach ($brandResult As $catRow) {
                    $stdId = $catRow['id'];
                    $catInfo = $this->getsStudent($stdId);
                    array_push($arrBrand, $catInfo);
                }
            }

            return $arrBrand;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getAllByStatus
     * @param        :
     * Description   :   The function is to get all category details by status
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllByStatus($status) {
        $arrBrand = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = "status = '" . $status . "'";
                $orderBy = "first_name ASC";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $catRes = $this->getResult();
                
                foreach ($catRes As $nIndex => $catRow) {
                    $stdId = $catRow['id'];
                    $catInfo = $this->getsStudent($stdId);
                    array_push($arrBrand, $catInfo);
                }
            }
            return $arrBrand;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    /*     * '
     * @name         :   getAllParentByStatus
     * @param        :
     * Description   :   The function is to get all category details by status
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllParentByStatus($status,$cat_id) {
        $arrBrand = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = "id != '".$cat_id."' AND status = '" . $status . "'";
                $orderBy = "first_name ASC";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $catRes = $this->getResult();

                foreach ($catRes As $nIndex => $catRow) {
                    $stdId = $catRow['id'];
                    $catInfo = $this->getsStudent($stdId);
                    array_push($arrBrand, $catInfo);
                }
            }
            return $arrBrand;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    
     /**'
	 * @name         :   updateOrder
	 * @param        :   ProductObject
	 * @desc   :   The function is to edit a Product Listing order
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   04-09-2012
	 * Modified By   :   Gayan Chathuranga
	 * Modified On   :   04-09-2012
	 */
	public function updateOrder(){
		$isUpdated = false;
		try{
			
				$id 				= $this->id;                               
				$displayorder 			= $this->displayorder;                               
				$arrayData          = array('display_order'=>$displayorder);
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("id = '" . $id . "'");
                                
				$isUpdated = $this->update($this->tb_name,$arrayData,$arrWhere);
			
			return $isUpdated;
		}catch (Exception $e){		}

			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>RestaurantMenu</em>, <strong>Function -</strong> <em>updateOrder()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
	}
        
        
        
        
         /*     * '
     * @name         :   getAllByCategoryObject
     * @param        :
     * Description   :   The function is to get all category details by object types
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   19-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllByBrandObject($param = '') {
        $arrBrand = array();
        try {

            $colums = 'id';
            $where = "id != '" . $cat_id . "' AND status = '" . $status . "'";
            $orderBy = "first_name ASC";
            $this->select($this->tb_name, $colums, $where, $orderBy);
            $catRes = $this->getResult();

            foreach ($catRes As $nIndex => $catRow) {
                $stdId = $catRow['id'];
                $catInfo = $this->getsStudent($stdId);
                array_push($arrBrand, $catInfo);
            }

            return $arrBrand;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
     /** '
     * @name         :   countRec
     * @param        :   CategoryObject
     * @desc         :   The function is to count the nnumer of active categories
     * @return       :   Integer (Total number Of News)
     * Added By      :   Gayan Chathuranga
     * Added On      :   12-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function countRec() {
        $totalNumberOfRec = 0;
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "first_name LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            $dbResult = $this->executeSelectQuery($SQL);
            $catRes = $this->getResult();
            $totalNumberOfRec = count($catRes);
            return $totalNumberOfRec;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   search
     * @param        :   category_name
     * @desc         :   The function is to search  category details by name
     * @return       :   Array (Array Of RestaurantMenu Object)
     * Added By      :   gayan Chathuranga
     * Added On      :   13-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function search() {
        $arrBrand = array();
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "first_name LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

	    if ($this->status != '') {
                    array_push($arrWhere, "status = '" . $this->status . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            if ($this->listingOrder) {
                $SQL.= ' ORDER BY ' . $this->listingOrder;
            }

            if ($this->limit) {
                $SQL.= $this->limit;
            }
            //echo $SQL;
            $dbResult = $this->executeSelectQuery($SQL);
            $catRes = $this->getResult();
            foreach ($catRes As $categoryRow) {
                $id = $categoryRow['id'];
                $studentInfo = $this->getsStudent($id);
                array_push($arrBrand, $studentInfo);
            }
            return $arrBrand;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
      }
    }

    
    
    // new functions 
    
    public function getStudentSupportList() {
    	$data_array = array();
    	try {
    		$colums = '*';
    		$where = '';
    		$orderBy = "name ASC";
    		$this->select('set_support_list', $colums, $where, $orderBy);
    		$supportInfo = $this->getResult();
    		foreach ($supportInfo as $key => $data) {
    			$objSupportList = new stdClass();
    			$objSupportList->id = $data['id'];
    			$objSupportList->name = $data['name'];
    			$objSupportList->description = $data['description'];
    
    			array_push($data_array, $objSupportList);
    		}
    		return $data_array;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    /* Get business main category list
     */
    public function getBizCategory($id){
    	$objBizCategory = new stdClass();
    	try {
    		$SQL = "SELECT * FROM set_biz_category";
    		$SQL.= " WHERE id = ".$id;
    		$this->executeSelectQuery($SQL);
    		$categoryInfo = $this->getResult();
    		//var_dump($categoryInfo);exit();
    		foreach ($categoryInfo as $row => $catRow) {
    			$objBizCategory->name = $catRow['name'];
    			$objBizCategory->description = $catRow['description'];
    			$objBizCategory->subCategories = $this->getBizSubCategory($id);
    		}
    		return $objBizCategory;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    /* Get institute data
     */
    public function getInstitute($id){
    	$objInstitute = new stdClass();
    	try {
    		$SQL = "SELECT * FROM set_edu_institute";
    		
    	
    		$SQL.= " WHERE id = ".$id;
    		
    	    		$this->executeSelectQuery($SQL);
    		$instituteInfo = $this->getResult();
    		foreach ($instituteInfo as $row => $dataRow) {
    			$objInstitute->id = $dataRow['id'];
    			$objInstitute->name = $dataRow['name'];
    			$objInstitute->description = $dataRow['description'];
    		}
    		return $objInstitute;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    /* Get institute data
     */
    public function getStudyLevel($id){
    	$objStudyLevel = new stdClass();
    	try {
    		$SQL = "SELECT * FROM set_study_level";
    	
    		$SQL.= " WHERE id = ".$id;

    		$this->executeSelectQuery($SQL);
    		$dataInfo = $this->getResult();
    		foreach ($dataInfo as $row => $dataRow) {
    			$objStudyLevel->id = $dataRow['id'];
    			$objStudyLevel->institute = $dataRow['institute'];
    			$objStudyLevel->level = $dataRow['level'];
    			$objStudyLevel->description = $dataRow['description'];
    		}
    		return $objStudyLevel;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    
    
    /* Get study level on institute
     *  * by : Deen
     */
    public function getStudyLevelonInstitute($ins_id){
    	
    	$arrData = array();
    	try {
    		$SQL = "SELECT id FROM set_study_level";
    		
    		$SQL.= " WHERE institute = ".$ins_id;
    		$this->executeSelectQuery($SQL);
    		$rsltData = $this->getResult();
    		foreach ($rsltData As $dataRow) {
    			$id = $dataRow['id'];
    			$dataInfo = $this->getStudyLevel($id);
    			array_push($arrData, $dataInfo);
    		}
    		return $arrData;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /* Get study program
     */
    public function getStudyProgram($id){
    	$objStudyProgram = new stdClass();
    	try {
    		$SQL = "SELECT * FROM set_study_area";
    		$SQL.= " WHERE id = ".$id;
    		$this->executeSelectQuery($SQL);
    		$dataInfo = $this->getResult();
    		foreach ($dataInfo as $row => $dataRow) {
    			$objStudyProgram->id = $dataRow['id'];
    			$objStudyProgram->level = $dataRow['level'];
    			$objStudyProgram->program = $dataRow['program'];
    			$objStudyProgram->description = $dataRow['description'];
    		}
    		return $objStudyProgram;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    public function getBizSubCategory($id){
    	$data_array = array();
    	try {
    		$SQL = "SELECT * FROM set_biz_category";
    		$SQL.= " WHERE parent = ".$id;
    		$this->executeSelectQuery($SQL);
    		$categoryInfo = $this->getResult();
    		//var_dump($categoryInfo);exit();
    		foreach ($categoryInfo as $row => $catRow) {
    			$objBizSubCategory = new stdClass();
    			$objBizSubCategory->name = $catRow['name'];
    			$objBizSubCategory->description = $catRow['description'];
    			$objBizSubCategory->parent = $catRow['parent'];
    			array_push($data_array, $objBizSubCategory);
    		}
    		return $data_array;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    public function getAllBizCategory($parent=0) {
    	$arrBizCategory = array();
    	try {
    		$SQL = "SELECT id FROM set_biz_category";
    		$SQL.= " WHERE parent = 0";
    		$SQL.= ' ORDER BY id ASC';
    		$this->executeSelectQuery($SQL);
    		$catResult = $this->getResult();
    		foreach ($catResult As $catRow) {
    			$id = $catRow['id'];
    			$catInfo = $this->getBizCategory($id);
    			array_push($arrBizCategory, $catInfo);
    		}
    		return $arrBizCategory;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    public function getAllInstitues(){
    	$arrInstitute = array();
    	try {
    		$SQL = "SELECT id FROM set_edu_institute";
    		$SQL.= ' ORDER BY name ASC';
    		$this->executeSelectQuery($SQL);
    		$rsltData = $this->getResult();
    		foreach ($rsltData As $dataRow) {
    			$id = $dataRow['id'];
    			$dataInfo = $this->getInstitute($id);
    			array_push($arrInstitute, $dataInfo);
    		}
    		return $arrInstitute;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    
    /* Get all Level of Studies
     */
    public function getAllLevelOfStudies(){
    	$arrData = array();
    	try {
    		$SQL = "SELECT id FROM set_study_level";
    		$SQL.= ' ORDER BY institute ASC';
    		$this->executeSelectQuery($SQL);
    		$rsltData = $this->getResult();
    		foreach ($rsltData As $dataRow) {
    			$id = $dataRow['id'];
    			$dataInfo = $this->getStudyLevel($id);
    			array_push($arrData, $dataInfo);
    		}
    		return $arrData;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    
    /* Get Program List
     */
    public function getAllProgramList(){
    	$arrData = array();
    	try {
    		$SQL = "SELECT id FROM set_study_area";
    		$SQL.= ' ORDER BY program ASC';
    		$this->executeSelectQuery($SQL);
    		$rsltData = $this->getResult();
    		foreach ($rsltData As $dataRow) {
    			$id = $dataRow['id'];
    			$dataInfo = $this->getStudyProgram($id);
    			array_push($arrData, $dataInfo);
    		}
    		return $arrData;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    
    /* getAllProgramListOnInstitute
     * by : Deen
     */
    public function getAllProgramListOnInstitute ($level_id){
    	$arrData = array();
    	try {
    		$SQL = "SELECT id FROM set_study_area";
    		//$SQL.= ' ORDER BY program ASC';
    		$SQL.= " WHERE level = ".$level_id;
    		$this->executeSelectQuery($SQL);
    		$rsltData = $this->getResult();
    		foreach ($rsltData As $dataRow) {
    			$id = $dataRow['id'];
    			$dataInfo = $this->getStudyProgram($id);
    			array_push($arrData, $dataInfo);
    		}
    		return $arrData;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    
    
    
    
    
    
}
?>
