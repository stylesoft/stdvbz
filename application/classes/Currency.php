<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Currency extends Core_Database {

    //news propoerties
    public $id;
    public $name;
    public $code;
    public $codeStr;
    public $isDefault;
    public $error = array();
    public $data_array = array();

    //constructor
    public function Currency() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /*     * '
     * @name         :   addCurrency
     * @param        :   categoryObject
     * Description   :   The function is to category details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function addCurrency() {
        $recordId = null;
        try {
               
            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   editNews
     * @param        :   NewsObject
     * Description   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editCurrency() {
        $isUpdated = false;
        try {
                
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   deleteCurrency
     * @param        :   CurrencyObject
     * Description   :   The function is to delete Currency details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deleteCurrency() {
        $isDeleted = false;
        try {
            
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getCurrency
     * @param        :   Integer (Currency ID)
     * Description   :   The function is to get a Currency details
     * @return       :   Currency Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   29-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getCurrency($currencyId) {
        $objCurrency = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' .$currencyId;
                $this->select('tbl_currency', $colums, $where);
                $categoryInfo = $this->getResult();

                if($categoryInfo){
                    $objCurrency->id = $categoryInfo['id'];
                    $objCurrency->name = $categoryInfo['name'];
                    $objCurrency->code = $categoryInfo['currency_code'];
                    $objCurrency->codeStr = $categoryInfo['currency_code_str'];
                    $objCurrency->isDefault = $categoryInfo['is_default'];
                } else {
                	$objCurrency->id = null;
                    $objCurrency->name = null;
                    $objCurrency->code = null;
                    $objCurrency->codeStr = null;
                    $objCurrency->isDefault = null;
                }
            }
            return $objCurrency;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   getAll
     * @param        :
     * Description   :   The function is to get all category details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAll() {
        $arrCurrency = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = '';
                $orderBy = "id ASC";
                $this->select('tbl_currency', $colums, $where, $orderBy);
                $currencyResult = $this->getResult();
                foreach ($currencyResult As $currencyRow) {
                    $currencyId = $currencyRow['id'];
                    $currencyInfo = $this->getCurrency($currencyId);
                    array_push($arrCurrency, $currencyInfo);
                }
            }

            return $arrCurrency;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

}
?>