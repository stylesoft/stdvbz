<?php
/**
 * @name        HomePage
 * @copyright   2012 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1
 * @author     	Iyngaran Iyathurai
 *              Developer -  Monara IT UK Ltd
 *              iyngaran.iyathurai@monara.com
 *
 */

class HomePage extends Core_Database{


	/**'
	 * @name         :   addHomePage
	 * @param        :   HomePageObject
	 * Description   :   The function is to add a new page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function addHomePage($objHomePage){
		$recordId = null;
		try{
			if($this->connect()){
				$id 				= $objHomePage->id;
				$title 				= $objHomePage->title;
				$keywords 			= $objHomePage->keywords;
				$description 		        = $objHomePage->description;
				$body 				= $objHomePage->body;
				$name 				= $objHomePage->name;
                                $contentType 			= $objHomePage->contentType;
				$inserted = $this->insert('index_page',array($id,$title,$keywords,$description,$body,$name,$contentType)); 
				if($inserted){
					$recordId = $this->getLastInsertedId();
				} 
			}
			return $recordId;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>HomePage</em>, <strong>Function -</strong> <em>addHomePage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}


	/**'
	 * @name         :   editHomePage
	 * @param        :   HomePageObject
	 * Description   :   The function is to edit a page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function editHomePage($objHomePage){
		$isUpdated = false;
		try{
			if($this->connect()){
				$id 				= $objHomePage->id;
				$title 				= $objHomePage->title;
				$keywords 			= $objHomePage->keywords;
				$description 		        = $objHomePage->description;
				$body 				= $objHomePage->body;
				$name 				= $objHomePage->name;
				
				$arrayData          = array('TITLE'=>$title,
										'KEYWORDS'=>$keywords,
										'DESCRIPTION'=>$description,
										'BODY'=>$body,
										'NAME'=>$name);
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("ID = '" . $id . "'");
				$isUpdated = $this->update('index_page',$arrayData,$arrWhere);
			}
			return $isUpdated;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>HomePage</em>, <strong>Function -</strong> <em>addHomePage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}

	/**'
	 * @name         :   deleteHomePage
	 * @param        :   HomePageObject
	 * Description   :   The function is to add a new page details
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function deleteHomePage($objHomePage){
		$isDeleted    = false;
		try {
			if($this->connect()){
				$id 				= $objHomePage->id;
				$arrWhere  = array("ID = '" . $id . "'");
				$isDeleted = $this->delete('index_page',$arrWhere);
			}
			return $isDeleted;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>HomePage</em>, <strong>Function -</strong> <em>addHomePage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
		
	}


	/**'
	 * @name         :   getHomePage
	 * @param        :   Integer (HomePage ID)
	 * Description   :   The function is to get a page details
	 * @return       :   HomePage Object
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getHomePage($pageId){
		$objHomePage = new stdClass();
		try{
			if($this->connect()){
				$colums = '*';
				$where  = 'ID = '.$pageId;
				$this->select('index_page',$colums,$where);
				$pageInfo = $this->getResult();
					
				$objHomePage->id = $pageInfo['ID'];
				$objHomePage->title = $pageInfo['TITLE'];
				$objHomePage->keywords = $pageInfo['KEYWORDS'];
				$objHomePage->description = $pageInfo['DESCRIPTION'];
				$objHomePage->body = $pageInfo['BODY'];
				$objHomePage->listingId = null;
				$objHomePage->name = $pageInfo['NAME'];
				$objHomePage->live = null;
				$objHomePage->isContact = null;
				$objHomePage->allowToDelete = null;
			}
			return $objHomePage;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>HomePage</em>, <strong>Function -</strong> <em>getHomePage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}

	}

	/**'
	 * @name         :   getAll
	 * @param        :
	 * Description   :   The function is to get all page details
	 * @return       :   Array (Array Of HomePage Object)
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   16-08-2012
	 * Modified By   :   Iyngaran Iyathurai
	 * Modified On   :   16-08-2012
	 */
	public function getAll(){
		$arrHomePages       = array();
		try{
			if($this->connect()){
				$colums = 'ID';
				$where  = null;
                                $orderBy = " ListingID Asc";
				$this->select('pages',$colums,$where,$orderBy);
				$pageRes = $this->getResult();
					
				foreach($pageRes As $pIndex=>$pageRow){
					$pageId = $pageRow['ID'];
					$pageInfo = $this->getHomePage($pageId);
					array_push($arrHomePages,$pageInfo);
				}
					
			}
			return $arrHomePages;
		}catch (Exception $e){
			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>HomePage</em>, <strong>Function -</strong> <em>getHomePage()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
		}
	}
        
     
	 
	 
}
?>