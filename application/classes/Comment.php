<?php
/**
 * Description of Comment
 *
 * @author Gayan Chathuranga <gayan.chathuranga@monara.com>
 */
class Comment extends Core_Database{
    
    //class propoerties
    public $id; 		
    public $comment_content_id; 	
    public $comment_author; 	
    public $comment_author_email; 	
    public $comment_author_url; 	
    public $comment_author_ip; 	
    public $comment_date; 	
    public $comment_content; 	
    public $comment_status; 	
    public $comment_member_id; 	
    public $follow_up_comments_notification; 	
    public $new_posts_email_notification;
   
    
    
    //construct
    public function __construct() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /** '
     * @name         :   addComment
     * @param        :   postObject
     * Description   :   The function is to add a new post to the blog
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   19-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */
    public function addComment() {
        $recordId = null;
        try {
            $id = $this->id;
            $comment_content_id = $this->comment_content_id;
            $comment_author = $this->comment_author;
             $comment_author_email = $this->comment_author_email;
             $comment_date = $this->comment_date;
             $comment_status = $this->comment_status;
             $comment_content = $this->comment_content;
          /*  $comment_author_url = $this->comment_author_url;
            $comment_author_ip = $this->comment_author_ip;
           
            
            
            $comment_member_id = $this->comment_member_id;
            $follow_up_comments_notification = $this->follow_up_comments_notification;
            $new_posts_email_notification = $this->new_posts_email_notification;
 */
            
            $inserted = $this->insert($this->tb_name, array($id,$comment_content_id,$comment_author,$comment_author_email, '','', $comment_date,$comment_content,$comment_status, '', '', ''));
            
            if ($inserted) {
            	echo "Running";
                $recordId = $this->getLastInsertedId();
                
            }

            return $recordId;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Image</em>, <strong>Function -</strong> <em>addImage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /*     * '
     * @name         :   editComment
     * @param        :   ImageObject
     * Description   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   16-08-2012
     * Modified By   :   Iyngaran Iyathurai
     * Modified On   :   16-08-2012
     */

    public function editComment($id) {
        $isUpdated = false;
        try {
            $id = $this->id;
            $comment_content_id = $this->comment_content_id;
            $comment_author = $this->comment_author;
            $comment_author_email = $this->comment_author_email;
            $comment_author_url = $this->comment_author_url;
            $comment_author_ip = $this->comment_author_ip;
            $comment_date = $this->comment_date;
            $comment_content = $this->comment_content;
            $comment_status = $this->comment_status;
            $comment_member_id = $this->comment_member_id;
            $follow_up_comments_notification = $this->follow_up_comments_notification;
            $new_posts_email_notification = $this->new_posts_email_notification;


            $arrayData = array(
                
                'comment_author' => $comment_author,
            		'comment_author_email'=> $comment_author_email,
            		'comment_member_id' => $comment_member_id,
            		'comment_author_url' => $comment_author_url,
            		 'comment_author_ip' => $comment_author_ip,
            		'comment_date' => $comment_date,
            		'comment_content' => $comment_content,
            		'comment_status' => $comment_status,
            		'comment_member_id' => $comment_member_id,
            		'follow_up_comments_notification' => $follow_up_comments_notification,
            		'new_posts_email_notification' => $new_posts_email_notification 
               
            		
            );
            $arrWhere = array("id = '" . $id . "'");
            $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Image</em>, <strong>Function -</strong> <em>addImage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   deleteComment
     * @param        :   CommentObject
     * @desc         :   The function is to delete a comment from blog
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   19-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */
    public function deleteComment($id) {
        $isDeleted = false;
        try {
            $id = $this->id;
            $arrWhere = array("id = '" . $id . "'");
            $isDeleted = $this->delete($this->tb_name, $arrWhere);

            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    /** '
     * @name         :   getComment
     * @param        :   Integer (Page ID)
     * @desc         :   The function is to get a post
     * @return       :   PostObject
     * Added By      :   Gayan Chathuranga
     * Added On      :   19-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getComment($id) {
        $objPost = new stdClass();
        try {

                $colums = '*';
                $where = 'id = ' . $id;
                $this->select($this->tb_name, $colums, $where);
                $dataInfo = $this->getResult();

                $objPost->id = $dataInfo['id'];
                $objPost->comment_content_id = $dataInfo['comment_content_id'];
                $objPost->comment_author = $dataInfo['comment_author'];
                $objPost->comment_author_email = $dataInfo['comment_author_email'];
                $objPost->comment_author_url = $dataInfo['comment_author_url'];
                $objPost->comment_author_ip = $dataInfo['comment_author_ip'];
                $objPost->comment_date = $dataInfo['comment_date'];
                $objPost->comment_content = $dataInfo['comment_content'];   
                $objPost->comment_status = $dataInfo['comment_status'];
                $objPost->comment_member_id = $dataInfo['comment_member_id'];
                $objPost->follow_up_comments_notification = $dataInfo['follow_up_comments_notification'];
                $objPost->new_posts_email_notification = $dataInfo['new_posts_email_notification'];

            return $objPost;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    /** '
     * @name         :   getAll
     * @param        :
     * @desc         :   The function is to get all Post
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   19-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAll() {
        $arrPost = array();
        try {

                $colums = 'id';
                $where = 'comment_status = "Approved"';
                $orderBy = "id";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $postResult = $this->getResult();
                foreach ($postResult As $postRow) {
                    $id = $postRow['id'];
                    $postInfo = $this->getPost($id);
                    array_push($arrPost, $postInfo);
                }
            return $arrPost;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    /** '
     * @name         :   getByStatus
     * @param        :   $status = Approved/Pending
     * @desc         :   The function is to get all Post
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   19-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getByStatus($status = 'Approved') {
    	$status=$status;
        $arrComment = array();
        try {
                $colums = '*';
                $where = array("comment_status = '" . $status . "'");
                $orderBy = "comment_date DESC";
               //echo($this->tb_name);
             // $arrWhere = array("id = '" . $id . "'");
               
               $this->select($this->tb_name, $colums, $where,$orderBy);
                //$this->select('tbl_blog_comments', 1, 'Approved','comment_date DESC');
                $commentResult = $this->getResult();
                foreach ($commentResult As $commentRow) {
                    $id = $commentRow['id'];
                    $commentInfo = $this->getComment($id);
                    array_push($arrComment, $commentInfo);
                
                }

            return $arrComment;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    /** '
     * @name         :   countRec
     * @param        :   PostObject
     * @desc         :   The function is to count the No of comments
     * @return       :   Integer (Total number Of News)
     * Added By      :   Gayan Chathuranga
     * Added On      :   19-09-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function countRec() {
        $totalNumberOfRec = 0;
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "comment_author LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            $dbResult = $this->executeSelectQuery($SQL);
            $dataRes = $this->getResult();
            $totalNumberOfRec = count($dataRes);
            return $totalNumberOfRec;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
     /** '
     * @name         :   search
     * @param        :
     * Description   :   The function is to search  comment details
     * @return       :   Array (Array Of RestaurantMenu Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   19-09-2012
     * Modified By   :   Iyngaran Iyathurai
     * Modified On   :   22-08-2012
     */

    public function search() {
        $arrComment = array();
        $arrWhere = array();
        //$arrWhere = array("comment_status = '" . $status . "'");
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "comment_author LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            if ($this->listingOrder) {
                $SQL.= ' ORDER BY ' . $this->listingOrder;
            }

            if ($this->limit) {
                $SQL.= $this->limit;
            }
            //echo $SQL;
            $dbResult = $this->executeSelectQuery($SQL);
            $dataRes = $this->getResult();
            foreach ($dataRes As $dataRow) {
                $id = $dataRow['id'];
                $commentInfo = $this->getComment($id);
                array_push($arrComment, $commentInfo);
            }
            return $arrComment;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    /** '
     * @name         :   searchBystatus
     * @param        :
     * Description   :   The function is to search  comment details
     * @return       :   Array (Array Of RestaurantMenu Object)
     * Added By      :   Zumry deen
     * Added On      :   
  
     */
    public function searchBystatus($status,$bid) {
    	$arrComment = array();
    	//$arrWhere = array();
    	$arrWhere = array("comment_status = '" . $status . "'","comment_content_id ='" . $bid . "'");
    	try {
    		$SQL = "SELECT * FROM $this->tb_name ";
    		if ($this->searchStr != '') {
    			array_push($arrWhere, "comment_author LIKE '" . "%" . $this->searchStr . "%" . "'");
    		}
    
    		if (count($arrWhere) > 0)
    			$SQL.= "WHERE " . implode(' AND ', $arrWhere);
    
    
    		if ($this->listingOrder) {
    			$SQL.= ' ORDER BY ' . $this->listingOrder;
    		}
    
    		if ($this->limit) {
    			$SQL.= $this->limit;
    		}
    		//echo $SQL;
    		$dbResult = $this->executeSelectQuery($SQL);
    		$dataRes = $this->getResult();
    		foreach ($dataRes As $dataRow) {
    			$id = $dataRow['id'];
    			$commentInfo = $this->getComment($id);
    			array_push($arrComment, $commentInfo);
    		}
    		return $arrComment;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    /** '
     * @name         :   searchBystatus
     * @param        :
     * Description   :   The function is to search  comment details
     * @return       :   Array (Array Of RestaurantMenu Object)
     * Added By      :   Zumry deen
     * Added On      :
    
     */
    
    public function searchByblogID($bid) {
    	$arrComment = array();
    	//$arrWhere = array();
    	$arrWhere = array("comment_content_id =$bid");
    	try {
    		$SQL = "SELECT * FROM $this->tb_name ";
    		if ($this->searchStr != '') {
    			array_push($arrWhere, "comment_author LIKE '" . "%" . $this->searchStr . "%" . "'");
    		}
    
    		if (count($arrWhere) > 0)
    			$SQL.= "WHERE " . implode(' AND ', $arrWhere);
    
    
    		if ($this->listingOrder) {
    			$SQL.= ' ORDER BY ' . $this->listingOrder;
    		}
    
    		if ($this->limit) {
    			$SQL.= $this->limit;
    		}
    		//echo $SQL;
    		$dbResult = $this->executeSelectQuery($SQL);
    		$dataRes = $this->getResult();
    		foreach ($dataRes As $dataRow) {
    			$id = $dataRow['id'];
    			$commentInfo = $this->getComment($id);
    			array_push($arrComment, $commentInfo);
    		}
    		return $arrComment;
    	} catch (Exception $e) {
    		throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
    	}
    }
    
    
    
    
    
    
}

?>
