<?php

/**
 * @name        Newas
 * @copyright   2012 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1
 * @author     	Gayan Chathuranga
 *              Developer -  Monara IT UK Ltd
 *              gayan.chathuranga@monara.com
 * {
 */
class PageContent extends Core_Database {

    //siteContent propoerties
    public $id;
    public $title;
    public $displayOrder;
    public $contents;
    public $contentType;
    public $pageId;
    public $contentMainImage;
    public $contentLink;
    public $pageType;
   
    

    //constructor

    public function PageContent() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    public function addPageContent() {
        $recordId = null;
        try {
                $id = $this->id;
                $title = $this->title;
                $displayOrder = $this->displayOrder;
                $contents = $this->contents;
                $contentType = $this->contentType;
                $pageId = $this->pageId;
                $contentMainImage = $this->contentMainImage;
                $contentLink = $this->contentLink;
                $pageType  = $this->pageType;
               
                $inserted = $this->insert($this->tb_name, array($id,$title,$displayOrder,$contents,$contentType,$pageId,$contentMainImage,$contentLink,$pageType));
                if ($inserted) {
                    $recordId = $this->getLastInsertedId();
                }
            return $recordId;
        } catch (Exception $e) {
            echo $e->message;
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   editPageContent
     * @param        :   PageContentObject
     * @desc   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editPageContent() {
        $isUpdated = false;
        try {

                $id = $this->id;
                $name = $this->name;
                $title = $this->title;
                $displayOrder = $this->displayOrder;
                $contents = $this->contents;
                $contentType = $this->contentType;
                $pageId = $this->pageId;
                $contentMainImage = $this->contentMainImage;
                $contentLink = $this->contentLink;
                $pageType  = $this->pageType;

                $arrayData = array(
                    'title' => $this->title,
                    'display_order' => $this->displayOrder,
                    'contents' => $this->contents,
                    'content_type' => $this->contentType,
                    'page_id' => $this->pageId,
                    'content_main_image' => $this->contentMainImage,
                    'content_link' => $this->contentLink,
                    'page_type'  => $pageType 
                );
                
                //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
                $arrWhere = array("id = '" . $id . "'");
                $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    /**'
	 * @name         :   updateOrder
	 * @param        :   PageContentObject
	 * @desc   :   The function is to edit a PageContent Listing order
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   04-09-2012
	 * Modified By   :   Gayan Chathuranga
	 * Modified On   :   04-09-2012
	 */
	public function updateOrder(){
		$isUpdated = false;
		try{
			
				$id 				= $this->id;                               
				$displayOrder 			= $this->displayOrder;                               
				$arrayData          = array('display_order'=>$displayOrder);
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("id = '" . $id . "'");
                                
				$isUpdated = $this->update($this->tb_name,$arrayData,$arrWhere);
			
			return $isUpdated;
		}catch (Exception $e){		}

			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>RestaurantMenu</em>, <strong>Function -</strong> <em>updateOrder()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
	}

        
        

    /** '
     * @name         :   deletePageContent
     * @param        :   PageContentObject
     * @desc   :   The function is to delete siteContent details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deletePageContent() {
        $isDeleted = false;
        try {
                $id = $this->id;
                $arrWhere = array("id = '" . $id . "'");
                $isDeleted = $this->delete($this->tb_name, $arrWhere);
            
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   getPageContent
     * @param        :   Integer (Page ID)
     * @desc   :   The function is to get a PageContent details
     * @return       :   PageContent Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getPageContent($siteContentId) {
        $objPageContent = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' . $siteContentId;
                $this->select($this->tb_name, $colums, $where);
                $siteContentInfo = $this->getResult();
                if($siteContentInfo){
                $objPageContent->id = $siteContentInfo['id'];
                $objPageContent->name = $siteContentInfo['name'];
                $objPageContent->title = $siteContentInfo['title'];
                $objPageContent->displayOrder = $siteContentInfo['display_order'];
                $objPageContent->contents = $siteContentInfo['contents'];
                $objPageContent->contentType = $siteContentInfo['content_type'];
                $objPageContent->pageId = $siteContentInfo['page_id'];
                 $objPageContent->contentMainImage = $siteContentInfo['content_main_image'];
                $objPageContent->contentLink = $siteContentInfo['content_link'];
                }
            }
            return $objPageContent;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   getAll
     * @param        :
     * @desc   :   The function is to get all siteContent details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAll() {
        $arrPageContent = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = 'siteContent_status = 1';
                $orderBy = " added_date Asc";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $siteContentResult = $this->getResult();
                foreach ($siteContentResult As $siteContentRow) {
                    $siteContentId = $siteContentRow['id'];
                    $siteContentInfo = $this->getPageContent($siteContentId);
                    array_push($arrPageContent, $siteContentInfo);
                }
            }

            return $arrPageContent;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   getAllByStatus
     * @param        :
     * @desc   :   The function is to get all PageContent details by status
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllByPageId($pageId) {
        $arrPageContent = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = "page_id = '" . $pageId . "'";
                $orderBy = " display_order Desc";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $siteContentRes = $this->getResult();

                foreach ($siteContentRes As $nIndex => $siteContentRow) {
                    $siteContentId = $siteContentRow['id'];
                    $siteContentInfo = $this->getPageContent($siteContentId);
                    array_push($arrPageContent, $siteContentInfo);
                }
            }
            return $arrPageContent;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    
    
    /** '
     * @name         :   search
     * @param        :
     * Description   :   The function is to search  siderBarContent details
     * @return       :   Array (Array Of RestaurantMenu Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   19-09-2012
     * Modified By   :   Iyngaran Iyathurai
     * Modified On   :   22-08-2012
     */

    public function getAllByPageIdAndType($pageId,$contentType,$pageType='') {
        $arrPageContents = array();
        $arrWhere = array();
        try {
            $SQL = "SELECT id FROM $this->tb_name ";
            
            
            if ($pageId != '') {
                array_push($arrWhere, " page_id = '" . $pageId . "'");
            }
            
            if ($contentType != '') {
                array_push($arrWhere, " content_type = '" . $contentType . "'");
            }
            
            
            if ($pageType != '') {
                array_push($arrWhere, " page_type = '" . $pageType . "'");
            }
            
            
            
            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);

            
            //echo $SQL; 
            $dbResult = $this->executeSelectQuery($SQL);
            $dataRes = $this->getResult();
         
            foreach ($dataRes As $dataRow) {
                $siteContentId = $dataRow['id'];
                $pageContentInfo = $this->getPageContent($siteContentId);
                if($pageContentInfo){
                    array_push($arrPageContents, $pageContentInfo);
                }
            }
            return $arrPageContents;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
     
}
?>