<?php

/**
 * @name        Newas
 * @copyright   2012 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1
 * @author     	Gayan Chathuranga
 *              Developer -  Monara IT UK Ltd
 *              gayan.chathuranga@monara.com
 * {
 */
class Services extends Core_Database {

    //services propoerties
    public $id;
    public $added_date;
    public $services_description;
    public $services_content;
    public $services_title;
    public $services_keywords;
    public $services_modified;
    public $services_author;    
    public $listingId;
    public $services_status;
    public $servicesImageIcon;
    
    
    public $error = array();
    public $data_array = array();
    
    


    //constructor

    public function Services() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    public function addServices() {
        $recordId = null;
        try {
                $id = $this->id;
                $added_date = $this->added_date;
                $services_description = $this->services_description;
                $services_content = $this->services_content;
                $services_title = $this->services_title;
                $services_keywords = $this->services_keywords;
                $services_modified = $this->services_modified;
                $services_author = $this->services_author;
                $listingId  = $this->listingId;
                $services_status = $this->services_status;
                $servicesImageIcon = $this->servicesImageIcon;

                $inserted = $this->insert($this->tb_name, array($id, $added_date, $services_description, $services_content, $services_title, $services_keywords, $services_modified, $services_author, $listingId, $services_status,$servicesImageIcon));
                if ($inserted) {
                    $recordId = $this->getLastInsertedId();
                }
            return $recordId;
        } catch (Exception $e) {
            echo $e->message;
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   editServices
     * @param        :   ServicesObject
     * @desc   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editServices() {
        $isUpdated = false;
        try {

                $id = $this->id;
                $added_date = $this->added_date;
                $services_description = $this->services_description;
                $services_content = $this->services_content;
                $services_title = $this->services_title;
                $services_keywords = $this->services_keywords;
                $services_modified = $this->services_modified;
                $services_author = $this->services_author;
                $listingId  = $this->listingId;
                $services_status = $this->services_status;
                $servicesImageIcon = $this->servicesImageIcon;

                $arrayData = array(
                    'id' => $id,
                    'added_date' => $added_date,
                    'services_description' => $services_description,
                    'services_content' => $services_content,
                    'services_title' => $services_title,
                    'services_keywords' => $services_keywords,
                    'services_modified' => $services_modified,
                    'services_author' => $services_author,
                    'listingId' => $listingId,
                    'services_status' => $services_status,
                    'image_icon' => $servicesImageIcon
                );
                //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
                $arrWhere = array("id = '" . $id . "'");
                $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    /**'
	 * @name         :   updateOrder
	 * @param        :   ServicesObject
	 * @desc   :   The function is to edit a Services Listing order
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   04-09-2012
	 * Modified By   :   Gayan Chathuranga
	 * Modified On   :   04-09-2012
	 */
	public function updateOrder(){
		$isUpdated = false;
		try{
			
				$id 				= $this->id;                               
				$listingId 			= $this->listingId;                               
				$arrayData          = array('listingId'=>$listingId);
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("id = '" . $id . "'");
                                
				$isUpdated = $this->update($this->tb_name,$arrayData,$arrWhere);
			
			return $isUpdated;
		}catch (Exception $e){		}

			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>RestaurantMenu</em>, <strong>Function -</strong> <em>updateOrder()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
	}

        
        

    /** '
     * @name         :   deleteServices
     * @param        :   ServicesObject
     * @desc   :   The function is to delete services details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deleteServices() {
        $isDeleted = false;
        try {
                $id = $this->id;
                $arrWhere = array("id = '" . $id . "'");
                $isDeleted = $this->delete($this->tb_name, $arrWhere);
            
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   getServices
     * @param        :   Integer (Page ID)
     * @desc   :   The function is to get a Services details
     * @return       :   Services Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getServices($servicesId) {
        $objServices = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' . $servicesId;
                $this->select($this->tb_name, $colums, $where);
                $servicesInfo = $this->getResult();

                $objServices->id = $servicesInfo['id'];
                $objServices->added_date = $servicesInfo['added_date'];
                $objServices->services_description = $servicesInfo['services_description'];
                $objServices->services_content = $servicesInfo['services_content'];
                $objServices->services_title = $servicesInfo['services_title'];
                $objServices->services_keywords = $servicesInfo['services_keywords'];
                $objServices->services_modified = $servicesInfo['services_modified'];
                $objServices->services_author = $servicesInfo['services_author'];
                $objServices->listingId  =  $servicesInfo['listingId'];
                $objServices->services_status = $servicesInfo['services_status'];
                $objServices->servicesImageIcon = $servicesInfo['image_icon'];
            }
            return $objServices;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   getAll
     * @param        :
     * @desc   :   The function is to get all services details
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAll() {
        $arrServices = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = 'services_status = 1';
                $orderBy = " added_date Asc";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $servicesResult = $this->getResult();
                foreach ($servicesResult As $servicesRow) {
                    $servicesId = $servicesRow['id'];
                    $servicesInfo = $this->getServices($servicesId);
                    array_push($arrServices, $servicesInfo);
                }
            }

            return $arrServices;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   getAllByStatus
     * @param        :
     * @desc   :   The function is to get all Services details by status
     * @return       :   Array (Array Of Page Object)
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAllByStatus($status) {
        $arrServices = array();
        try {
            if ($this->connect()) {
                $colums = 'id';
                $where = "services_status = '" . $status . "'";
                $orderBy = " added_date Asc";
                $this->select($this->tb_name, $colums, $where, $orderBy);
                $servicesRes = $this->getResult();

                foreach ($servicesRes As $nIndex => $servicesRow) {
                    $servicesId = $servicesRow['id'];
                    $servicesInfo = $this->getServices($servicesId);
                    array_push($arrServices, $servicesInfo);
                }
            }
            return $arrServices;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   countRec
     * @param        :Restaurant Menu
     * @desc         :   The function is to count the Services details
     * @return       :   Integer (Total number Of Services)
     * Added By      :   Gayan Chathuranga
     * Added On      :   22-08-2012
     * Modified By   :   Gayan Chathuranga
     * Modified On   :   04-09-2012
     */

    public function countRec() {
        $totalNumberOfRec = 0;
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "services_title LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            $dbResult = $this->executeSelectQuery($SQL);
            $servicesRes = $this->getResult();
            $totalNumberOfRec = count($servicesRes);
            return $totalNumberOfRec;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   search
     * @param        :
     * Description   :   The function is to search  Restaurant Menu details
     * @return       :   Array (Array Of RestaurantMenu Object)
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   22-08-2012
     * Modified By   :   Iyngaran Iyathurai
     * Modified On   :   22-08-2012
     */

    public function search() {
        $arrServices = array();
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "services_title LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            if ($this->listingOrder) {
                $SQL.= ' ORDER BY ' . $this->listingOrder;
            }

            if ($this->limit) {
                $SQL.= $this->limit;
            }
            //echo $SQL;
            $dbResult = $this->executeSelectQuery($SQL);
            $servicesRes = $this->getResult();
            foreach ($servicesRes As $servicesRow) {
                $servicesId = $servicesRow['id'];
                $servicesInfo = $this->getServices($servicesId);
                array_push($arrServices, $servicesInfo);
            }
            return $arrServices;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
     
}
?>