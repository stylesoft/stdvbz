<?php

/**
 * @name        Newas
 * @copyright   2012 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1
 * @author     	Gayan Chathuranga
 *              Developer -  Monara IT UK Ltd
 *              gayan.chathuranga@monara.com
 * {
 */
class Album extends Core_Database {

    //news propoerties
    public $id;
    public $albumTitle; 
    public $albumKeywords;
    public $albumDescription;   
    public $albumOrder;
    public $albumStatus;
    public $searchStr;
    public $limit;
    public $albumImage;
    
    public $error = array();
    public $data_array = array();
    
    


    //constructor

    public function __construct() {
        try {
            parent::connect();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    public function addAlbum() {
        $recordId = null;
        try {
                $id = $this->id;
                $albumTitle = $this->albumTitle;
                $albumOrder = $this->albumOrder;
                $albumStatus = $this->albumStatus;
                $albumKeywords = $this->albumKeywords;
                $albumDescription = $this->albumDescription;
                $albumImage = $this->albumImage;
                $inserted = $this->insert($this->tb_name, array($id,$albumTitle,$albumStatus,$albumOrder,$albumKeywords,$albumDescription,$albumImage));
                if ($inserted) {
                    $recordId = $this->getLastInsertedId();
                }
            return $recordId;
        } catch (Exception $e) {
            echo $e->message;
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   editAlbum
     * @param        :   AlbumObject
     * @desc   :   The function is to edit a page details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function editAlbum() {
        $isUpdated = false;
        try {

               	$id = $this->id;
                $albumTitle = $this->albumTitle;
                $albumOrder = $this->albumOrder;
                $albumStatus = $this->albumStatus;
                $albumKeywords = $this->albumKeywords;
                $albumDescription = $this->albumDescription;
                $albumImage = $this->albumImage;
                
                $arrayData = array(
                    'id' => $id,
                    'album_title' => $albumTitle,
                    'status' => $albumStatus,
                    'display_order' => $albumOrder,
                    'keywords' => $albumKeywords,
                    'description' => $albumDescription,
                    'album_image' => $albumImage
                );
                //$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
                $arrWhere = array("id = '" . $id . "'");
                $isUpdated = $this->update($this->tb_name, $arrayData, $arrWhere);
            return $isUpdated;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    /**'
	 * @name         :   updateOrder
	 * @param        :   AlbumObject
	 * @desc   :   The function is to edit a Album Listing order
	 * @return       :   boolean
	 * Added By      :   Iyngaran Iyathurai
	 * Added On      :   04-09-2012
	 * Modified By   :   Gayan Chathuranga
	 * Modified On   :   04-09-2012
	 */
	public function updateOrder(){
		$isUpdated = false;
		try{
			
				$id 				= $this->id;                               
				$albumOrder 		= $this->albumOrder;                               
				$arrayData          = array('display_order'=>$albumOrder);
				//$this->update('pages',array('name'=>'Changed!'),array("ID = '" . 44 . "'","NAME = 'xxx'"));
				$arrWhere  = array("id = '" . $id . "'");
                                
				$isUpdated = $this->update($this->tb_name,$arrayData,$arrWhere);
			
			return $isUpdated;
		}catch (Exception $e){		}

			throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>RestaurantMenu</em>, <strong>Function -</strong> <em>updateOrder()</em>, <strong>Exception -</strong> <em>".$e->getMessage()."</em>");
	}

        
        

    /** '
     * @name         :   deleteAlbum
     * @param        :   AlbumObject
     * @desc   :   The function is to delete news details
     * @return       :   boolean
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function deleteAlbum() {
        $isDeleted = false;
        try {
                $id = $this->id;
                $arrWhere = array("id = '" . $id . "'");
                $isDeleted = $this->delete($this->tb_name, $arrWhere);
            
            return $isDeleted;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>addPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   getAlbum
     * @param        :   Integer (Page ID)
     * @desc   :   The function is to get a Album details
     * @return       :   Album Object
     * Added By      :   Gayan Chathuranga
     * Added On      :   28-08-2012
     * Modified By   :   -
     * Modified On   :   -
     */

    public function getAlbum($albumId) {
        $objAlbum = new stdClass();
        try {
            if ($this->connect()) {
                $colums = '*';
                $where = 'id = ' . $albumId;
                $this->select($this->tb_name, $colums, $where);
                $albumInfo = $this->getResult();

                $objAlbum->id = $albumInfo['id'];
                $objAlbum->albumTitle = $albumInfo['album_title'];
                $objAlbum->albumStatus = $albumInfo['status'];
                $objAlbum->albumOrder = $albumInfo['display_order'];
                $objAlbum->albumKeywords = $albumInfo['keywords'];
                $objAlbum->albumDescription = $albumInfo['description'];
                $objAlbum->albumImage = $albumInfo['album_image'];
            }
            return $objAlbum;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

   

    /** '
     * @name         :   countRec
     * @param        :Restaurant Menu
     * @desc         :   The function is to count the Album details
     * @return       :   Integer (Total number Of Album)
     * Added By      :   Gayan Chathuranga
     * Added On      :   22-08-2012
     * Modified By   :   Gayan Chathuranga
     * Modified On   :   04-09-2012
     */

    public function countRec() {
        $totalNumberOfRec = 0;
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "album_title LIKE '" . "%" . $this->searchStr . "%" . "'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            $dbResult = $this->executeSelectQuery($SQL);
            $newsRes = $this->getResult();
            $totalNumberOfRec = count($newsRes);
            return $totalNumberOfRec;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /** '
     * @name         :   search
     * @param        :
     * Description   :   The function is to search  Restaurant Menu details
     * @return       :   Array (Array Of RestaurantMenu Object)
     * Added By      :   Iyngaran Iyathurai
     * Added On      :   22-08-2012
     * Modified By   :   Iyngaran Iyathurai
     * Modified On   :   22-08-2012
     */

    public function search() {
        $arrAlbum = array();
        $arrWhere = array();
        try {
            $SQL = "SELECT * FROM $this->tb_name ";
            if ($this->searchStr != '') {
                array_push($arrWhere, "album_title LIKE '" . "%" . $this->searchStr . "%" . "' ");
            }
            
            if ($this->albumStatus != '') {
                array_push($arrWhere, "status = '".$this->albumStatus ."'");
            }

            if (count($arrWhere) > 0)
                $SQL.= "WHERE " . implode(' AND ', $arrWhere);


            if ($this->albumOrder) {
                $SQL.= ' ORDER BY ' . $this->albumOrder;
            }

            if ($this->limit) {
                $SQL.= $this->limit;
            }
            //echo $SQL;
            $dbResult = $this->executeSelectQuery($SQL);
            $newsRes = $this->getResult();
            foreach ($newsRes As $newsRow) {
                $newsId = $newsRow['id'];
                $newsInfo = $this->getAlbum($newsId);
                array_push($arrAlbum, $newsInfo);
            }
            return $arrAlbum;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
     
}
?>