<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <title>Student Vibez Bradford</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet' type='text/css'>

  <!-- Stylesheets -->
  <!-- Bootstrap -->
  <link href="<?php print(FRONT_LAYOUT_URL);?>contents/css/bootstrap.css" rel="stylesheet">
  <!-- Font awesome icon -->
 <!-- <link rel="stylesheet" href="style/font-awesome.css">-->
  <!-- Navigation menu -->
  <link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/css/ddlevelsmenu-base.css">
  <link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/css/ddlevelsmenu-topbar.css">
  <!-- cSlider -->
  <link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/css/slider.css">
  <!-- PrettyPhoto -->
  <link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/css/prettyPhoto.css">
  <!-- Custom style -->
  <link href="<?php print(FRONT_LAYOUT_URL);?>contents/css/style.css" rel="stylesheet">
  
  
  
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon -->
  <link rel="shortcut icon" href="<?php print(SITE_BASE_URL);?>img/favicon/favicon.png">
  
  <link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/font/font-awesome.css">
  
</head>

<body>

  <!-- Sliding panel starts-->

  <div class="slidepanel">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div class="spara"> 
            <!-- Contact details -->
            <p><i class="icon-envelope-alt lightblue"></i> <a>help@studentvibez.com</a> &nbsp; 
              <i class="icon-twitter lightblue"></i> <a>@StudentVibez</a> &nbsp; 
              <i class="icon-lock lightblue"></i> <a>Login</a> / <a>Register</a>
            </p>
          </div>
        </div>
        <div class="col-md-4">
            <div class="social">
              <!-- Social media icons. Repalce # with your profile links -->
                      <a href="#" class="blightblue"><i class="icon-facebook"></i></a>
                      <a href="#" class="blightblue"><i class="icon-google-plus"></i></a> 
                      <a href="#" class="blightblue"><i class="icon-twitter"></i></a>
                      <a href="#" class="blightblue"><i class="icon-linkedin"></i></a>
                      <a href="#" class="blightblue"><i class="icon-pinterest"></i></a>
                      <a href="#" class="blightblue"><i class="icon-rss"></i></a>
            </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>

  <!-- Sliding panel ends-->

  <!-- Header starts -->

  <header>
    <div class="container">
      <div class="row">

        <div class="col-md-3 col-sm-4">

          <!-- Logo starts -->

          <div class="logo">
				<div> <a href="index.html"><img src="<?php print(SITE_BASE_URL);?>img/logo.png"  alt=""> </a></div>
            <!--<div class="logo-image">
               Image link 
              <a href="index.html"><i class="icon-desktop blue"></i></a>
            </div>-->
            
            <!--<div class="logo-text">
              <h1></h1>
              <a href="index.html">Student<span class="lightblue">Vibez</span></a>
              
              <div class="logo-meta">&#35;1 Student HubSpot</div>
            </div>-->

            <div class="clearfix"></div>

          </div>

          <!-- Logo ends -->

        </div>

     <?php require_once(FRONT_LAYOUT_VIEW_PATH .'common/menu.tpl.php'); ?>

      </div>
    </div>
  </header>

  <div class="clearfix"></div>

  <!-- Header ends -->

<!-- Slider starts -->

  <!-- content  -->
  
    <?php include($CONTENT); ?>
    <!-- end content -->
  

<?php require_once(FRONT_LAYOUT_VIEW_PATH .'common/footer.tpl.php'); ?>

<!-- JS -->
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/jquery.js"></script> <!-- jQuery -->
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/bootstrap.js"></script> <!-- Bootstrap -->
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/jquery.prettyPhoto.js"></script> <!-- prettyPhoto -->
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/jquery.isotope.js"></script> <!-- isotope -->
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/ddlevelsmenu.js"></script> <!-- Navigation menu -->
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/jquery.cslider.js"></script> <!-- jQuery cSlider -->
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/modernizr.custom.28468.js"></script> <!-- Extra script for cslider -->

<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/filter.js"></script> <!-- Support -->
<script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/custom.js"></script> <!-- Custom JS -->
</body>
</html>