<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <title>Student Vibez - The Number 1 Student Hubspot</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- CSS -->
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans:400,700'>
        <link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/prettyPhoto/css/prettyPhoto.css">
        <link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/font-awesome/css/font-awesome.css">
        <link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/css/social-icons.css">
        <link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/css/style.css">
        <link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/css/reset.css">


        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="<?php print(FRONT_LAYOUT_URL);?>contents/assets/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php print(FRONT_LAYOUT_URL);?>contents/assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php print(FRONT_LAYOUT_URL);?>contents/assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php print(FRONT_LAYOUT_URL);?>contents/assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php print(FRONT_LAYOUT_URL);?>contents/assets/ico/apple-touch-icon-57-precomposed.png">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    </head>

    <body>

        <!-- Header -->

<?php require_once(FRONT_LAYOUT_VIEW_PATH .'common/menu.tpl.php'); ?>


        <!-- Product Showcase -->
        <?php include($CONTENT); ?>

        <!-- Content -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="span4 from-twitter">
                        <h3>From Twitter</h3>
                        <div class="show-tweet"></div>
                    </div>
                    <div class="span4 testimonials">
                        <h3>Testimonials</h3>
                        <p>" Student Vibez is a great new iniatative that brings students together and provides useful resources.    "</p>
                        <div class="test-author">
                            <img src="/img/testimonials/2.jpg" alt="">
                            <p><span>NAZMUL MONJUH</span><br>Student Vibez.</p>
                        </div>
                    </div>
                    <div class="span4 contact-us">
                        <h3>Contact Us</h3>
                        <div id="formcontl">
                             <div class="alert-success" style="display:none;">
                                    <h3>It's Done!!</h3>
                                    <p>We will be in touch with you soon</p>
                            </div>
                            <div class="alert-error" style="display:none;">
                                    <h3>Oops Something went wrong!!</h3>
                                    <p>Please try again</p>
                             </div>
                             <div class="divregform">
                                 <form id="formcont">
                                <input type="text" name="name" placeholder="Name">
                                <input type="text" name="email" placeholder="Email">
                                <textarea name="message" placeholder="Message"></textarea>
                                <button type="submit">Submit</button>
                            </form>
                             </div>
                        </div>
                        
                    </div>
                </div> <!-- end row -->
                <div class="row">
                    <div class="span12">
                        <div class="social-links">
                            <a class="facebook" href="https://www.facebook.com/pages/Student-Vibez/241952939296863"></a>
                            <a class="twitter" href="https://twitter.com/StudentVibez"></a>
                           <!-- <a class="dribbble" href="#"></a>-->
                            <!--<a class="pinterest" href="#"></a>-->
                            <a class="googleplus" href="https://plus.google.com/u/0/b/104289261693150796685/"></a>
                           <!-- <a class="skype" href="#"></a>-->
                            <a class="youtube" href="http://www.youtube.com/channel/UCscAj7_CE01A_PuMbo4M4fg"></a>
                           <!-- <a class="vimeo" href="#"></a>
                            <a class="forrst" href="#"></a>-->
                            <a class="email" href="mailto:info@studentvibez.com"></a>
                        </div>
                    </div>
                </div> <!-- end row -->
                <div class="row">
                    <div class="span12 copyright">
                        <p>Copyright &copy; 2014 - Student Vibez Ltd. - All right reserved.<br>Student Vibez Ltd. is a company registered in England and Wales Registered Number: 08826105 <br>Registered Office: 6 Southbrook Terrace, Bradford, BD7 1AB <br>Powered by <a href="http://pluspro.com" target="_blank">PlusPro</a></p>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </footer>


        <!-- Javascript -->
        <script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/jquery-1.8.2.min.js"></script>
        <script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/jquery.backstretch.min.js"></script>
        <script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/jquery.tweet.js"></script>
        <script src="<?php print(FRONT_LAYOUT_URL);?>contents/js/ebook-scripts.js"></script>
        <script src="<?php print(FRONT_LAYOUT_URL);?>contents/prettyPhoto/js/jquery.prettyPhoto.js"></script>
      
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45750554-1', 'studentvibez.com');
  ga('send', 'pageview');

</script>
<script type="text/javascript">
        $(document).ready(function() {
                // validate site_info on keyup and submit
                $("#formreg").validate({
                    rules: {
                        fname: {
                            required : true
                        },
                        lname: {
                            required : true
                        },
                        email: {
                            required : true,
                            email : true
                        } 

                    },
                    submitHandler: function() {                
                        $.ajax({
                            type: "POST",
                            url: "<?php  echo SITE_BASE_URL  ?>sendmail.php",
                            data: $("#formreg").serialize(), // serializes the form's elements. http://api.jquery.com/serialize/
                            success: function(Res) {
                                $('#formregstl .divregform').css('display','none');
                                $('#formregstl .alert-success').css('display','block');

                            },
                            error: function(Res) {
                                $('#formregstl .divregform').css('display','none');
                                $('#formregstl .alert-error').css('display','block'); 
                            }
                        });
                        return false;
                    },
                    errorPlacement: function(error, element) { //element.prop("placeholder", error.text()); overide the error 
                    }
                });

                //Concatct Form
                $("#formcont").validate({
                    rules: {
                        name: {
                            required : true
                        },
                        message: {
                            required : true
                        },
                        email: {
                            required : true,
                            email : true
                        } 

                    },
                    submitHandler: function() {                
                        $.ajax({
                            type: "POST",
                            url: "<?php echo SITE_BASE_URL;?>sendmail.php?c=contact",
                            data: $("#formcont").serialize(), // serializes the form's elements. http://api.jquery.com/serialize/
                            success: function(Res) {
                                $('#formcontl .divregform').css('display','none');
                                $('#formcontl .alert-success').css('display','block');
                            },
                            error: function(Res) {
                                $('#formcontl .divregform').css('display','none');
                                $('#formcontl .alert-error').css('display','block');
                            }
                        });
                        return false;
                    },
                    errorPlacement: function(error, element) { //element.prop("placeholder", error.text()); overide the error 
                    }
                }); 
        });
</script>
 <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js"></script>

    </body>

</html>

