  <!-- Page heading -->
  <!-- Give background color class on below line (bred, bgreen, borange, bviolet, blightblue, bblue) -->
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){
	
	//email = $("#txtEmail");
    errornotice = jQuery("#error");
    // The text to show up within a field when it is incorrect
    emptyerror = "This field is required.";
    emailerror = "Please enter a valid e-mail.";


    jQuery("#regfrom").submit(function() {
        //name title..
        var fname = jQuery('#fname').val();
        if ((fname == "") || (fname == emptyerror)) {
            jQuery('#fname').addClass("error");
            jQuery('#fname').val(emptyerror);
            errornotice.fadeIn(750);
        } else {
            jQuery('#fname').removeClass("error");
        }


        var lname = jQuery('#lname').val();
        if ((lname == "") || (lname == emptyerror)) {
            jQuery('#lname').addClass("error");
            jQuery('#lname').val(emptyerror);
            errornotice.fadeIn(750);
        } else {
            jQuery('#lname').removeClass("error");
        }


        var fmail = jQuery('#fmail').val();
        if ((fmail == "") || (fmail == emptyerror)) {
            jQuery('#fmail').addClass("error");
            jQuery('#fmail').val(emptyerror);
            errornotice.fadeIn(750);
        } else {
            jQuery('#fmail').removeClass("error");
        }

        var email = jQuery('#email').val();
        if ((email == "") || (email == emptyerror)) {
            jQuery('#email').addClass("error");
            jQuery('#email').val(emptyerror);
            errornotice.fadeIn(750);
        } else {
            jQuery('#email').removeClass("error");
        }
        

        
        
                                                                
        if (jQuery(":input").hasClass("error")) {
            jQuery(window).scrollTop(jQuery('#msg').offset().top);
            return false;
       
        } else {
            errornotice.hide();

            $.ajax({
                type: "POST",
                url: "<?php  echo SITE_BASE_URL;  ?>sendmail.php",
                data: $("#regfrom").serialize(), // serializes the form's elements. http://api.jquery.com/serialize/
                success: function(Res) {

//alert(Res);
                    
                    $('#regfrom').css('display','none');
                    $('#msg .alert-success').css('display','block');

                },
                error: function(Res) {
                $('#regfrom').css('display','none');
                    $('#msg .alert-error').css('display','block'); 
                }
            });
            
            return false;
        }
       
                            
    });
            

    // Clears any fields in the form when the user clicks on them
    jQuery(":input").focus(function() {
        if (jQuery(this).hasClass("error")) {
            jQuery(this).val("");
            jQuery(this).removeClass("error");
        }
			
    });
    
});
  
  </script>



 
  <div class="page-heading blightblue">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="pull-left"><i class="icon-arrow-right title-icon"></i> Register</h2>
          <div class="pull-right heading-meta">This Is <span class="lightblue">Meta</span>. Write <span class="lightblue">Something</span> Here</div>
        </div>
      </div>
    </div>
  </div>
  <!-- Page heading ends -->

  <!-- Content starts -->
  <div class="content">
    <div class="container">

      <div class="register">
      
      			
                <div class="row">
                <div class="col-md-12">
                  <h2>Start Building your physical &amp; virtual local networks today</h2>
                  <p class="big grey">The &#35;1 Student Hubspot</p>
   
                  <p>Student Vibez is Bradford’s first and only Student web portal, created with the sole purpose of assisting University and College students achieve success.

This site is aimed at helping and supporting students in Bradford. We understand that it is these very people that play a crucial part in the future within societies in UK and beyond.</p>
                    <h3>Key things Student Vibez is Setting out to Achieve</h3>
                        <h5>1. Recruiting Student Ambassadors for Various Campaigns</h5>
                        <p>Being a Student Vibez Ambassador is a fantastic role in shaping the future of the fellow student. Giving live industrial experience in the business arena, this encourages team support, awareness and future goals.</p>
                        <h5>2. Training and support offered on work placement students</h5>
                        <p>We aim to place 100 specifically selected students within the Corporate and Business world. Our entire work placement programmes will offer valuable experience, work related skills and career prospects to each individuals taking part. </p>
                        <h5>3. Support for students wanting to become entrepreneurs</h5>
                        <p>The UK is one of the top five countries in the G20 to start and run businesses. The most inspiring Entrepreneurs are coming out of the UK, here at Student Vibez we have teamed up with Investor Angels, UK Trade and Investment and working with the Sirius programme.</p>
                </div>

                <br>

              </div>
                
                
                <div id="msg">
                
                    <div class="alert-success" style="display:none;">
                                    <h2> Thank You </h2>
                                    <p>Your pre-registration succesfully recorded </p>
                                  </div>
                                  <div class="alert-error" style="display:none;">
                                    <h2>Error Recording</h2>
                                    <p>fdsfsdfsdfsdfsfsdf <br/>fdfdsfsdf</p>
                                  </div>
                </div>
                
              <div class="row formy blightblue">
              <br>
<form id="regfrom" method="post" name="regfrom">
                <div class="col-md-6">
                 <div class="form-horizontal" >
                <!--<div class="control-group">
                  <label class="control-label fieldname" for="inputCompanyName">First Name :</label>-->
                  
                   <div class="form-group">
                   <label class="control-label col-md-3" for="inputCompanyName" >Name </label>
                  
                  <div class="col-md-8">
                    
                     <input type="text" class="form-control" id="fname"  name="fname">
                  </div>
                </div>
                
                <div class="form-group" >
                  <label class="control-label col-md-3" for="inputFirst">Date of Birth </label>
                  <div class="col-md-8">
                   <input type="text" class="form-control" id="dob"  name="dob">
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label col-md-3" for="inputLast">Email Confirmation </label>
                  <div class="col-md-8">
                   <input type="text" class="form-control" id="email"  name="email">
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label col-md-3" for="inputLast">Confirm Password </label>
                  <div  class="col-md-8">
                    <input type="password" id="inputLastPass" class="form-control"  id="password"  name="password">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3" for="inputLast">Educational Institute </label>
                  <div class="controls fieldinput">
                    <div  class="col-md-8">
                     <select  id="stdInstitute" class="form-control" name="edu_institute">
                      <option value="">--Select Institute--</option>
                      <option value="Bradford Colleg">Bradford College</option>
                      <option value="University of Bradford">University of Bradford</option>
                     </select>
                  </div>
                  </div>
                   <input type="hidden" id="stdInstituteId">
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3" for="inputLast">Area of Study </label>
                  <div class="controls fieldinput">
                    <div class="col-md-8">
                    <select id="studyProgram" class="form-control" name="course">
                      
                    <option value="">--Study Program--</option><option value="16">Arts and Media</option>
                    <option value="Beauty Therapy">Beauty Therapy</option>
                    <option value="Business and Enterprise">Business and Enterprise
                    </option><option value="Community and Social Care">Community and Social Care</option>
                    <option value="Community Learning">Community Learning</option><option value="Science">Science</option></select>
                  </div>
                  </div>
                  <input type="hidden" id="studyProgramId"/>
                </div>
				</div>
                </div>

                <div class="col-md-6">
                    <div class="form-horizontal">
                    
                  <div  class="form-group">
                  <label class="control-label col-md-3" for="lname">Surname Name </label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" id="lname"  name="lname">
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label col-md-3" for="inputFirst">Email Address </label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" id="fmail"  name="fmail">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3" for="inputLast">Password </label>
                  <div class="col-md-8">
                    <input type="password" id="inputLast"  class="form-control"  tabindex="6">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3" for="inputLast">Living Post Code </label>
                  <div class="col-md-8">
                    <input type="password" id="inputLastPass" class="form-control" tabindex="8">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3"  for="inputLast">Level of study </label>
                  <div class="controls fieldinput">
                    <div class="col-md-8">
                        <select  id="lvlOfStudy" class="form-control" name="level_of_study">                         
                       <option value="">--Study Level--</option><option value="Certificate Program">Certificate Program</option>
                       <option value="Diploma Program">Diploma Program</option>
                       </select>
                  </div>
                  </div>
                 
                </div>
                <div class="form-group">
                  <label  class="control-label col-md-3" for="inputLast">Code </label>
                  <div class="col-md-8">
                    <input type="password" id="inputLastPass" name="code" class="form-control" tabindex="12">
                  </div>
                </div>
                  
				</div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
    
        <div class="col-md-12">
          <div class="control-group">
               
                  <div class="controls col-md-4">
          
                               
                               
                                <!-- Buttons -->
                                          <div class="form-group" style="margin-top:20px;">
                                             <!-- Buttons -->
											 <div class=" col-md-offset-4"> 
												<button type="submit" class="btn btn-primary">Register</button>
												<button type="reset" class="btn btn-primary">Reset</button>
											</div>
                                          </div>
                                    
                                             Already have an Account? <a href="login.html">Login</a>
                                      <br>
                               
                               
                          </form>
                      
                  </div>
                          
                  </div> 
            </div>
        </div>
                
                
              </div>
              
                  
      </div>
              
              
              
            </div>  

    </div>
  </div>
  <!-- Content ends -->

<!-- Footer -->
<!-- Below area is for Testimonial -->


<div class="foot blightblue">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
          
          <!-- User icon -->
          <span class="twitter-icon text-center"><i class="icon-user"></i></span>
          <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras elementum dolor eget nisi <br />fermentum quis hendrerit magna vestibulum."</em></p>
        
      </div>
    </div>
  </div>
</div>  

