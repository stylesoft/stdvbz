<footer>
  <div class="container">
    <div class="row">


      <div class="widgets">

        <div class="col-md-3">
          <div class="fwidget">
            
            <h4>Contact</h4>

                  <p>Feel free to call or email our head office:) </p>
                  <hr />
                  <i class="icon-home"></i> &nbsp; Bradford
                  <hr />
                  <i class="icon-phone"></i> &nbsp; 011387637263
                  <hr />
                  <i class="icon-envelope-alt"></i> &nbsp; <a href="mailto:#">help@studentvibez.com</a>
                  <hr />
                    <div class="social">
                      <a href="#" class="blightblue"><i class="icon-facebook"></i></a>
                      <a href="#" class="blightblue"><i class="icon-google-plus"></i></a> 
                      <a href="#" class="blightblue"><i class="icon-twitter"></i></a>
                      <a href="#" class="blightblue"><i class="icon-linkedin"></i></a>
                      <a href="#" class="blightblue"><i class="icon-pinterest"></i></a>
                      <a href="#" class="blightblue"><i class="icon-rss"></i></a>
                    </div>

          </div>
        </div>

        <div class="col-md-3">
          <div class="fwidget">
            <h4>Categories</h4>
            <ul>
              <li><a href="#">Profiles</a></li>
              <li><a href="#">Renting</a></li>
              <li><a href="#">Directory</a></li>
              <li><a href="#">Jobs</a></li>
              <li><a href="#">News</a></li>
              <li><a href="#">Contact</a></li>
            </ul>
          </div>
        </div>        

        <div class="col-md-3">
          <div class="fwidget">
            
            <h4>Subscribe to Newsletter</h4>
            <p>Get notifications on updates to new site features and latest promotions</p>
            <p>Enter you email to Subscribe</p>
            
            <form class="form-inline" role="form">
              <div class="form-group">
                <input type="text" class="form-control" id="subscribe" placeholder="Subscribe...">
              </div>
              <button type="submit" class="btn btn-primary">Subscribe</button>
            </form>

          </div>
        </div>

        <div class="col-md-3">
          <div class="fwidget">
            <h4>Recent Posts</h4>
            <ul>
              <li><a href="#">Working on Rentals</a></li>
              <li><a href="#">Designing framework</a></li>
              <li><a href="#">Student Register form</a></li>
              <li><a href="#">Landing page</a></li>
              <li><a href="#">Logo hand draw</a></li>
            </ul>
          </div>
        </div>

      </div>
	</div>
	<div class="row">
      <div class="col-md-12">
          <div class="copy">
                <p>Copyright &copy; 2014 <a href="#">Student Vibez Ltd.</a> - <a href="index.html">Home</a> | <a href="about.html">About Us</a> | <a href="about.html">FAQ</a> | <a href="about.html">Contact Us</a> | <a href="about.html">T&amp;C</a>| <a href="about.html">Privacy Policy</a><br>
              All right reserved.
Student Vibez Ltd. is a company registered in England and Wales Registered Number: 08826105 | 
Registered Office: 6 Southbrook Terrace, Bradford, BD7 1AB 
| Powered by<a href="http://www.pluspro.com"> PlusPro</a></p>
          </div>
      </div>

    </div>
  <div class="clearfix"></div>
  </div>
</footer> 