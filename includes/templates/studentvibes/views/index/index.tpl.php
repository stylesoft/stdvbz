
  <div class="parallax-slider">
          <!-- Slider (Parallax Slider) -->            
            <div id="da-slider" class="da-slider">
              <!-- Each slide should be enclosed inside "da-slide" class -->
              <!-- Slide starts -->
              <div class="da-slide">

                  <p class="blightblue">
                    <!-- Heading -->
                    <span class="das-head">Sign Up For Courses Today</span><br />
                    <!-- Para -->
                    Check out our course list and sign-up today -- there's no limit to the number of classes you can take! You can enroll in a course even if the start date hasn't been announced yet.
                    <br />
                    <!-- Link -->
                    <a class="das-link btn btn-default">Read More <i class="icon-double-angle-right"></i></a>
                  </p>
                  <!-- Image -->
                  <div class="da-img"><img src="<?php print(SITE_BASE_URL);?>img/parallax/1.png" alt="image01" /></div>

              </div>
              <!-- Slide ends -->

              <div class="da-slide">

                  <p class="blightblue">
                    <!-- Heading -->
                    <span class="das-head">Chat and Get The Latest Insights</span><br />
                    <!-- Para -->
                   Join our Social Media pages to learn about popular courses, participate in contests, submit your photos, give shout outs, and more. You can always visit our blog and join or plan a meetup in your home city as well!
                    <br />
                    <a class="das-link btn btn-default">Read More <i class="icon-double-angle-right"></i></a>
                  </p>
                  <!-- Image -->
                  <div class="da-img"><img src="<?php print(SITE_BASE_URL);?>img/parallax/2.png" alt="image01" /></div>

              </div>
              <div class="da-slide">

                  <p class="blightblue">
                    <!-- Heading -->
                    <span class="das-head">Find a Place to Rent</span><br />
                    <!-- Para -->
                    Around the Bradford area to Bradford University and Bradford College Students. Where a student can show interest to a student accommodation.
                    <br />
                    <a class="das-link btn btn-default">Read More <i class="icon-double-angle-right"></i></a>
                  </p>
                  <!-- Image -->
                  <div class="da-img"><img src="<?php print(SITE_BASE_URL);?>img/parallax/3.png" alt="image01" /></div>

              </div>
              <div class="da-slide">

                  <p class="blightblue">
                    <!-- Heading -->
                    <span class="das-head">Search for Jobs</span><br />
                    <!-- Para -->
                    Get ready to start working for Large Corporate Organisations, Offering Work Placements, Apprenticeships and Career Jobs
                    <br />
                    <a class="das-link btn btn-default">Read More <i class="icon-double-angle-right"></i></a>
                  </p>
                  <!-- Image -->
                  <div class="da-img"><img src="<?php print(SITE_BASE_URL);?>img/parallax/4.png" alt="image01" /></div>

              </div>

              <nav class="da-arrows">
                <span class="da-arrows-prev"></span>
                <span class="da-arrows-next"></span>
              </nav>


            </div>
  </div>
<!-- Slider ends -->

<!-- Hero starts -->

  <div class="hero">
    <div class="container">
      <div class="row">
        <div class="col-md-12">

          <div class="hero-left">
            <i class="icon-arrow-right"></i>
          </div>

          <div class="hero-right">
            <div class="hero-title"><b class="lightblue">Welcome</b> to <b class="lightblue">Student Vibez</b> The Number 1 HubSpot for Students in and around Bradford</div>
            <p>Student Vibez is Bradford’s first &amp; only Student web portal, created with the sole purpose of assisting University &amp; College students achieve success.

This site is aimed at helping &amp; supporting students as we understand you play a crucial roll in the future UK society &amp; beyond.</p>
          </div>

          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>


<!-- Hero Ends -->

<!-- Big box starts -->

  <div class="big-box">
    <div class="container">
      <div class="row">
        <div class="col-md-12">

          <!-- Left box -->
          <div class="big-box-left blightblue">
            <!-- Box content -->
            <div class="box-content">
              <!-- title -->
              <div class="box-title">Welcome to the <strong >COOL BLUE</strong> <br /> of <strong>STUDENT VIBEZ PROFILES</strong></div>
              <!-- Para -->
              <p>Join a cool community - <strong>Pre Register</strong> today to secure your place in our first Beta release. Limited to first 100 Sign ups:) </p>
              <!-- Button -->
              <a href="register.html" class="btn btn-primary">Start your Profile!</a>
              <br /><br />
              <!-- Links -->
              <div class="box-links">
                <a href="#">Checkout other profiles <i class="icon-double-angle-right"></i></a>
                <a href="#">Already a member? LOGIN <i class="icon-double-angle-right"></i></a>
              </div>
            </div>
          </div>

          <!-- Center box -->
          <div class="big-box-mid blightblue">
            <!-- Image -->
            <a href="#"><img src="<?php print(SITE_BASE_URL);?>img/photos/girl1.jpg" alt="" class="img-responsive" /></a>
          </div>

          <!-- Right box -->
          <div class="big-box-right bblack">
             <div class="box-content">
                <div class="box-title">Featured<span class="lightblue">Profile</span></div>

                <h4>Name</h4>
                Jennifer
                <h4>Date Of Birth</h4>
                02/07/1990
                <h4>Info</h4>
                Bradford Uni,<br />
                Postgraduate <br />
                Informatics and Media <br>
                 <a href="resume.html"><span class="lightblue">Full Profile CV</span> <i class="icon-double-angle-right"></i></a>
             </div>
          </div>

          <div class="clearfix"></div>

        </div>
      </div>
    </div>  
  </div>
  <div class="clearfix"></div>

<!-- Big box ends -->


<!-- CTA Starts -->

<div class="container">
  <div class="cta bblack">
    <div class="row">
      <div class="col-md-5 col-sm-6">
        <div class="ctas">
          <!-- Title and Para -->
          <h5><i class="icon-arrow-right icon-metroid"></i> HAVE YOUR SAY IN BRADFORD TODAY </h5>
          <p>Join the group in discussions on the Student Vibez Forum </p>
        </div>
      </div>
      <div class="col-md-4 col-sm-6">
        <div class="ctas">
          <!-- List -->
          <ul>
            <li>Participate in the project as it grows.</li>
            <li>Get early notifications on new features</li>
            <li>Forum Moderators Required</li>
          </ul>
        </div>
      </div>
      <div class="col-md-3 col-sm-12">
        <div class="ctas text-center">
          <!-- Button -->
		  <div class="ctasl">
			  <a href="register.html" class="btn btn-primary">JOIN IN THE DISCUSSIONS <i class="icon-double-angle-right"></i></a>
			  <p>Limited Moderator positions</p>
		  </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- CTA Ends -->


<!-- Features starts -->

<div class="features-one">
  <div class="container">

    <div class="row">

      <div class="col-md-6 col-sm-6">
        <a href="#"><img src="<?php print(SITE_BASE_URL);?>img/photos/shots.jpg" alt="" class="img-responsive" /></a>
      </div>

      <div class="col-md-6 col-sm-6">

        <h2>Need help<span class="lightblue"> Making a Move</span> Tell us were you want to be and we'll do the looking:) </h2>
        <!-- Meta -->
        <div class="features-meta">
          <i class="icon-gift lightblue"></i> Its FREE no agency fees! <br>
          <i class="icon-sitemap lightblue"></i> Priority placement in brand new developments!<br>
          <i class="icon-shopping-cart lightblue"></i> Coming or going buying or selling?
        </div>
        <p>We dont just help you find a new crib, we help you get setup easily, quickly &amp; cheaply right through your student years. </p>
        <a href="register.html" class="btn btn-primary btn-large">REGISTER YOUR ACCOMODATION REQUIREMENTS <i class="icon-double-angle-right"></i></a>
      </div>

    </div>
  </div>
</div>

<!-- Features ends -->

 <div class="content">
    <div class="container">

      <div class="row">
    <div class="col-md-12">

                    <div class="img-portfolio">
                    <p>
                        <!-- Add filter names inside "data-filter". For example ".web-design", ".seo", etc., Filter names are used to filter the below images. -->
                           <ul id="filters">
                             <li><a href="#" data-filter="*" class="btn btn-default blightblue">All</a></li>
                             <li><a href="#" data-filter=".one" class="btn btn-default blightblue">City</a></li>
                             <li><a href="#" data-filter=".two" class="btn btn-default blightblue">University</a></li>
                             <li><a href="#" data-filter=".three" class="btn btn-default blightblue">Max£100pw</a></li>
                             <li><a href="#" data-filter=".four" class="btn btn-default blightblue">Max200</a></li>
                             <li><a href="#" data-filter=".five" class="btn btn-default blightblue">New</a></li>
                           </ul>
                    </p>
                        
                        
                    <div id="portfolio">
                        <!-- Add the above used filter names inside div tag. You can add more than one filter names. For image lightbox you need to include "a" tag pointing to image link, along with the class "prettyphoto". -->                        
                       <div class="element one three"><a href="<?php print(SITE_BASE_URL);?>img/portfolio/1.jpg" class="prettyphoto">
                           <img src="img/portfolio/1.jpg" alt=""/>
                           <!-- Portfolio caption -->
                           <div class="pcap blightblue">
                              <h4>Studio Flat £139</h4>
                              <p>Campus House, <br>10 Hey Street, The Green</p>
                           </div>
                       </a></div>
                       <div class="element two one"><a href="<?php print(SITE_BASE_URL);?>img/portfolio/2.jpg" class="prettyphoto">
                           <img src="<?php print(SITE_BASE_URL);?>img/portfolio/2.jpg" alt=""/>
                           <div class="pcap blightblue">
                              <h4>1 Bed £130</h4>
                              <p>Great Horton Road,<br> Bradford, BD7</p>
                           </div>                           
                       </a></div>
                       <div class="element three five"><a href="<?php print(SITE_BASE_URL);?>img/portfolio/3.jpg" class="prettyphoto">
                           <img src="<?php print(SITE_BASE_URL);?>img/portfolio/3.jpg" alt=""/>
                           <div class="pcap blightblue">
                              <h4>1 Bed £85 </h4>
                              <p>178 Sunbridge Road, <br>Bradford, BD1</p>
                           </div>                           
                       </a></div>
                       <div class="element four two"><a href="<?php print(SITE_BASE_URL);?>img/portfolio/4.jpg" class="prettyphoto">
                           <img src="<?php print(SITE_BASE_URL);?>img/portfolio/4.jpg" alt=""/>
                           <div class="pcap blightblue">
                              <h4>Studio Flat £105</h4>
                              <p>Laisteridge Lane, <br>Bradford, BD5</p>
                           </div>                           
                       </a></div>
                       <div class="element five one"><a href="<?php print(SITE_BASE_URL);?>img/portfolio/5.jpg" class="prettyphoto">
                           <img src="<?php print(SITE_BASE_URL);?>img/portfolio/5.jpg" alt=""/>
                           <div class="pcap blightblue">
                              <h4>1 Bedroom £85</h4>
                              <p>Great Horton Road <br>
                               Bradford, BD7</p>
                           </div>                           
                       </a></div> 
                      
                     
                    </div>
               </div>

    </div>
  </div>

    </div>
  </div>
  <!-- Content ends -->
    
    
<!-- Service and Social media starts -->

<div class="service-home">
  <div class="container">
    <!-- Title -->
    <h3 class="title"><i class="icon-arrow-right title-icon"></i>Local Business Directory</h3>
    <div class="row">

      <!-- Social -->
      <div class="col-md-3 col-sm-6">

        <div class="service-social bblack">
          <!-- Big number -->
          <div class="service-big">Local<span class="lightblue">Offers</span></div>
          <!-- Labels -->
          <div class="label label-danger">FOOD</div> 
          <div class="label label-primary">VOUCHERS</div> 
          <div class="label label-success">DISCOUNT</div>

          <hr />

          <!-- Social media -->
          <div class="service-box blightblue">
            <!-- name and followers -->
            <a href="#">Gym / Spa <span class="pull-right">25% OFF</span></a>
          </div>
          <div class="service-box blightblue">
            <!-- name and followers -->
            <a href="#">Night Club <span class="pull-right">15% OFF</span></a>
          </div>
          <div class="service-box blightblue">
            <!-- name and followers -->
            <a href="#">Pub Lunch <span class="pull-right">£5 OFF</span></a>
          </div>
          <div class="service-box blightblue">
            <!-- name and followers -->
            <a href="#">Pizza / Pasta <span class="pull-right">50% OFF</span></a>
          </div>
          <div class="service-box blightblue">
            <!-- name and followers -->
            <a href="#">Cloths / Bags<span class="pull-right">£25 OFF</span></a>
          </div>    
 <div class="service-box bblue">
            <!-- name and followers -->
            <a href="register.html">See Latest Offers</a>
          </div>    
          <div class="clearfix"></div>

        </div>

      </div>

      <!-- Service -->

      <div class="col-md-6 col-sm-6 service-list">

          <!--  Service #1 -->
          <!-- Service icon -->
          <div class="service-icon">
            <i class="icon-briefcase blightblue"></i>
          </div>

          <div class="service-content">
            <!-- Title -->
            <div class="service-home-meta">Coming Soon</div>
            <h4>Jobs Work Placements &amp; Apprenticeship</h4>
            <p>We like to support students in getting them into employment pursuing there dream careers and giving employment advice.</p>
          </div>

          <hr />

          <!-- Service #2 -->

          <div class="service-icon">
            <i class="icon-shopping-cart blightblue"></i>
          </div>

          <div class="service-content">
            <div class="service-home-meta">Coming Soon</div>
            <h4>Retail Discounts &amp; Vouchers</h4>
            <p>Like to shop and save. Well you’re in the right place. Have access to the latest local discounts and voucher available on your fingertip.</p>
          </div>

          <hr />   

          <!-- Service #3 -->

          <div class="service-icon">
            <i class="icon-credit-card blightblue"></i>
          </div>

          <div class="service-content">
            <div class="service-home-meta">Coming Soon</div>
            <h4>Student Finance Support</h4>
            <p>As a student we understand that finance can be a burden to many. That’s why we have a finance department that can advice and help.</p>
          </div>
          
          <hr />

          <!-- Service #4 -->

          <div class="service-icon">
            <i class="icon-money blightblue"></i>
          </div>

          <div class="service-content">
            <div class="service-home-meta">Coming Soon</div>
            <h4>Becoming an Entrepreneur</h4>
            <p>he UK is one of the top five countries in the G20 to start and run businesses. The most inspiring Entrepreneurs are coming out of the UK, here at Student Vibez we have teamed up with Investor Angels, UK Trade and Investment and working with the Sirius programme.</p>
          </div>
          
          <br />


          <div class="clearfix"></div>

      </div>

      <!-- Image -->
      <div class="col-md-3 col-sm-6">
        <a href="#" class="service-image"><img src="<?php print(SITE_BASE_URL);?>img/photos/girl2.png" alt="" class="img-responsive" /></a>
      </div>

    </div>

    <hr />
    
  </div>
</div>

<!-- Service and Social media ends -->


<!-- Clients starts -->
  
  <div class="clients">
    <div class="container">
            <div class="row">
               <div class="col-md-12">
                     <h3 class="title">Check us out on the web... </h3>
                     <p><i class="icon-quote-left lightblue"></i>Contact Student Vibez and make a <strong>Strong Social Connection</strong><i class="icon-quote-right lightblue"></i></p>
                     <img src="<?php print(SITE_BASE_URL);?>img/clients/google.png" alt="" class="img-responsive" />
                     <img src="<?php print(SITE_BASE_URL);?>img/clients/twitter.png" alt="" class="img-responsive" />
                     <img src="<?php print(SITE_BASE_URL);?>img/clients/facebook.png" alt="" class="img-responsive" />
                     <img src="<?php print(SITE_BASE_URL);?>img/clients/skype.png" alt="" class="img-responsive" />
                     <img src="<?php print(SITE_BASE_URL);?>img/clients/youtube.png" alt="" class="img-responsive" />
               </div>
            </div>
    </div>
  </div>
            
<!-- Clients ends -->

<!-- Footer -->

<!-- Below area is for testimonial -->
<div class="foot blightblue">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
          
          <!-- User icon -->
          <span class="twitter-icon text-center"><i class="icon-user"></i></span>
          <p><em>"If you can't solve a problem,<br />it's because you are playing by the rules."</em></p>
          
        
      </div>
    </div>
  </div>
</div> 
