$(document).ready(function(){
	
	//email = $("#txtEmail");
    errornotice = jQuery("#error");
    // The text to show up within a field when it is incorrect
    emptyerror = "This field is required.";
    emailerror = "Please enter a valid e-mail.";
    emailNomatch = "Please enter a valid e-mail.";


    jQuery("#regfrom").submit(function() {
        //name title..
        var fname = jQuery('#fname').val();
        if ((fname == "") || (fname == emptyerror)) {
            jQuery('#fname').addClass("error");
            jQuery('#fname').val(emptyerror);
            errornotice.fadeIn(750);
        } else {
            jQuery('#fname').removeClass("error");
        }


        var lname = jQuery('#lname').val();
        if ((lname == "") || (lname == emptyerror)) {
            jQuery('#lname').addClass("error");
            jQuery('#lname').val(emptyerror);
            errornotice.fadeIn(750);
        } else {
            jQuery('#lname').removeClass("error");
        }


        var fmail = jQuery('#fmail').val();
        if ((fmail == "") || (fmail == emptyerror)) {
            jQuery('#fmail').addClass("error");
            jQuery('#fmail').val(emptyerror);
            errornotice.fadeIn(750);
        } else {
            jQuery('#fmail').removeClass("error");
        }

    	var email = $('#email').val();
		if ((email == "") || (email == emptyerror)) {
			$('#email').addClass("error");
			$('#email').val(emptyerror);
			errornotice.fadeIn(750);
		} else {
			$('#email').removeClass("error");
		}

        
        
                                                                
        if (jQuery(":input").hasClass("error")) {
            jQuery(window).scrollTop(jQuery('#msg').offset().top);
            return false;
       
        } else {
            errornotice.hide();

            $.ajax({
                type: "POST",
                url: "<?php  echo SITE_BASE_URL  ?>sendmail.php",
                data: $("#regfrom").serialize(), // serializes the form's elements. http://api.jquery.com/serialize/
                success: function(Res) {

//alert(Res);
                    
                    $('#regfrom').css('display','none');
                    $('#msg .alert-success').css('display','block');

                },
                error: function(Res) {
                $('#regfrom').css('display','none');
                    $('#msg .alert-error').css('display','block'); 
                }
            });
            
            return false;
        }
       
                            
    });
            

    // Clears any fields in the form when the user clicks on them
    jQuery(":input").focus(function() {
        if (jQuery(this).hasClass("error")) {
            jQuery(this).val("");
            jQuery(this).removeClass("error");
        }
			
    });
    
});
