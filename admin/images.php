<?php
require_once '../bootstrap.php';
require_once('includes/auth.php');

$module         = "";
$controller     = "";
$objectId       = "";
$imageRec       = "";
$menuId         = "";

$backUrl        = "";

if($_GET){
    
    if(isset($_GET['m'])){
        $module         = $_GET['m'];
    }
    
    
    if(isset($_GET['c'])){
        $controller         = $_GET['c'];
    }
    
    
    if(isset($_GET['mid'])){
        $menuId         = $_GET['mid'];
    }
    
    if(isset($_GET['id'])){
        $objectId         = $_GET['id'];
    }
    
    

    $imageObject = new Image();
    $imageRec    = $imageObject->getAllByImageObject($controller,$objectId); 
    
    
}


if($module == 'restaurant' &&  $controller == 'Restaurant_Menu_Item'){
    
    $backUrl        = "restaurant/menu_items.html?mid=".$menuId;
    
} elseif($module == 'album' &&  $controller == 'Image_Gallery'){
    
    $backUrl        = "album/album.html";
}elseif($module == 'product' &&  $controller == 'Product'){
    
    $backUrl        = "product/product.html";
}

$contentTitle = "The content title comes here";

$LAYOUT = ADMIN_LAYOUT_PATH."layout.tpl.php";
$CONTENT = ADMIN_LAYOUT_PATH."images/index.tpl.php";
	
require_once $LAYOUT;
?>