<?php 
$objSideModules = new Modules();
$objSideModulesLink = new ModuleLink();
?>
       
       
       <div id="containerHolder">
			<div id="container">


<div id="sidebar">
                	<ul id="sideNav">
                    
                      
                       
                       
                    <?php if($objSideModules->isEnabled('cms')){?>
                    	<li><span class="parent">Manage Pages</span>
                        	
                             <ul>
                                <?php if($objSideModulesLink->isModuleLinkEnable('add_a_page')){?>
                                <li><a href="<?php print(ADMIN_BASE_URL);?>pages.html?action=add&pageType=mainPage">Add a Page</a></li>
                                <?php } ?>
                                 <?php if($objSideModulesLink->isModuleLinkEnable('edit_pages')){?>
                            	<li><a href="<?php print(ADMIN_BASE_URL);?>pages.html">Edit Pages</a></li>
                                 <?php } ?>

                          </ul>
                        </li>
                       <?php } ?>
                       
                       <?php if($objSideModules->isEnabled('service')){?>
                           <li><span class="parent">Services</span>
                              <ul>
                               <?php if($objSideModulesLink->isModuleLinkEnable('manage_service')){?>
                                    <li><a href="<?php print(ADMIN_BASE_URL); ?>service/service.html">View All Services</a></li>
                                
                           
                                <li><a href="<?php print(ADMIN_BASE_URL); ?>service/service.html?action=add">Add Services</a></li>
                                <?php } ?>
                                
                          </ul>
                        </li>
                        <?php } ?>
                        
                        
                       <?php if($objSideModules->isEnabled('site_contents')){?>
                           <li><span class="parent">Site Contents</span>
                        	
                              <ul>
                                <?php if($objSideModulesLink->isModuleLinkEnable('side_bar_contents')){?>
                            	<li><a href="<?php print(ADMIN_BASE_URL); ?>sidebar/sidebar.html">Side Bar Contents</a></li>
                                <?php } ?>
                                <?php if($objSideModulesLink->isModuleLinkEnable('add_side_bar_contents')){?>
                                <li><a href="<?php print(ADMIN_BASE_URL); ?>sidebar/sidebar.html?action=add">Add Side Bar Contents</a></li>
                                <?php } ?>
                                <?php if($objSideModulesLink->isModuleLinkEnable('site_contents')){?>
                                 <li><a href="<?php print(ADMIN_BASE_URL); ?>site-contents/site-content.html">Site Contents</a></li>
                                 <?php } ?>
                                 
                                 
                                 
                                  <?php if($objSideModulesLink->isModuleLinkEnable('add_site_contents')){?>
                                    <li><a href="<?php print(ADMIN_BASE_URL); ?>site-contents/site-content.html?action=add">Add Site Contents</a></li>
                                    <?php } ?>
                                   <?php if($objSideModulesLink->isModuleLinkEnable('view_all_home_banners')){?>

                                   <li><a href="<?php print(ADMIN_BASE_URL); ?>banner/banner.html?bannerType=HomeBanners">View All Home Banners</a></li>
                                 <?php } ?>
                                 
                              <?php if($objSideModulesLink->isModuleLinkEnable('add_banner')){?>

                                 <li><a href="<?php print(ADMIN_BASE_URL); ?>banner/banner.html?bannerType=HomeBanners&action=add">Add Home Banners</a></li>
                                <?php } ?>
                                
                                
                                
                                
                                <?php if($objSideModulesLink->isModuleLinkEnable('side_bar_contents_header')){?>
                            	<li><a href="<?php print(ADMIN_BASE_URL); ?>sidebar/sidebar.html?type=header">Header</a></li>
                                <?php } ?>
                                
                                
                                
                                <?php if($objSideModulesLink->isModuleLinkEnable('side_bar_contents_middle_left')){?>
                            	<li><a href="<?php print(ADMIN_BASE_URL); ?>sidebar/sidebar.html?type=middle-left">Middle Left</a></li>
                                <?php } ?>
                                
                                <?php if($objSideModulesLink->isModuleLinkEnable('side_bar_contents_middle_right')){?>
                            	<li><a href="<?php print(ADMIN_BASE_URL); ?>sidebar/sidebar.html?type=middle-right">Middle Right</a></li>
                                <?php } ?>
                                
                                <?php if($objSideModulesLink->isModuleLinkEnable('side_bar_contents_footer')){?>
                            	<li><a href="<?php print(ADMIN_BASE_URL); ?>sidebar/sidebar.html?type=footer">Footer</a></li>
                                <?php } ?>
                                
                                
                                <?php if($objSideModulesLink->isModuleLinkEnable('opening_times')){?>
                            	<li><a href="<?php print(ADMIN_BASE_URL); ?>opening_times/opening_times.html">Opening Times</a></li>
                                <?php } ?>

                          </ul>
                        </li>
                        <?php } ?>
                        
                        
                        <?php if($objSideModules->isEnabled('image_gallery')){?>
                           <li><span class="parent">Image Gallery</span>
                        	
                               <ul>
                                <?php if($objSideModulesLink->isModuleLinkEnable('view_galleries')){?>
                            	<li><a href="<?php print(ADMIN_BASE_URL); ?>album/album.html">View Albums</a></li>
                                <?php } ?>
                                 <?php if($objSideModulesLink->isModuleLinkEnable('add_gallery')){?>
                                <li><a href="<?php print(ADMIN_BASE_URL); ?>album/album.html?action=add">Add Album</a></li>
                                <?php } ?>
                                
                          </ul>
                        </li>
                        <?php } ?>
                        
                        <?php if($objSideModules->isEnabled('news')){?>
                           <li><span class="parent">News Management</span>
                              <ul>
                               <?php if($objSideModulesLink->isModuleLinkEnable('view_all_news')){?>
                            	<li><a href="<?php print(ADMIN_BASE_URL); ?>news/news.html">View All News</a></li>
                                <?php } ?>
                                <?php if($objSideModulesLink->isModuleLinkEnable('add_news')){?>
                                <li><a href="<?php print(ADMIN_BASE_URL); ?>news/news.html?action=add">Add News</a></li>
                                <?php } ?>
                                
                          </ul>
                        </li>
                        <?php } ?>
                        
                        
                        
                  
                        
                        
                        
                        


						<?php if($objSideModules->isEnabled('restaurants_menu')){?>
                       <li><span class="parent">Manage Menus</span>
                        	
                             <ul>
                                <?php if($objSideModulesLink->isModuleLinkEnable('view_all_menus')){?>
                            	<li><a href="<?php print(ADMIN_BASE_URL);?>restaurant/menu.html">View All Menus</a></li>
                                <?php } ?>
                                <?php if($objSideModulesLink->isModuleLinkEnable('add_new_menu')){?>
                                <li><a href="<?php print(ADMIN_BASE_URL);?>restaurant/menu.html?action=add">Add New Menu</a></li>
                                <?php } ?>
                          </ul>
                        </li>
                        <?php } ?>
                        
                        
                           

<?php if($objSideModules->isEnabled('Price')){?>
                           <li><span class="parent">Price List</span>
                        	
                               <ul>
                                <?php if($objSideModulesLink->isModuleLinkEnable('manage_price')){?>
                            	<li><a href="<?php print(ADMIN_BASE_URL); ?>price/price.html">Manage Price</a></li>
                                <?php } ?>
                                 
                                
                          </ul>
                        </li>
                        <?php } ?>


						<?php if($objSideModules->isEnabled('translation')){?>
                           <li><span class="parent">Translations</span>
                        	
                              <ul>
                                <?php if($objSideModulesLink->isModuleLinkEnable('view_all_phrases')){?>
                            	<li><a href="<?php print(ADMIN_BASE_URL); ?>translation/translation.html">View all Phrases</a></li>
                                <?php } ?>
                                <?php if($objSideModulesLink->isModuleLinkEnable('add_phrase')){?>
                                <li><a href="<?php print(ADMIN_BASE_URL); ?>translation/translation.html?action=add">Add Phrase</a></li>
                                <?php } ?>
                          </ul>
                        </li>
                        <?php } ?>


			 <?php if($objSideModules->isEnabled('registration')){?>
                    	<li><span class="parent">Student Profiles </span>
                        	
                             <ul>
                                 <?php if($objSideModulesLink->isModuleLinkEnable('view_registration')){?>
                            	<li><a href="<?php print(ADMIN_BASE_URL);?>registration/registration.html">View Profiles</a></li>
                                 <?php } ?>
<li><a href="#">Manage Groups</a></li>
                          </ul>
                        </li>
                       <?php } ?>


<?php if($objSideModules->isEnabled('Project')){?>
                           <li><span class="parent">Projects</span>
                                
                              <ul>
				<?php if($objSideModulesLink->isModuleLinkEnable('project_listing')){?>
                                <li><a href="<?php print(ADMIN_BASE_URL); ?>project/project.html">Project Listing</a></li>
				<?php } ?>
				<?php if($objSideModulesLink->isModuleLinkEnable('new_project')){?>
                                <li><a href="<?php print(ADMIN_BASE_URL); ?>project/project.html?action=add"> New Project </a></li>
				<?php } ?>

                          </ul>
                        </li>
                        <?php } ?>
                        
                        
                        <?php if($objSideModules->isEnabled('Testimonial')){?>
                           <li><span class="parent">Testimonials</span>
                                
                              <ul>
					<?php if($objSideModulesLink->isModuleLinkEnable('manage_testimonial')){?>
                                    <li><a href="<?php print(ADMIN_BASE_URL); ?>testimonial/testimonials.html">Manage List</a></li>
					<?php } ?>
				<?php if($objSideModulesLink->isModuleLinkEnable('add_new_testimonial')){?>
                                <li><a href="<?php print(ADMIN_BASE_URL); ?>testimonial/testimonials.html?action=add"> Add New </a></li>
				<?php } ?>
					<?php if($objSideModulesLink->isModuleLinkEnable('featured_testimonial')){?>
                                 <li><a href="<?php print(ADMIN_BASE_URL); ?>testimonial/testimonials.html?c=featured"> Featured Testimonials </a></li>
				<?php } ?>
				
                          </ul>
                        </li>
                        <?php } ?>


<?php if($objSideModules->isEnabled('Product')){?>
                           <li><span class="parent">Products</span>
                                
                              <ul>
					<?php if($objSideModulesLink->isModuleLinkEnable('product_listing')){?>
                                    	<li><a href="<?php print(ADMIN_BASE_URL); ?>product/product.html">Product Listing</a></li>
					<?php } ?>
					<?php if($objSideModulesLink->isModuleLinkEnable('add_product')){?>
                                	<li><a href="<?php print(ADMIN_BASE_URL); ?>product/product.html?action=add"> Add Product </a></li>
					<?php } ?>
                                        
                                        <?php if($objSideModulesLink->isModuleLinkEnable('special_offers')){?>
                                    	<li><a href="<?php print(ADMIN_BASE_URL); ?>product/product.html?action=specialOffers">Special Offers</a></li>
					<?php } ?>
					<?php if($objSideModulesLink->isModuleLinkEnable('featured_products')){?>
                                	<li><a href="<?php print(ADMIN_BASE_URL); ?>product/product.html?action=featured"> Featured Products </a></li>
					<?php } ?>
                                        <?php if($objSideModulesLink->isModuleLinkEnable('view_all_categories')){?>
                                <li><a href="<?php print(ADMIN_BASE_URL); ?>category/category.html">View All Categories</a></li>
                                <?php } ?>
                                
                                <?php if($objSideModulesLink->isModuleLinkEnable('add_new_category')){?>
                                <li><a href="<?php print(ADMIN_BASE_URL); ?>category/category.html?action=add">Add New Category</a></li>
                                <?php } ?>

                          </ul>
                        </li>
                        <?php } ?>
                        
                        
                        <?php if($objSideModules->isEnabled('Order')){?>
                           <li><span class="parent">Orders</span>
                                
                              <ul>
					<?php if($objSideModulesLink->isModuleLinkEnable('view_all_orders')){?>
                                    	<li><a href="<?php print(ADMIN_BASE_URL); ?>order/order.html">View Orders</a></li>
					<?php } ?>
					

                          </ul>
                        </li>
                        <?php } ?>
                        
                        
                        
<?php if($objSideModules->isEnabled('customer')){?>
                           <li><span class="parent">Customers</span>
                                
                              <ul>
					<?php if($objSideModulesLink->isModuleLinkEnable('manage_customer')){?>
                                    	<li><a href="<?php print(ADMIN_BASE_URL); ?>customer/customer.html">Manage Customer</a></li>
					<?php } ?>
					

                          </ul>
                        </li>
                        <?php } ?>
                        
                        
                        <!--   institute modle -->
                        
                             <li><span class="parent">Institute </span>
                        	
                             <ul>
                                
                                <li><a href="<?php print(ADMIN_BASE_URL); ?>institute/institute.html">View Listings</a></li>
                                <li><a href="<?php print(ADMIN_BASE_URL); ?>institute/institute.html?action=add">Add Institute</a></li>
                               
                          </ul>
                        </li>
                        
                        <!--   End institute modle -->
                        
                           <!--   study level modle -->
                        
                             <li><span class="parent">Study Level </span>
                        	
                             <ul>
                                
                                <li><a href="<?php print(ADMIN_BASE_URL); ?>studylevel/studylevel.html">View Study Level</a></li>
                                <li><a href="<?php print(ADMIN_BASE_URL); ?>studylevel/studylevel.html?action=add">Add Study Level</a></li>
                               
                          </ul>
                        </li>
                        
                        <!--   End study Area modle -->
                        
                        
                                            <!--   study level modle -->
                        
                             <li><span class="parent">Study Area</span>
                        	
                             <ul>
                                
                                <li><a href="<?php print(ADMIN_BASE_URL); ?>studyarea/studyarea.html">View Study Area</a></li>
                                <li><a href="<?php print(ADMIN_BASE_URL); ?>studyarea/studyarea.html?action=add">Add Study Area</a></li>
                               
                          </ul>
                        </li>
                        
                        <!--   End study Area modle -->
                        
                        
                        <li><span class="parent">Property</span>
                        	
                             <ul>
                                 <li><a href="#">Add Listing</a></li>
                            	<li><a href="#">Edit Listing</a></li>
                                <li><a href="#">View Listings</a></li>
                                <li><a href="#">Add Developer</a></li>
                                <li><a href="#">View Developers</a></li>
                               <li><a href="#">Edit Developers</a></li>
                                <li><a href="#">Featured</a></li>
                                 <li><a href="#">Availability</a></li>
                          </ul>
                        </li>

 <li><span class="parent">Directory</span>
                        	
                             <ul>
                                 <li><a href="#">Add Package</a></li>
                            	<li><a href="#">Edit Packages</a></li>
                                <li><a href="#">View Offers</a></li>
                                <li><a href="#">Edit Offers</a></li>
                                <li><a href="#">View Companies</a></li>
                               <li><a href="#">Edit Companies</a></li>
                                <li><a href="#">Applications</a></li>
                                 <li><a href="#">View Orders</a></li>
                          </ul>
                        </li>
                       
 <li><span class="parent">Jobs</span>
                        	
                             <ul>
                            	<li><a href="#">Add Job</a></li>
                                <li><a href="#">View Jobs</a></li>
                                <li><a href="#">Edit Jobs</a></li>
                                <li><a href="#">View Companies</a></li>
                               <li><a href="#">Edit Companies</a></li>
                                <li><a href="#">Applications</a></li>
                          </ul>
                        </li>
                        

<li><span class="parent">News</span>
                        	
                             <ul>
                            	<li><a href="#">Add Post</a></li>
                                <li><a href="#">View Posts</a></li>
                                <li><a href="#">Edit Posts</a></li>
                         
                                 
                          </ul>
      </li>
                        <li><span class="parent">Emails</span>
                                <ul>
                            	<li><a href="#">Newsletters</a></li>
                                <li><a href="#">Notifications</a></li>
                         
                                 
                          </ul>
                        </li>
                        </li>
                        <li><span class="parent">Help</span>
                                <ul>
                            	<li><a href="#">Forum</a></li>
                                <li><a href="#">FAQ's</a></li>
                         
                                 
                          </ul>
                        </li>
                           <li><span class="parent">Site Settings</span>
                        	
                             <ul>
                            	
                                <li><a href="<?php print(ADMIN_BASE_URL);?>settings.html">Configuration</a></li>
                                <li><a href="<?php print(ADMIN_BASE_URL);?>site-contacts.html">Contact Details</a></li>
                                <li><a href="<?php print(ADMIN_BASE_URL);?>social_media_settings.html">Social Media</a></li>
                               <li><a href="<?php print(ADMIN_BASE_URL);?>users.html">User Admin</a></li>
                                <li><a href="<?php print(ADMIN_BASE_URL);?>changepassword.html">Change Pass </a></li>
                          </ul>
                        </li>


                    </ul>
                    <!-- // #sideNav -->
</div>   

                <!-- // #sidebar -->




	
	
