<script type="text/javascript">
	$(document).ready(
		function()
		{
			$('#mainbody').redactor({
				imageUpload: '<?php print(ADMIN_BASE_URL); ?>scripts/image_upload.php'
			});
                        
                        $('#pageContents').redactor({
				imageUpload: 'scripts/image_upload.php'
			});
		}
	);
	</script>

<div id="search_main_wrapper"  style="margin-bottom:55px;">

            <h2>Update Home Pages</h2>     
</div>


<div id="table_main_wrapper">
    <div id="dashboard" style="background-color:#FFF;">
        <div>
            <form  action="" method="post">

<div id="two">
                
               

                <div  <?php if($pageType == 'homePage'){?> style="display: none;" <?php } ?>>
                    <fieldset>

                        
                        <div id="separator">
                            <label for="pagename">Page Name : </label> 
                            <input name="pagename" type="text" id="pagename" tabindex="1" size="30" maxlength="30" value="<?php print($NAME);?>"/>
                        </div>




                     <div id="separator">
                            <label for="status">Page Status : </label> 
                            <select name="status" id="status">
                                <option value="Published" <?php if($LIVE == 'Published'){?> selected="selected" <?php } ?>>Published</option>
                                <option value="Exclude_from_menu" <?php if($LIVE == 'Exclude_from_menu'){?> selected="selected" <?php } ?>>Exclude from menu</option>
                                <option value="Draft" <?php if($LIVE == 'Draft'){?> selected="selected" <?php } ?>>Draft</option>
                            </select>
                      </div>



                        <div id="separator">
                            <label for="contact">Add Contact: </label> 
                            <select name="contact" id="contact">
                                <option value="Yes" <?php if($ISCONTACT == 'Yes'){?> selected="selected" <?php } ?>>Yes</option>
                                <option value="No" <?php if($ISCONTACT == 'No'){?> selected="selected" <?php } ?>>No</option>
                            </select>
                            <span>Select Yes if this page is a contact us page.</span>
                            
                        </div>
                    </fieldset>
                </div>
               

<h2>Header Info</h2>
                <div>   
                    <fieldset>

                       

                           <div id="separator">

                            <label for="pagetitle">Page Title : </label>
                            <input name="pagetitle" type="text" id="pagetitle" tabindex="2" size="64" style="width:600px;" value="<?php print($TITLE);?>"/>
                        </div>



                          <div id="separator">

                            <label for="keywords">Keywords : </label> 
                            <input name="keywords" type="text" id="keywords" tabindex="3" style="width:600px;" value="<?php print($KEYWORDS);?>"/>
                        </div>




                          <div id="separator">


                            <label for="description">Description : </label> 
                            <textarea name="description" style="width:600px; height:80px;" id="description"><?php print($DESCRIPTION);?></textarea>
                        </div>  

                    </fieldset>



                </div>










  <h2>Top Content</h2>

                <fieldset>
                
                
                          <div id="separator">
                            <label for="pagename">Heading : </label> 
                            <input name="pageContentId[]" type="hidden" id="pageContentId" tabindex="1" size="30" maxlength="30" <?php if($topContentDetails){?> value="<?php print($topContentDetails->id);?>" <?php } ?>/>
                            <input name="contentType[]" type="hidden" id="contentType" tabindex="1" size="30" maxlength="30" value="TOP_CONTENT"/>
                            <input name="contentDisplayOrder[]" type="hidden" id="contentDisplayOrder" tabindex="1" size="30" maxlength="30" value="1"/>
                            
                            <input name="pageContentsTitle[]" type="text" id="pageContentsTitle" tabindex="1" size="30"  <?php if($topContentDetails){?> value="<?php print($topContentDetails->title);?>" <?php } ?> />
                        </div>
                 
                    <div id="separator">
                        <textarea name="pageContents[]"  id="pageContents"><?php if($topContentDetails){?> <?php print($topContentDetails->contents);?> <?php } ?></textarea>
                    </div>
                </fieldset>
                
                
                
        <h2>Home Page Columns</h2>      
        <div>
        
        
        <fieldset>
        
            
            <?php for($x = 0;$x<=2;$x++){
                 $homePageColumnsContent = "";
                 $homePageColumnsContent = $homePageColumnsContents[$x];
                ?>
         <div id="separator">

                            <label for="lcol_title">Left Col Heading : </label> 
                            <input name="pageContentId[]" type="hidden" id="pageContentId" tabindex="1" size="30" maxlength="30" <?php if($homePageColumnsContent){?> value="<?php print($homePageColumnsContent->id);?>" <?php } ?>/>
                            <input name="contentType[]" type="hidden" id="contentType" tabindex="1" size="30" maxlength="30" value="COLUMN_CONTENT"/>
                            <input name="contentDisplayOrder[]" type="hidden" id="contentDisplayOrder" tabindex="1" size="30" maxlength="30" value="<?php print($x);?>"/>
                            
                          
                            
                            <input name="pageContentsTitle[]" type="text" id="lcol_title" tabindex="3" style="width:600px;" <?php if($homePageColumnsContent){?> value="<?php print($homePageColumnsContent->title);?>" <?php } ?>/>
                        </div>




                          <div id="separator">


                            <label for="lcol_description">Left Col Text : </label> 
                            <textarea name="pageContents[]" style="width:600px; height:80px;" id="description"><?php if($homePageColumnsContent){?> <?php print($homePageColumnsContent->contents);?> <?php } ?></textarea>
                        </div>  
                        
                <div style="clear:both; margin-top:25px;">&nbsp;</div>     
                                
         <?php } ?>
                        
             
        
        
        </fieldset>  
                
                
   </div>    
   
   
   
           <h2>Testamonials</h2>      
        <div>
        
        
        <fieldset>
        
       




                          
                          
                           
                            <?php for($t = 0;$t<=1;$t++){
                                
                                $testamonialContent = "";
                                $testamonialContent = $testamonials[$t];
                                ?>
                            <div id="separator">

                            <label for="left_test_title"> <?php if($t==1){?>  Left <?php } else {?> Right <?php } ?> Person : </label> 
                            <input name="pageContentsTitle[]" type="text" id="left_test_title" tabindex="3" style="width:600px;" <?php if($testamonialContent){?> value="<?php print($testamonialContent->title);?>" <?php } ?> />
                            
                            <input name="pageContentId[]" type="hidden" id="pageContentId" tabindex="1" size="30" maxlength="30" <?php if($testamonialContent){?> value="<?php print($testamonialContent->id);?>" <?php } ?>/>
                            <input name="contentType[]" type="hidden" id="contentType" tabindex="1" size="30" maxlength="30" value="TESTAMONIAL"/>
                            <input name="contentDisplayOrder[]" type="hidden" id="contentDisplayOrder" tabindex="1" size="30" maxlength="30" value="<?php print($t);?>"/>
                            
                            </div>
            
                            <div id="separator">

                            <label for="left_test_description"><?php if($t==1){?>  Left <?php } else {?> Right <?php } ?> Text : </label> 
                            <textarea name="pageContents[]" style="width:600px; height:80px;" id="left_test_description">
<?php if($testamonialContent){?> <?php print($testamonialContent->contents);?> <?php } ?>
                            </textarea>
                            </div>  
    
                            <div style="clear:both; margin-top:25px;">&nbsp;</div>     
                            <?php } ?>   
        
                        


                                
                        
 
                        
       
        
        
        </fieldset>  
                
                
   </div> 
   
            
                
                
                
                <p>
</div>
                    <input id="testbutton" class="testbutton" type="submit" value="submit" name="Submit" /> 
                    <input id="resetbutton" type="reset"  />
                    <input type="hidden" name="txtPageId" id="txtPageId" value="<?php print($pageId);?>" />
                    <input type="hidden" name="txtParentpageId" id="txtParentpageId" value="<?php print($parentpageId);?>" />
                    <input type="hidden" name="txtSecParentpageId" id="txtSecParentpageId" value="<?php print($secParentpageId);?>" />
                    <input type="hidden" name="txtAction" id="txtAction" value="<?php print($action);?>" />
                    <input type="hidden" name="txtPageType" id="txtPageType" value="<?php print($pageType);?>" />
                     <input type="hidden" name="txtListingId" id="txtListingId" value="<?php print($ListingID);?>" />
                      <input type="hidden" name="txtAllowToDelete" id="txtAllowToDelete" value="<?php print($ALLOWDELETE);?>" />
                </p>

                <div id="show"></div>
            </form>
        </div>
        <div class="clear">
        </div> 
    </div>
</div>
