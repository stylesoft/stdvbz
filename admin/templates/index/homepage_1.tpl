<script src="<?php print(ADMIN_BASE_URL);?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>jlibs/js/pluspro_validation/pages.js"></script>


<script type="text/javascript">
	$(document).ready(
		function()
		{
                      
                     <?php for($uploaderT=0;$uploaderT<=1; $uploaderT++){?>
                        // the small image button
			var btnUpload=$('#btnImgT<?php print($uploaderT);?>');
			new AjaxUpload(btnUpload, {
				action: 'uploadFile.php',
				name: 'uploadfile',
				onSubmit: function(file, ext){
					 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
	                    // extension is not allowed
						status.text('Only JPG, PNG or GIF files are allowed');
						return false;
					}
					 var prg_image = "<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/uploader50.gif";
                                        $("#btnImgT<?php print($uploaderT);?>").attr("src",prg_image);
				},
				onComplete: function(file, response){
					//Add uploaded file to list
					if(response!="error"){
						//$('<li></li>').appendTo('#files').html('<img src="../content/uploads/photos/'+file+'" alt="" /><br />'+file).addClass('success');
	                                        var new_image = "<?php print(SITE_BASE_URL); ?>imgs/"+response;
	                                        $("#btnImgT<?php print($uploaderT);?>").attr("src",new_image);
	                                        $("#txtSmallFileNameT<?php print($uploaderT);?>").attr("value", response);
					} else{
                                                alert("Error, when upload the file");
					}
				}
			});
                       <?php } ?>
                        
                        
                        
                        
                        
                         <?php for($uploaderM=0;$uploaderM<=3; $uploaderM++){?>
                        // the small image button
			var btnUpload=$('#btnImgM<?php print($uploaderM);?>');
			new AjaxUpload(btnUpload, {
				action: 'uploadFile.php',
				name: 'uploadfile',
				onSubmit: function(file, ext){
					 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
	                    // extension is not allowed
						status.text('Only JPG, PNG or GIF files are allowed');
						return false;
					}
					 var prg_image = "<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/uploader50.gif";
                                        $("#btnImgM<?php print($uploaderM);?>").attr("src",prg_image);
				},
				onComplete: function(file, response){
					//Add uploaded file to list
					if(response!="error"){
						//$('<li></li>').appendTo('#files').html('<img src="../content/uploads/photos/'+file+'" alt="" /><br />'+file).addClass('success');
	                                        var new_image = "<?php print(SITE_BASE_URL); ?>imgs/"+response;
	                                        $("#btnImgM<?php print($uploaderM);?>").attr("src",new_image);
	                                        $("#txtSmallFileNameM<?php print($uploaderM);?>").attr("value", response);
					} else{
                                                alert("Error, when upload the file");
					}
				}
			});
                        <?php } ?>
                        
                        
                        <?php for($uploaderB=0;$uploaderB<1; $uploaderB++){?>
                        // the small image button
			var btnUpload=$('#btnImgB<?php print($uploaderB);?>');
			new AjaxUpload(btnUpload, {
				action: 'uploadFile.php',
				name: 'uploadfile',
				onSubmit: function(file, ext){
					 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
	                    // extension is not allowed
						status.text('Only JPG, PNG or GIF files are allowed');
						return false;
					}
					 var prg_image = "<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/uploader50.gif";
                                        $("#btnImgB<?php print($uploaderB);?>").attr("src",prg_image);
				},
				onComplete: function(file, response){
					//Add uploaded file to list
					if(response!="error"){
						//$('<li></li>').appendTo('#files').html('<img src="../content/uploads/photos/'+file+'" alt="" /><br />'+file).addClass('success');
	                                        var new_image = "<?php print(SITE_BASE_URL); ?>imgs/"+response;
	                                        $("#btnImgB<?php print($uploaderB);?>").attr("src",new_image);
	                                        $("#txtSmallFileNameB<?php print($uploaderB);?>").attr("value", response);
					} else{
                                                alert("Error, when upload the file");
					}
				}
			});
                        <?php } ?>
                       
                        
                        
                        
		}
	);
	</script>
        
        
        
        
        
       

<div id="search_main_wrapper"  style="margin-bottom:55px;">
            <?php if($action == 'add'){?>
           <h2>Add New Page</h2> 
            <?php } else {?>
            <h2>Update Page</h2> 
            <?php } ?>
</div>

         <div id="error">
            Please fill in all the required fields and re-submit the form .<br>
        </div>

<div id="table_main_wrapper">
    <div id="dashboard" style="background-color:#FFF;">
        <div>
            <form  action="" method="post" name="pageForm" id="pageForm">

<div id="two">
                
               

                <div  <?php if($pageType == 'homePage'){?> style="display: none;" <?php } ?>>
                    <fieldset>

                        
                        <div id="separator">
                            <label for="pagename">Page Name : </label> 
                            <input name="pagename" type="text" id="pagename" tabindex="1" size="30" maxlength="30" value="<?php print($NAME);?>"/>
                        </div>




                     <div id="separator">
                            <label for="status">Page Status : </label> 
                            <select name="status" id="status" tabindex="2">
                                <option value="Published" <?php if($LIVE == 'Published'){?> selected="selected" <?php } ?>>Published</option>
                                <option value="Exclude_from_menu" <?php if($LIVE == 'Exclude_from_menu'){?> selected="selected" <?php } ?>>Exclude from menu</option>
                                <option value="Draft" <?php if($LIVE == 'Draft'){?> selected="selected" <?php } ?>>Draft</option>
                            </select>
                      </div>



                        <div id="separator" style="display: none;">
                            <label for="contact">Add Contact: </label> 
                            <select name="contact" id="contact" tabindex="3">
                                <option value="Yes" <?php if($ISCONTACT == 'Yes'){?> selected="selected" <?php } ?>>Yes</option>
                                <option value="No" <?php if($ISCONTACT == 'No'){?> selected="selected" <?php } ?>>No</option>
                            </select>
                            <span>Select Yes if this page is a contact us page.</span>
                            
                        </div>
                    </fieldset>
                </div>
               

<h2>Header Info</h2>
                <div>   
                    <fieldset>

                       

                           <div id="separator">

                            <label for="pagetitle">Page Title : </label>
                            <input name="pagetitle" type="text" id="pagetitle" tabindex="4" size="64" style="width:600px;" value="<?php print($TITLE);?>"/>
                        </div>



                          <div id="separator">

                            <label for="keywords">Keywords : </label> 
                            <input name="keywords" type="text" id="keywords" tabindex="5" style="width:600px;" value="<?php print($KEYWORDS);?>"/>
                        </div>




                          <div id="separator">


                            <label for="description">Description : </label> 
                            <textarea name="description" style="width:600px; height:80px;" id="description" tabindex="6"><?php print($DESCRIPTION);?></textarea>
                        </div>  

                    </fieldset>



                </div>





  <h2>Top Slider Content</h2>

                <fieldset>
                
                        <?php 
                        for($t = 0;$t<2;$t++){
                         $contentImage = ""; 
                         
                         $topContent = "";
                         $topContent = $topContentsDetails[$t];
                         
                         if($topContent){
                            if($topContent->contentMainImage != ''){
                                $contentImage = SITE_BASE_URL.'imgs/'.$topContent->contentMainImage;
                            } else {
                                $contentImage = GLOBAL_ADMIN_CONTENT_PATH."images/no-image-icon.png";
                            }
                        } else {
                            $contentImage = GLOBAL_ADMIN_CONTENT_PATH."images/no-image-icon.png";
                        }
                        ?>
                        <div id="separator">
                
                    <label for="lcol_title">Content Image : </label>
                    <img src="<?php print($contentImage);?>" height="50" width="50" name="btnImgt" id="btnImgT<?php print($t);?>"/>
                    <input type="hidden" id="txtSmallFileNameT<?php print($t);?>" name="txtSmallFileName[]" <?php if($topContent){?> value="<?php print($topContent->contentMainImage);?>" <?php } ?> />
                     <span style="color:red;font-size: 10px;">Clcik on image to change</span>
                    
                </div>
                    
                          <div id="separator">
                            <label for="pagename">Heading : </label> 
                            <input name="pageContentId[]" type="hidden" id="pageContentId" tabindex="1" size="30" maxlength="30" <?php if($topContent){?> value="<?php print($topContent->id);?>" <?php } ?>/>
                            <input name="contentType[]" type="hidden" id="contentType" tabindex="1" size="30" maxlength="30" value="TOP_CONTENT"/>
                            <input name="contentDisplayOrder[]" type="hidden" id="contentDisplayOrder" tabindex="1" size="30" maxlength="30" value="1"/>
                            <input name="pageContentsLink[]" id="pageContentsLink" value="NONE"  type="hidden" />
                            
                            <input name="pageContentsTitle[]" type="text" id="pageContentsTitle" tabindex="1" size="30"  <?php if($topContent){?> value="<?php print($topContent->title);?>" <?php } ?> style="width:600px;" />
                        </div>
                        
                        
                 
                    <div id="separator">
                        <textarea name="pageContents[]"  id="pageContents" class="ckeditor"><?php if($topContent){?> <?php print($topContent->contents);?> <?php } ?></textarea>
                    </div>
                    <?php } ?>
                </fieldset>

<h2>Middle Contents</h2>      
        <div>
        
        
        <fieldset>
        
            
            <?php for($m = 0;$m<4;$m++){
                 $homePageMiddleContent = "";
                 $homePageMiddleContent = $middleContentsDetails[$m];
                 $contentImage = "";
                 if($homePageMiddleContent){
                    if($homePageMiddleContent->contentMainImage != ''){
                        $contentImage = SITE_BASE_URL.'imgs/'.$homePageMiddleContent->contentMainImage;
                    } else {
                         $contentImage = GLOBAL_ADMIN_CONTENT_PATH."images/no-image-icon.png";
                    }
                 } else {
                     $contentImage = GLOBAL_ADMIN_CONTENT_PATH."images/no-image-icon.png";
                 }
                ?>
         
                <div id="separator">
                    <label for="lcol_title">Content Image : </label>
                    <img src="<?php print($contentImage);?>" height="50" width="50" name="btnImgM<?php print($m);?>" id="btnImgM<?php print($m);?>"/>
                    <input type="hidden" id="txtSmallFileNameM<?php print($m);?>" name="txtSmallFileName[]" <?php if($homePageMiddleContent){?> value="<?php print($homePageMiddleContent->contentMainImage);?>" <?php } ?> />
                     <span style="color:red;font-size: 10px;">Clcik on image to change</span>
                </div>
                
                <div id="separator">

                            <label for="lcol_title">Middle Content Title</label> 
                            <input name="pageContentId[]" type="hidden" id="pageContentId" tabindex="1" size="30" maxlength="30" <?php if($homePageMiddleContent){?> value="<?php print($homePageMiddleContent->id);?>" <?php } ?>/>
                            <input name="contentType[]" type="hidden" id="contentType" tabindex="1" size="30" maxlength="30" value="MIDDEL_CONTENT"/>
                            <input name="contentDisplayOrder[]" type="hidden" id="contentDisplayOrder" tabindex="1" size="30" maxlength="30" value="<?php print($m);?>"/>
                            
                          
                            
                            <input name="pageContentsTitle[]" type="text" id="lcol_title" tabindex="3" style="width:600px;" <?php if($homePageMiddleContent){?> value="<?php print($homePageMiddleContent->title);?>" <?php } ?>/>
                        </div>


                        <div id="separator">


                            <label for="lcol_description">Content Link : </label> 
                            <input name="pageContentsLink[]" type="text" id="pageContentsLink" tabindex="3" style="width:600px;" <?php if($homePageMiddleContent){?> value="<?php print($homePageMiddleContent->contentLink);?>" <?php } ?>/>
                        </div> 


                          <div id="separator">


                            <label for="lcol_description" style="display: none;">Standard colour options : </label> 
                            <textarea name="pageContents[]" style="width:600px; height:80px;" id="description<?php print($m);?>" class="ckeditor"><?php if($homePageMiddleContent){?> <?php print($homePageMiddleContent->contents);?> <?php } ?></textarea>
                        </div>  
                        
                        
                        
                <div style="clear:both; margin-top:25px;">&nbsp;</div>     
                                
         <?php } ?>
                        
             
        
        
        </fieldset>  
                
                
   </div>   
                        
                        
                        
                        <div>
  <h2>Page Content</h2>

                <fieldset>
                 
                    <div id="separator">
                        <textarea name="mainbody"  id="mainbody" tabindex="7" class="ckeditor"><?php print($BODY);?></textarea>
                    </div>
                </fieldset>
</div>

                        
                        
            
                        <h2>Bottom Contents</h2>      
        <div>
        
        
        <fieldset>
        
            
            <?php for($b = 0;$b<1;$b++){
                 $homePageBottomContent = "";
                 $homePageBottomContent = $bottomContentsDetails[$b];
                 $contentImage = "";
                 if($homePageBottomContent){
                    if($homePageBottomContent->contentMainImage != ''){
                        $contentImage = SITE_BASE_URL.'imgs/'.$homePageBottomContent->contentMainImage;
                    } else {
                         $contentImage = GLOBAL_ADMIN_CONTENT_PATH."images/no-image-icon.png";
                    }
                 } else {
                     $contentImage = GLOBAL_ADMIN_CONTENT_PATH."images/no-image-icon.png";
                 }
                ?>
         
                <div id="separator">
                    <label for="lcol_title">Content Image : </label>
                    <img src="<?php print($contentImage);?>" height="50" width="50" name="btnImgB<?php print($b);?>" id="btnImgB<?php print($b);?>"/>
                    <input type="hidden" id="txtSmallFileNameB<?php print($b);?>" name="txtSmallFileName[]" <?php if($homePageBottomContent){?> value="<?php print($homePageBottomContent->contentMainImage);?>" <?php } ?> />
                     <span style="color:red;font-size: 10px;">Clcik on image to change</span>
                </div>
                
                     <div id="separator" style="display: none;">

                            <label for="lcol_title">Bottom Content Title</label> 
                            <input name="pageContentId[]" type="hidden" id="pageContentId" tabindex="1" size="30" maxlength="30" <?php if($homePageBottomContent){?> value="<?php print($homePageBottomContent->id);?>" <?php } ?>/>
                            <input name="contentType[]" type="hidden" id="contentType" tabindex="1" size="30" maxlength="30" value="BOTTOM CONTENT"/>
                            <input name="contentDisplayOrder[]" type="hidden" id="contentDisplayOrder" tabindex="1" size="30" maxlength="30" value="<?php print($b);?>"/>
                            
                          
                            
                            <input name="pageContentsTitle[]" type="text" id="lcol_title" tabindex="3" style="width:600px;" <?php if($homePageBottomContent){?> value="<?php print($homePageBottomContent->title);?>" <?php } ?>/>
                        </div>


                        <div id="separator" style="display: none;">


                            <label for="lcol_description">Content Link : </label> 
                            <input name="pageContentsLink[]" type="text" id="pageContentsLink" tabindex="3" style="width:600px;" <?php if($homePageBottomContent){?> value="<?php print($homePageBottomContent->contentLink);?>" <?php } ?>/>
                        </div> 


                          <div id="separator" style="display: none;">


                            <label for="lcol_description" style="display: none;">Standard colour options : </label> 
                            <textarea name="pageContents[]" style="width:600px; height:80px;" id="description<?php print($b);?>" class="ckeditor"><?php if($homePageBottomContent){?> <?php print($homePageBottomContent->contents);?> <?php } ?></textarea>
                        </div>  
                        
                        
                        
                <div style="clear:both; margin-top:25px;">&nbsp;</div>     
                                
         <?php } ?>
                        
             
        
        
        </fieldset>  
                
                
   </div>   
                        
                        
                        
                       
  
</div>
                    <input id="testbutton" class="testbutton" type="submit" value="submit" name="Submit" tabindex="8"/> 
                    <input id="resetbutton" type="reset" tabindex="9" />
                    <input type="hidden" name="txtPageId" id="txtPageId" value="<?php print($pageId);?>" />
                    <input type="hidden" name="txtParentpageId" id="txtParentpageId" value="<?php print($parentpageId);?>" />
                    <input type="hidden" name="txtSecParentpageId" id="txtSecParentpageId" value="<?php print($secParentpageId);?>" />
                    <input type="hidden" name="txtAction" id="txtAction" value="<?php print($action);?>" />
                    <input type="hidden" name="txtPageType" id="txtPageType" value="<?php print($pageType);?>" />
                     <input type="hidden" name="txtListingId" id="txtListingId" value="<?php print($ListingID);?>" />
                      <input type="hidden" name="txtAllowToDelete" id="txtAllowToDelete" value="<?php print($ALLOWDELETE);?>" />
                </p>

                <div id="show"></div>
            </form>
        </div>
        <div class="clear">
        </div> 
    </div>
</div>
