<?php
require_once '../bootstrap.php';
require_once('includes/auth.php');


$SocialMediaSettingId           = 1;
$objSocialMediaSetting         	= new SocialMediaSetting();
$socialMediaSettingInfo       	= $objSocialMediaSetting->getSocialMediaSetting($SocialMediaSettingId);

$id                             =   $socialMediaSettingInfo->id;
$facebook                       =   $socialMediaSettingInfo->facebook;
$twitter                        =   $socialMediaSettingInfo->twitter;
$linkedIn                       =   $socialMediaSettingInfo->linkedIn;
$googlePlus                     =   $socialMediaSettingInfo->googlePlus;
$other                          =   $socialMediaSettingInfo->other;
$pinterest                      =   $socialMediaSettingInfo->pinterest;


if($_POST){
	
	$socialMediaSettingDataObject = new stdClass();
	
	$id                      =   $_POST['txtId'];
        $facebook                =   $_POST['txtFacebookUrl'];
        $twitter                 =   $_POST['txtTwitterUrl'];
        $linkedIn                =   $_POST['txtLinkedIn'];
        $googlePlus              =   $_POST['txtGooglePlus'];
        $other                   =   $_POST['txtOther'];
        $pinterest               =   $_POST['txtPinterest'];
        
	
	$socialMediaSettingDataObject->id                 = $id;
        $socialMediaSettingDataObject->facebook           = $facebook;
        $socialMediaSettingDataObject->twitter            = $twitter;
        $socialMediaSettingDataObject->linkedIn           = $linkedIn;
        $socialMediaSettingDataObject->googlePlus         = $googlePlus;
        $socialMediaSettingDataObject->other              = $other;
        $socialMediaSettingDataObject->pinterest          = $pinterest;
	$objSocialMediaSetting->editSocialMediaSetting($socialMediaSettingDataObject);
	
	$mainPageUrl = 'social_media_settings.html';

	print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Your <span class='red'>Social Media Settings</span> has been updated!<br /><br /><br /><br /><a href='$mainPageUrl'  class='mybutton' style='padding-right:20px;float:none;'>Continue</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
	/*
	echo "<div id='coverit'></div><div id='message'>Your <span class='red'>Settings</span> has been updated!<br /><br /><br /><br /><a class='myButton' href='$mainPageUrl'>Continue</a></div>";
	*/
}

$LAYOUT = ADMIN_LAYOUT_PATH."layout.tpl.php";
$CONTENT = ADMIN_LAYOUT_PATH."tpl/social_media.tpl";
	
require_once $LAYOUT;
?>
