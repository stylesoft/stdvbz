<?php
require_once '../../../bootstrap.php';
require_once('../../includes/auth.php');

$id ='';
$program ='';
$description='';
$level='';
$lang="EN";
$created_on = getCurrentDateTime();
$created_by ='';
$last_modified_on=getCurrentDateTime();
$last_modified_by='';

// listings.....
$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;
$_REC_ORDER = "";
$_REC_PAGES = "";
$_SEARCH_QUERY = "";
$ORDER_BY = "";
$ORDER = "";

$invalid = false;

$lastInsertedId = "";
$isRecordUpdated = "";

$action = '';
$arrCategory = array();

$objArea = new StudyArea();
$objArea->tb_name = 'set_study_area';



if (isset($_GET['action'])) {
    $action = $_GET['action'];
    if (isset($_GET['id'])) { $id = $_GET['id'];}
    
               $studyareainfo = $objArea->getStudyProgram($id);
                
    if ($action == 'edit') {      

	$id = $studyareainfo->id;
	$program = $studyareainfo->program;
	$description = $studyareainfo->description;
	$lang = $studyareainfo->lang ;
	$level = $studyareainfo->level;
	
    }
    
   
}

if ($_POST) {

	

	$action = $_POST['txtAction'];

	if(isset($_POST['id'])) $id = $_POST['id'];
	 $level = $_POST['level'];
	 $description = $_POST['description'];
	 $program =$_POST['program'];
	//$lang =$_POST['lang'];
	








	//validation
	if($level == ""){
		array_push($objArea->error, 'The Study Area  name is required');
		$invalid = true;
	}

	$objArea->id = $id;
	$objArea->program= $program;
	$objArea->description= $description;
	
	
	$objArea->level= $level;
	$objArea->lang= $lang;
	
	$objArea->created_on = $created_on;
	$objArea->created_by = $created_by;
	$objArea->last_modified_on = $last_modified_on;
	$objArea->last_modified_by = $last_modified_by;
	
	if ($action == 'add' && !$invalid) {
	
		$lastInsertedId = $objArea->addstudyArea();
	}elseif ($action == 'edit' && !$invalid) {
		$isRecordUpdated = $objArea->updatestudyLevel();
	}

	$newPageUrl = "studyarea.html?action=add";
	$mainPageUrl = "studyarea.html";

	if ($lastInsertedId) {

		print("<div id='boxes'>");
		print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
		//The message comes here..
		print("Study Area <span class='red'>$name</span> has been added!<br /><br /><br /><a href='$mainPageUrl'  class='mySmallButton' style='margin-right:20px'>ok</a>");
		print("</div>");
		print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
		print("</div>");

		//echo "<div id='coverit'></div><div id='message'>Category $name <span class='red'>$banner_title</span> has been added!<br /><br /><br />Add Another Category?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
	} else if ($isRecordUpdated) {


		print("<div id='boxes'>");
		print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
		//The message comes here..
		print("Study Area <span class='red'>$name</span> has been Updated!<br /><br /><br /><a href='$mainPageUrl'  class='mySmallButton' style='margin-right:20px'>ok</a>");
		print("</div>");
		print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
		print("</div>");

		// echo "<div id='coverit'></div><div id='message'>Category $name <span class='red'>$banner_title</span> has been updated!<br /><br /><br />Edit Category?<br /><br /><a href=''  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
	}
} else {

	if ($_GET) {

		if (isset($_GET['q'])) {
			$_SEARCH_QUERY = $_GET['q'];
		}

		if (isset($_GET['orderby'])) {
			$ORDER_BY = $_GET['orderby'];
		} else {
			$ORDER_BY = "program";
		}

		if (isset($_GET['order'])) {
			$ORDER = $_GET['order'];
		} else {
			$ORDER = 'ASC';
		}
		if (isset($_GET['rows'])) {
			$_REC_PER_PAGE = $_GET['rows'];
		}
	}
	$_SEARCH_QUERY = str_replace(" ","",trim($_SEARCH_QUERY));
	// get all the news details

	$objArea->searchStr = $_SEARCH_QUERY;
	//$totalNumberOfMenus = $objArea->countRec();
	$pageNumbers = ceil($totalNumberOfMenus / $_REC_PER_PAGE);
}

if ($action == 'edit' || $action == 'add') {
	$CONTENT = MODULE_TEMPLATES_PATH . "form.tpl.php";
}else {
	$CONTENT = MODULE_TEMPLATES_PATH . "index.tpl.php";
}
$LAYOUT = ADMIN_LAYOUT_PATH . "layout.tpl.php";
require_once $LAYOUT;
?>
