<?php

/** '
 * @author  :   Gayan Chathuranga <gayan.chathuranga@monara.com> i
 * @desc    :   This scripts get fires when home banner image order is changed. so it updates the database
 */
require_once '../../../../bootstrap.php';
require_once('../../../includes/auth.php');

$objHomeBanner = new HomeBanner();
$objHomeBanner->tb_name = 'home_banner_images';

$action = mysql_real_escape_string($_POST['action']);
$updateRecordsArray = $_POST['recordsArray'];

if ($action == "updateRecordsListings") {

    $listingCounter = 1;
    foreach ($updateRecordsArray as $recordIDValue) {

        $objHomeBanner->recordListingID = $listingCounter;
        $objHomeBanner->recordID = $recordIDValue;
        $objHomeBanner->updateBannerListingOrder();
        //$query = "UPDATE album_images SET recordListingID = " . $listingCounter . " WHERE recordID = " . $recordIDValue;
        //mysql_query($query) or die('Error, insert query failed');
        $listingCounter = $listingCounter + 1;
    }
    echo 'If you refresh the page, you will see that records will stay just as you modified.';
}
?>