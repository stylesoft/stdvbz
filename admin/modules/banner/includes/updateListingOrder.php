<?php 
require_once '../../../../bootstrap.php';
require_once('../../../includes/auth.php');

$array	= $_POST['recordsArray'];
$objHomeBanner = new HomeBanner();
$objHomeBanner->tb_name = 'home_banner_images';

if ($_POST['action'] == "updateRecordsListings"){
	$count = 1;
	foreach ($array as $idval) {
            $objHomeBanner->recordListingID = $count;
            $objHomeBanner->recordID        = $idval;
            $objHomeBanner->updateBannerListingOrder();
		$count ++;	
	}
	echo 'All saved! refresh the page to see the changes';
}
?>
