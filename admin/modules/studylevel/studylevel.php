<?php
require_once '../../../bootstrap.php';
require_once('../../includes/auth.php');

$id ='';
$institute ='';
$description='';
$level='';
$lang="EN";
$created_on = getCurrentDateTime();
$created_by ='';
$last_modified_on=getCurrentDateTime();
$last_modified_by='';

// listings.....
$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;
$_REC_ORDER = "";
$_REC_PAGES = "";
$_SEARCH_QUERY = "";
$ORDER_BY = "";
$ORDER = "";

$invalid = false;

$lastInsertedId = "";
$isRecordUpdated = "";

$action = '';
$arrCategory = array();

$objLevel = new studyLevel();
$objLevel->tb_name = 'set_study_level';



if (isset($_GET['action'])) {
    $action = $_GET['action'];
    if (isset($_GET['id'])) { $id = $_GET['id'];}
    
               $studiLevelInfo = $objLevel->getLevel($id);
                
    if ($action == 'edit') {      

	$id = $studiLevelInfo->id;
	$institute = $studiLevelInfo->institute;
	$description = $studiLevelInfo->description;
	$lang = $studiLevelInfo->lang ;
	$level = $studiLevelInfo->level;
	
    }
    
   
}

if ($_POST) {

	

	$action = $_POST['txtAction'];

	if(isset($_POST['id'])) $id = $_POST['id'];
	$level = $_POST['level'];
	$description = $_POST['description'];
	$institute =$_POST['institute'];
	//$lang =$_POST['lang'];
	








	//validation
	if($level == ""){
		array_push($objLevel->error, 'The Study level  name is required');
		$invalid = true;
	}

	$objLevel->id = $id;
	$objLevel->institute= $institute;
	$objLevel->description= $description;
	
	
	$objLevel->level= $level;
	$objLevel->lang= $lang;
	
	$objLevel->created_on = $created_on;
	$objLevel->created_by = $created_by;
	$objLevel->last_modified_on = $last_modified_on;
	$objLevel->last_modified_by = $last_modified_by;
	
	if ($action == 'add' && !$invalid) {
	
		$lastInsertedId = $objLevel->addstudyLevel();
	}elseif ($action == 'edit' && !$invalid) {
		$isRecordUpdated = $objLevel->updatestudyLevel();
	}

	$newPageUrl = "studylevel.html?action=add";
	$mainPageUrl = "studylevel.html";

	if ($lastInsertedId) {

		print("<div id='boxes'>");
		print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
		//The message comes here..
		print("Study level <span class='red'>$name</span> has been added!<br /><br /><br /><a href='$mainPageUrl'  class='mySmallButton' style='margin-right:20px'>ok</a>");
		print("</div>");
		print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
		print("</div>");

		//echo "<div id='coverit'></div><div id='message'>Category $name <span class='red'>$banner_title</span> has been added!<br /><br /><br />Add Another Category?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
	} else if ($isRecordUpdated) {


		print("<div id='boxes'>");
		print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
		//The message comes here..
		print("Study level <span class='red'>$name</span> has been Updated!<br /><br /><br /><a href='$mainPageUrl'  class='mySmallButton' style='margin-right:20px'>ok</a>");
		print("</div>");
		print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
		print("</div>");

		// echo "<div id='coverit'></div><div id='message'>Category $name <span class='red'>$banner_title</span> has been updated!<br /><br /><br />Edit Category?<br /><br /><a href=''  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
	}
} else {

	if ($_GET) {

		if (isset($_GET['q'])) {
			$_SEARCH_QUERY = $_GET['q'];
		}

		if (isset($_GET['orderby'])) {
			$ORDER_BY = $_GET['orderby'];
		} else {
			$ORDER_BY = "level";
		}

		if (isset($_GET['order'])) {
			$ORDER = $_GET['order'];
		} else {
			$ORDER = 'ASC';
		}
		if (isset($_GET['rows'])) {
			$_REC_PER_PAGE = $_GET['rows'];
		}
	}
	$_SEARCH_QUERY = str_replace(" ","",trim($_SEARCH_QUERY));
	// get all the news details

	$objLevel->searchStr = $_SEARCH_QUERY;
	//$totalNumberOfMenus = $objLevel->countRec();
	$pageNumbers = ceil($totalNumberOfMenus / $_REC_PER_PAGE);
}

if ($action == 'edit' || $action == 'add') {
	$CONTENT = MODULE_TEMPLATES_PATH . "form.tpl.php";
}else {
	$CONTENT = MODULE_TEMPLATES_PATH . "index.tpl.php";
}
$LAYOUT = ADMIN_LAYOUT_PATH . "layout.tpl.php";
require_once $LAYOUT;
?>
