<?php
require_once '../../../bootstrap.php';
require_once('../../includes/auth.php');

$id ='';
$name ='';
$description='';
$created_on = getCurrentDateTime();
$created_by ='';
$last_modified_on=getCurrentDateTime();
$last_modified_by='';

// listings.....
$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;
$_REC_ORDER = "";
$_REC_PAGES = "";
$_SEARCH_QUERY = "";
$ORDER_BY = "";
$ORDER = "";

$invalid = false;

$lastInsertedId = "";
$isRecordUpdated = "";

$action = '';
$arrCategory = array();

$objInstitute = new Institute();
$objInstitute->tb_name = 'set_edu_institute';



if (isset($_GET['action'])) {
    $action = $_GET['action'];
    if (isset($_GET['id'])) { $id = $_GET['id'];}
    
                $instituteInfo = $objInstitute->getInstitute($id);
                
    if ($action == 'edit') {      

	$id = $instituteInfo->id;
	$name = $instituteInfo->name;
	$description = $instituteInfo->description;
        
    }
    
   
}

if ($_POST) {

	$action = $_POST['txtAction'];

	if(isset($_POST['id'])) $id = $_POST['id'];
	$name = $_POST['name'];
	$description = $_POST['description'];
	









	//validation
	if($name == ""){
		array_push($objInstitute->error, 'The Institute  name is required');
		$invalid = true;
	}

	$objInstitute->id = $id;
	$objInstitute->name= $name;
	$objInstitute->description= $description;
	$objInstitute->created_on = $created_on;
	$objInstitute->created_by = $created_by;
	$objInstitute->last_modified_on = $last_modified_on;
	$objInstitute->last_modified_by = $last_modified_by;

	if ($action == 'add' && !$invalid) {
		$lastInsertedId = $objInstitute->addInstitute();
	}elseif ($action == 'edit' && !$invalid) {
		$isRecordUpdated = $objInstitute->updateInstitute();
	}

	$newPageUrl = "institute.html?action=add";
	$mainPageUrl = "institute.html";

	if ($lastInsertedId) {

		print("<div id='boxes'>");
		print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
		//The message comes here..
		print("Institute <span class='red'>$name</span> has been added!<br /><br /><br /><a href='$mainPageUrl'  class='mySmallButton' style='margin-right:20px'>ok</a>");
		print("</div>");
		print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
		print("</div>");

		//echo "<div id='coverit'></div><div id='message'>Category $name <span class='red'>$banner_title</span> has been added!<br /><br /><br />Add Another Category?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
	} else if ($isRecordUpdated) {


		print("<div id='boxes'>");
		print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
		//The message comes here..
		print("Institute <span class='red'>$name</span> has been Updated!<br /><br /><br /><a href='$mainPageUrl'  class='mySmallButton' style='margin-right:20px'>ok</a>");
		print("</div>");
		print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
		print("</div>");

		// echo "<div id='coverit'></div><div id='message'>Category $name <span class='red'>$banner_title</span> has been updated!<br /><br /><br />Edit Category?<br /><br /><a href=''  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
	}
} else {

	if ($_GET) {

		if (isset($_GET['q'])) {
			$_SEARCH_QUERY = $_GET['q'];
		}

		if (isset($_GET['orderby'])) {
			$ORDER_BY = $_GET['orderby'];
		} else {
			$ORDER_BY = "name";
		}

		if (isset($_GET['order'])) {
			$ORDER = $_GET['order'];
		} else {
			$ORDER = 'ASC';
		}
		if (isset($_GET['rows'])) {
			$_REC_PER_PAGE = $_GET['rows'];
		}
	}
	$_SEARCH_QUERY = str_replace(" ","",trim($_SEARCH_QUERY));
	// get all the news details

	$objInstitute->searchStr = $_SEARCH_QUERY;
	//$totalNumberOfMenus = $objInstitute->countRec();
	$pageNumbers = ceil($totalNumberOfMenus / $_REC_PER_PAGE);
}

if ($action == 'edit' || $action == 'add') {
	$CONTENT = MODULE_TEMPLATES_PATH . "form.tpl.php";
}else {
	$CONTENT = MODULE_TEMPLATES_PATH . "index.tpl.php";
}
$LAYOUT = ADMIN_LAYOUT_PATH . "layout.tpl.php";
require_once $LAYOUT;
?>
