 <link rel="stylesheet" href="<?php print(FRONT_LAYOUT_URL);?>contents/css/form_styles.css">
<!--wrapper start-->

          <div class="row formy blightblue">
              <br>

                <div class="col-md-6">
                 <form class="form-horizontal">
                <!--<div class="control-group">
                  <label class="control-label fieldname" for="inputCompanyName">First Name :</label>-->
                  
                   <div class="form-group">
                   <label class="control-label col-md-3" for="inputCompanyName">Name </label>
                  
                  <div class="col-md-8">
                    
                     <input type="text" class="form-control" id="inputCompanyName" tabindex="1">
                  </div>
                </div>
                
                <div class="form-group" >
                  <label class="control-label col-md-3" for="inputFirst">Date of Birth </label>
                  <div class="col-md-8">
                    <input type="text" id="inputFirst" class="form-control" tabindex="3">
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label col-md-3" for="inputLast">Email Confirmation </label>
                  <div class="col-md-8">
                    <input type="password" id="inputLast" class="form-control" tabindex="5">
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label col-md-3" for="inputLast">Confirm Password </label>
                  <div  class="col-md-8">
                    <input type="password" id="inputLastPass" class="form-control" tabindex="7">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3" for="inputLast">Educational Institute </label>
                  <div class="controls fieldinput">
                    <div  class="col-md-8">
                     <select  id="stdInstitute" class="form-control">
                      <option value="">--Select Institute--</option>
                      <option value="2">Bradford College</option>
                      <option value="1">University of Bradford</option>
                     </select>
                  </div>
                  </div>
                   <input type="hidden" id="stdInstituteId">
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3" for="inputLast">Area of Study </label>
                  <div class="controls fieldinput">
                    <div class="col-md-8">
                    <select id="studyProgram" class="form-control">
                      
                    <option value="">--Study Program--</option><option value="16">Arts and Media</option><option value="17">Beauty Therapy</option><option value="18">Business and Enterprise</option><option value="30">Community and Social Care</option><option value="19">Community Learning</option><option value="20">Computing and ICT</option><option value="21">Construction</option><option value="36">Courses</option><option value="22">Distance and E-Learning</option><option value="23">Early Years</option><option value="25">Engineering</option><option value="24">Entry Pathways</option><option value="26">ESOL</option><option value="38">Exercise and Fitness</option><option value="27">GCSES</option><option value="28">Hairdressing</option><option value="29">Health</option><option value="31">Hospitality and Catering</option><option value="32">International Students courses</option><option value="40">Other</option><option value="15">OtherA Levels (AS and A2)</option><option value="35">Professional part-time and Short</option><option value="33">Public Services</option><option value="34">Science</option><option value="37">Sport</option><option value="39">Travel and Tourism</option></select>
                  </div>
                  </div>
                  <input type="hidden" id="studyProgramId"/>
                </div>
				</form>
                </div>

                <div class="col-md-6">
                    <form class="form-horizontal">
                    
                  <div  class="form-group">
                  <label class="control-label col-md-3" for="inputCompanyName">Surname Name </label>
                  <div class="col-md-8">
                    <input type="text" id="inputCompanyName"  class="form-control" tabindex="2">
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label col-md-3" for="inputFirst">Email Address </label>
                  <div class="col-md-8">
                    <input type="text" id="inputFirst" class="form-control"  tabindex="4">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3" for="inputLast">Password </label>
                  <div class="col-md-8">
                    <input type="password" id="inputLast"  class="form-control"  tabindex="6">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3" for="inputLast">Living Post Code </label>
                  <div class="col-md-8">
                    <input type="password" id="inputLastPass" class="form-control" tabindex="8">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3"  for="inputLast">Level of study </label>
                  <div class="controls fieldinput">
                    <div class="col-md-8">
                        <select  id="lvlOfStudy" class="form-control">                         
                       <option value="">--Study Level--</option><option value="3">Certificate Program</option></select>
                  </div>
                  </div>
                 
                </div>
                <div class="form-group">
                  <label  class="control-label col-md-3" for="inputLast">Code </label>
                  <div class="col-md-8">
                    <input type="password" id="inputLastPass" class="form-control" tabindex="12">
                  </div>
                </div>
                  
				</form>
                </div>
                <div class="clearfix"></div>
                <div class="row">
      <h3 class="lightblue">&nbsp;&nbsp;What can student vibez help with</h3> 
        <div class="col-md-12">
          <div class="control-group">
                      <h5><strong>Accommodation</strong></h5>
                  <div class="controls col-md-4">
                   <form class="form-horizontal">
                   			
                              <div class="checkbox"> 
                                <label class="checkbox">
                                  <input type="checkbox" value="option1" id="inlineCheckbox1"> Bed &amp; Breakfast                   
                                </label>
                            </div>
                            
                             <div class="checkbox"> 
                           		 <label class="checkbox">
                          			<input type="checkbox" value="option1" id="inlineCheckbox1"> Campgrounds &amp; Caravan Parks                  
                                  </label>
                             </div>
                           
                            <div class="checkbox">   
                               <label class="checkbox">
                              	<input type="checkbox" value="option1" id="inlineCheckbox1"> Crisis Care Accommodation                 
                              </label>
                            </div>
                            
                             <div class="checkbox">  
                              <label class="checkbox">
                         		 <input type="checkbox" value="option1" id="inlineCheckbox1"> Hostels                   
                         	  </label>
                              </div>
                              
                               <div class="checkbox"> 
                                 <label class="checkbox">
                          			<input type="checkbox" value="option1" id="inlineCheckbox1"> Holiday Resorts                  
                                  </label>
                               </div>
                          </form>
                      
                  </div>
                  
                  <div class="controls col-md-4">   
                   <form class="form-horizontal">
                   		<div class="checkbox">
                        <label class="checkbox">
                          <input type="checkbox" value="option1" id="inlineCheckbox1"> Hotels                   
                        </label>
                        </div>
                        <div class="checkbox">
                         <label class="checkbox">
                          <input type="checkbox" value="option1" id="inlineCheckbox1"> Mobile Homes                  
                         </label>
                         </div>
                         
                          <div class="checkbox">
                          <label class="checkbox">
                         	 <input type="checkbox" value="option1" id="inlineCheckbox1"> Motels                  
                           </label>
                           </div>
                           
                            <div class="checkbox">
                            <label class="checkbox">
                         	  <input type="checkbox" value="option1" id="inlineCheckbox1"> Serviced Apartments                   
                            </label>
                            </div>
                            
                             <div class="checkbox">
                               <label class="checkbox">
                          			<input type="checkbox" value="option1" id="inlineCheckbox1"> Student Halls                 
                               </label>
                 			 </div>
                             
                              <div class="checkbox">
                                  <label class="checkbox">
                          			<input type="checkbox" value="option1" id="inlineCheckbox1"> Accommodation                  
                              </label> 
                              </div>              
                          </form>
                          
                                    </div>
                          
                          <div class="controls col-md-4">
                   <form class="form-horizontal">
                   			
                              <div class="checkbox"> 
                                <label class="checkbox">
                                  <input type="checkbox" value="option1" id="inlineCheckbox1"> Bed &amp; Breakfast                   
                                </label>
                            </div>
                            
                             <div class="checkbox"> 
                           		 <label class="checkbox">
                          			<input type="checkbox" value="option1" id="inlineCheckbox1"> Campgrounds &amp; Caravan Parks                  
                                  </label>
                             </div>
                           
                            <div class="checkbox">   
                               <label class="checkbox">
                              	<input type="checkbox" value="option1" id="inlineCheckbox1"> Crisis Care Accommodation                 
                              </label>
                            </div>
                            
                             <div class="checkbox">  
                              <label class="checkbox">
                         		 <input type="checkbox" value="option1" id="inlineCheckbox1"> Hostels                   
                         	  </label>
                              </div>
                              
                               <div class="checkbox"> 
                                 <label class="checkbox">
                          			<input type="checkbox" value="option1" id="inlineCheckbox1"> Holiday Resorts                  
                                  </label>
                               </div>
                               
                               
                                <!-- Buttons -->
                                          <div class="form-group" style="margin-top:20px;">
                                             <!-- Buttons -->
											 <div class=" col-md-offset-4"> 
												<button type="submit" class="btn btn-primary">Register</button>
												<button type="reset" class="btn btn-primary">Reset</button>
											</div>
                                          </div>
                                    
                                             Already have an Account? <a href="login.html">Login</a>
                                      <br>
                               
                               
                          </form>
                      
                  </div>
                          
                  </div> 
            </div>
        </div>
                
                
              </div>
              
<!-- Populate regions based on selected country : Start -->
<script>
$(document).ready(function(){
    //level of sudy for institutes
  $("#stdInstitute").change(function(){
      var instId = $(this).find(":selected").val();
      var studyList = <?php print($study_list); ?>;

      $("#stdInstituteId").val(instId);
      var studyListForInstitute = $.grep(studyList,function(object, index){
        return object.institute == instId;
      });

      $('#lvlOfStudy').find('option').remove();
      $('#lvlOfStudy').append( new Option("--Study Level--","",false,false) );

      var defaultSelected = false;
      var nowSelected     = false;
      var text = "";
      var val = ""; 
          
      for(var i=0; i < studyListForInstitute.length; i++){
        text = studyListForInstitute[i].level;
        val = studyListForInstitute[i].id;
        $('#lvlOfStudy').append( new Option(text,val,defaultSelected,nowSelected) );
      }

  });
  
    // program list for the selected level
    $("#lvlOfStudy").change(function(){
      var selctID = $(this).find(":selected").val();
      var programList = <?php print($program_list); ?>;

      $("#studyProgramId").val(selctID);

      var programListForLevel = $.grep(programList,function(object, index){
        return object.level == selctID;
      });

      $('#studyProgram').find('option').remove();
      $('#studyProgram').append( new Option("--Study Program--","",false,false) );


      var defaultSelected = false;
      var nowSelected     = false;
      var text = "";
      var val = ""; 
          
      for(var i=0; i < programListForLevel.length; i++){
        text = programListForLevel[i].program;
        val = programListForLevel[i].id;
        $('#studyProgram').append( new Option(text,val,defaultSelected,nowSelected) );
      }

  });
});
</script>