<?php
require_once '../bootstrap.php';
require_once('includes/auth.php');


$member_id  =   $_SESSION['SESS_MEMBER_ID'];
$firstname  =   "";
$lastname   =   "";
$login      =   "";
$passwd     =   "";
$isadmin    =   "";
        
$action = "";
$usersResult  = "";


$objUser = new User();

$userInfo = $objUser->getUser($member_id);
$member_id  =   $userInfo->member_id;
$firstname  =   $userInfo->firstname;
$lastname   =   $userInfo->lastname;
$login      =   $userInfo->login;
$passwd     =   $userInfo->passwd;
$isadmin    =   $userInfo->isadmin;

if ($_POST) {

    $action = $_POST['txtAction'];

    $member_id = ($action == 'add') ? "" : $_POST['txtId'];
    $firstname  =   $_POST['txtFirstName'];
    $lastname   =   $_POST['txtLastName'];
    $login      =   $_POST['txtEmail'];
    $passwd     =   $_POST['password'];
    $isadmin    =   $_POST['cmbIsAdmin'];
    
    $objUserData = new stdClass();
    $objUserData->member_id = $member_id;
    $objUserData->firstname = $firstname;
    $objUserData->lastname = $lastname;
    $objUserData->login = $login;
    if($passwd){
        $objUserData->passwd = md5($passwd);
    }
    $objUserData->isadmin = $isadmin;
    

    
    $isRecordUpdated = $objUser->changePassword($objUserData);
   $newPageUrl = "changepassword.html";

      print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Your password has been updated!<br /><br /><br /><a href='$newPageUrl'  class='mybutton' style='padding-right:20px;float:none;'>Continue</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
   /*
   echo "<div id='coverit'></div><div id='message'>Your password has been updated!<br /><br /><br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>OK</a></div>";
   */
 
}

$LAYOUT = ADMIN_LAYOUT_PATH."layout.tpl.php";
$CONTENT = ADMIN_LAYOUT_PATH."tpl/user/changePassword.tpl";
	
require_once $LAYOUT;
?>
