<?php

/**
 * @package     Registration Module
 * @copyright   2013 Monara IT UK Ltd
 * @license     http://www.pluspro.com/license/1_0.txt   Pluspro License 3.0
 * @version    	1.0.0.0
 * @author     	Gayan Chathuranga
 *              Developer -  Monara IT UK Ltd
 *              gayan.c@pluspro.com
 */
class Registration extends Core_Database {

    public $id;
    public $firstName;
    public $surName;
    public $dateOfBirth;
    public $password;
    public $livingAt;
    public $eduInstitute;
    public $levelOfStudy;
    public $areaOfStudy;
    public $code;
    public $status;
    public $lang;
    public $createdOn;
    public $createdBy;
    public $lastModifiedOn;
    public $lastModifiedBy;

    //constructor
    function __construct() {
        try {
            parent::connect();
            $this->isModuleExists();
        } catch (Exception $exc) {
            throw new PlusProException("Error Connecting to the Database <br/>
                    " . $exc->file . "<br/>" . $exc->line);
        }
    }

    /* check for module existance
     */

    public function isModuleExists() {
        //TODO: implement the functionality
    }

    /* create module related tables
     */

    public function installModule() {
        //TODO: implement the functionality
    }

    /* Get Student Support List
     */

    public function getStudentSupportList() {
       $data_array = array();
        try {
            $colums = '*';
            $where = ''; 
            $orderBy = "name ASC";
            $this->select('set_support_list', $colums, $where, $orderBy);
            $supportInfo = $this->getResult();
            foreach ($supportInfo as $key => $data) {
                $objSupportList = new stdClass();
                $objSupportList->id = $data['id'];
                $objSupportList->name = $data['name'];
                $objSupportList->description = $data['description'];

                array_push($data_array, $objSupportList);
            }
            return $data_array;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /* Get business main category list
    */
    public function getBizCategory($id){
        $objBizCategory = new stdClass();
        try {                            
                $SQL = "SELECT * FROM set_biz_category";
                $SQL.= " WHERE id = ".$id;
                $this->executeSelectQuery($SQL);
                $categoryInfo = $this->getResult();
                //var_dump($categoryInfo);exit();
                 foreach ($categoryInfo as $row => $catRow) {
                    $objBizCategory->name = $catRow['name'];
                    $objBizCategory->description = $catRow['description'];
                    $objBizCategory->subCategories = $this->getBizSubCategory($id);
                }
            return $objBizCategory;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /* Get institute data
    */
    public function getInstitute($id){
        $objInstitute = new stdClass();
        try {                            
                $SQL = "SELECT * FROM set_edu_institute";
                $SQL.= " WHERE id = ".$id;
                $this->executeSelectQuery($SQL);
                $instituteInfo = $this->getResult();
                 foreach ($instituteInfo as $row => $dataRow) {
                    $objInstitute->id = $dataRow['id'];
                    $objInstitute->name = $dataRow['name'];
                    $objInstitute->description = $dataRow['description'];
                }
            return $objInstitute;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /* Get institute data
    */
    public function getStudyLevel($id){
        $objStudyLevel = new stdClass();
        try {                            
                $SQL = "SELECT * FROM set_study_level";
                $SQL.= " WHERE id = ".$id;
                $this->executeSelectQuery($SQL);
                $dataInfo = $this->getResult();
                 foreach ($dataInfo as $row => $dataRow) {
                    $objStudyLevel->id = $dataRow['id'];
                    $objStudyLevel->institute = $dataRow['institute'];
                    $objStudyLevel->level = $dataRow['level'];
                    $objStudyLevel->description = $dataRow['description'];
                }
            return $objStudyLevel;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /* Get study program
    */
    public function getStudyProgram($id){
        $objStudyProgram = new stdClass();
        try {                            
                $SQL = "SELECT * FROM set_study_area";
                $SQL.= " WHERE id = ".$id;
                $this->executeSelectQuery($SQL);
                $dataInfo = $this->getResult();
                 foreach ($dataInfo as $row => $dataRow) {
                    $objStudyProgram->id = $dataRow['id'];
                    $objStudyProgram->level = $dataRow['level'];
                    $objStudyProgram->program = $dataRow['program'];
                    $objStudyProgram->description = $dataRow['description'];
                }
            return $objStudyProgram;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    public function getBizSubCategory($id){
        $data_array = array();
        try {                            
                $SQL = "SELECT * FROM set_biz_category";
                $SQL.= " WHERE parent = ".$id;
                $this->executeSelectQuery($SQL);
                $categoryInfo = $this->getResult();
                //var_dump($categoryInfo);exit();
                 foreach ($categoryInfo as $row => $catRow) {
                    $objBizSubCategory = new stdClass();
                    $objBizSubCategory->name = $catRow['name'];
                    $objBizSubCategory->description = $catRow['description'];
                    $objBizSubCategory->parent = $catRow['parent'];
                    array_push($data_array, $objBizSubCategory);
                }
            return $data_array;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    public function getAllBizCategory($parent=0) {
         $arrBizCategory = array();
        try {
              $SQL = "SELECT id FROM set_biz_category";
              $SQL.= " WHERE parent = 0";
              $SQL.= ' ORDER BY id ASC';
              $this->executeSelectQuery($SQL);
                $catResult = $this->getResult();
                foreach ($catResult As $catRow) {
                   $id = $catRow['id'];                  
                   $catInfo = $this->getBizCategory($id);
                    array_push($arrBizCategory, $catInfo);
                }
            return $arrBizCategory;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    public function getAllInstitues(){
        $arrInstitute = array();
        try {
              $SQL = "SELECT id FROM set_edu_institute";
              $SQL.= ' ORDER BY name ASC';
              $this->executeSelectQuery($SQL);
                $rsltData = $this->getResult();
                foreach ($rsltData As $dataRow) {
                   $id = $dataRow['id'];                  
                   $dataInfo = $this->getInstitute($id);
                    array_push($arrInstitute, $dataInfo);
                }
            return $arrInstitute;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    
    /* Get all Level of Studies
    */
    public function getAllLevelOfStudies(){
        $arrData = array();
        try {
              $SQL = "SELECT id FROM set_study_level";
              $SQL.= ' ORDER BY institute ASC';
              $this->executeSelectQuery($SQL);
                $rsltData = $this->getResult();
                foreach ($rsltData As $dataRow) {
                   $id = $dataRow['id'];                  
                   $dataInfo = $this->getStudyLevel($id);
                    array_push($arrData, $dataInfo);
                }
            return $arrData;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }
    
    
    /* Get Program List
    */
    public function getAllProgramList(){
        $arrData = array();
        try {
              $SQL = "SELECT id FROM set_study_area";
              $SQL.= ' ORDER BY program ASC';
              $this->executeSelectQuery($SQL);
                $rsltData = $this->getResult();
                foreach ($rsltData As $dataRow) {
                   $id = $dataRow['id'];                  
                   $dataInfo = $this->getStudyProgram($id);
                    array_push($arrData, $dataInfo);
                }
            return $arrData;
        } catch (Exception $e) {
            throw new PlusProException("<strong>Oops !, Error Class name -</strong>  <em>Page</em>, <strong>Function -</strong> <em>getPage()</em>, <strong>Exception -</strong> <em>" . $e->getMessage() . "</em>");
        }
    }

    /* get widget checkout
     */

    public function getRegisterFormWidget() {
        /* http://php.net/manual/en/function.ob-get-clean.php
         */
        ob_start();
        $support_list = $this->getStudentSupportList();
        $biz_categories = $this->getAllBizCategory();
        $institue_list = $this->getAllInstitues();
        $study_list = json_encode($this->getAllLevelOfStudies());
        $program_list = json_encode($this->getAllProgramList());
        //var_dump($program_list);exit;
        $view = include('widgets/register.tpl');
        $view = ob_get_clean();
        echo json_encode($view);
        exit();
    }

}

?>