<?php 
require_once '../../../../bootstrap.php';
require_once('../../../includes/auth.php');


$pageNumber     = "";
$searchQ        = "";
$recLimit       = "";
$orderBy        = "";
$order          = "";
$orderStr       = "";
//--- pagination --------------------------

$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;

if(isset($_GET)){
    
             // get the search query....
            if(isset($_GET['m'])){
                $module   = $_GET['m'];
            }
            
            // the page numbers............
            if(isset($_GET['page'])){
                $pageNumber = $_GET['page'];
            } else {
                $pageNumber = 1;
            }
            
            // get the search query....
            if(isset($_GET['q'])){
                $searchQ   = $_GET['q'];
            }
            
            
            // get the order list...
            if(isset($_GET['orderby'])){
                $orderBy = $_GET['orderby'];
            }
            
            if (!isset($_GET['order'])){
                $order = 'Dsc';
            }

           else {
                
                $orderStr       = 'id'." ".'Asc';  
            }
            
            if(isset($_GET['rows'])){
                $_REC_PER_PAGE = $_GET['rows'];
            }

}



//--------------------------------------------

$objReg = new Registration();
$objReg->tb_name = 'trn_student_info';

$objReg->searchStr = $searchQ;
$objReg->limit = $recLimit;
$objReg->listingOrder = $orderStr;
$StudentResult = $objReg->search();



$recStart = ($pageNumber-1)*$_REC_PER_PAGE;
$recLimitTo = $_REC_PER_PAGE;
$recLimit    = " LIMIT ".$recStart.",".$recLimitTo;
//--------------------------------------------

?>


<script type="text/javascript">
$(document).ready(function(){ 	
	  function slideout(){
  setTimeout(function(){
  $("#response").slideUp("slow", function () {
      });
    
}, 2000);}
	
    $("#response").hide();
	$(function() {
	$("#list ul").sortable({ opacity: 0.8, cursor: 'move', update: function() {
			
			var order = $(this).sortable("serialize") + '&update=update'; 
			$.post("<?php print(ADMIN_BASE_URL); ?>modules/<?=$module?>/includes/updateListingOrder.php", order, function(theResponse){
				$("#response").html(theResponse);
				$("#response").slideDown('slow');
				slideout();
			}); 															 
		}								  
		});
	});

});	

function deleterow(id){


	
if (confirm('Are you sure want to delete?')) {
	 $.post('../modules/registration/includes/deletstudent.php', {id: +id, ajax: 'true' },
function(){
$("#arrayorder"+id).fadeOut("slow");

//$(".message").delay(2000).fadeOut(1000);
});
}
}

</script>


<div id="list">
<div id="table_main_div" >
     <div id="response"> </div>
                    	<div class="row_heding">
                        	
                                <div class="colum" style="width:70px;">
                                    <strong>ID&nbsp;</strong>
                                   
                                </div>
                                <div class="colum" style="width:150px;">
                                    <strong>First Name</strong>
                                    
                                </div>
                                
                                
                                 <div class="colum" style="width:230px;">
                                    <strong>Surname</strong>
                                    
                                </div>
                                
                                  <div class="colum" style="width:70px;">
                                    <strong>Status</strong>
                                    
                                </div>
                                
                                <div class="colum_right" align="right" style="margin-top:2px; width:125px;"></div>
                            </div>
                            <div class="clear"></div>
                    	
                            <ul>
                                  <?php 
      if(count($StudentResult) >0){

      foreach($StudentResult As $rowIndex=>$studentData) {?> 
                                <li id="arrayorder<?php print($studentData->id);?>">
                                    <?php if($rowIndex%2 != 0){?>
                                    <div class="row1">
                                    <?php } else {?>
                                    <div class="row2">
                                    <?php } ?>
                        	<div class="colum" style="width:70px;"><?php print($studentData->id);?></div>
                            <div class="colum" style="width:150px;"> <?php print($studentData->firstName);?> </div>
                          <div class="colum" style="width:230px;"> <?php print($studentData->surname);?> </div>
                          <div class="colum" style="width:70px;"> <?php print($studentData->status);?> </div>
                            <div class="colum_right" align="right" style="margin-top:2px;  width:125px;">
                                

                            
                              

                                <a href="<?=$module?>.html?action=edit&id=<?php print($studentData->id);?>">
                                    <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/edit.png" alt="" width="16" height="16" border="0" title="Edit" />
                                </a>&nbsp;&nbsp;&nbsp;&nbsp;
                            

                                <a href="javascript:deleterow(<?php echo $studentData->id; ?>);">
                                    <img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/delete.png" alt="" width="16" height="16" border="0" title="Delete"  />
                                </a>
                            </div>
                            </div>
                            <div class="clear"></div>
                            </li>
                                
                                <?php } }else{?>
                            <li>
                                <div id="listings" style="background:#eee;">No Matching Records Found </div>
                            </li>
                            <?php }?>
                            
                        </ul>
                       
                        
                       
                    </div>
                            
                            </div>
















