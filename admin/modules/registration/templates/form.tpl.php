<script src="<?php print(ADMIN_BASE_URL);?>ckeditor/ckeditor.js"></script>

<script type="text/javascript">
	$(document).ready(
		function()
		{
                       
                        // the small image button
			var btnUpload=$('#btnImg');
			new AjaxUpload(btnUpload, {
				action: "uploadFile.php",
				name: 'uploadfile',
				onSubmit: function(file, ext){
					 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
	                    // extension is not allowed
						status.text('Only JPG, PNG or GIF files are allowed');
						return false;
					}
					 var prg_image = "<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/uploader50.gif";
                                        $("#btnImg").attr("src",prg_image);
				},
				onComplete: function(file, response){
					//Add uploaded file to list
					if(response!="error"){
						//$('<li></li>').appendTo('#files').html('<img src="../content/uploads/photos/'+file+'" alt="" /><br />'+file).addClass('success');
	                                        var new_image = "<?php print(SITE_BASE_URL); ?>imgs/"+response;
	                                        $("#btnImg").attr("src",new_image);
	                                        $("#txtSmallFileName").attr("value", response);
					} else{
                                                alert("Error, when upload the file");
					}
				}
			});
                    
                        
                        
		}
	);
	</script>

<div id="search_main_wrapper">

                <h2>Add/Edit</h2>
    </div>


    

    <div id="table_main_wrapper">



            <div id="dashboard" style="background-color:#FFF;">

            <form name="form1" id="form1" action="" method="post">
            
                     <div id="two">
                     
                  
                        
                        
                
                      
                        
                        
                         <fieldset style="margin-top: 20px;">
                   
                    
                    <div id="separator">
                        <label for="headline">First Name: </label>
                        <input name="fname" type="text" id="fname" value="<?php echo $firstName; ?>" size="65" />
                    </div>
                   
                   
                    <div id="separator">
                        <label for="headline">Last Name: </label>
                        <input name="lname" type="text" id="last_name" value="<?php echo $surname; ?>" size="65" />
                    </div>
                   
                    <div id="separator">
                        <label for="headline">Password: </label>
                        <input name="password" type="text" id="last_name" value="<?php echo $password; ?>" size="65" />
                    </div>
                   
                   
                     <div id="separator">
                        <label for="headline">Date of Birth: </label>
                        <input name="dob" type="text" id="dob" value="<?php echo $date_of_birth; ?>" size="65" />
                    </div>
                   
               
                     <div id="separator">
                        <label for="headline">E-mail </label>
                        <input name="email" type="text" id="email" value="<?php echo $email; ?>" size="65" />
                    </div>
 
 
 					 <div id="separator">
                        <label for="headline">Area of Study</label>
                      
                        
                        
                          <select id="course" name="course" style="width: 200px;">
							
								<?php 
                      	foreach ($studyArea as $index => $area) {?>
                      	
                      <option value="<?php echo $area->id; ?>"  <?php if ($area_of_study == $area->id) echo "selected='selected'" ?>><?php echo $area->program; ?></option>
                     <?php }?>
						</select>
                        
                        
                        
                        
                    </div>
 
 
  <div id="separator">
                        <label for="headline">Level of study </label>
                       
                        
                        
                        
                           <select id="level_of_study" name="level_of_study" style="width: 200px;">
							
								<?php 
                      	foreach ($studyLevel as $index => $levels) {?>
                      	
                      <option value="<?php echo $levels->id; ?>"  <?php if ($level_of_study == $levels->id) echo "selected='selected'" ?>><?php echo $levels->level; ?></option>
                     <?php }?>
						</select>
                        
                        
                        
                    </div>
                    
                    
 					 <div id="separator">
                        <label for="headline">Code </label>
                        <input name="code" type="text" id="code" value="<?php echo $code; ?>" size="65" />
                    </div>
 					
                   <div id="separator">
                        <label for="headline">Educational Institute  </label>
                        
                        
                        
                        
                        <select
							id="edu_institute" name="edu_institute" style="width: 200px;">
							
								<?php 
                      	foreach ($objInstitute as $index => $institute) {?>
                      	
                      <option value="<?php echo $institute->id; ?>"  <?php if ($edu_institute == $institute->id) echo "selected='selected'" ?>><?php echo $institute->description; ?></option>
                     <?php }?>
						</select>
                        
                        
                        
                        
                        
                    </div>
                    
                    	 <div id="separator">
                        <label for="headline">Living Post Code</label>
                        <input name="postCode" type="text" id="code" value="<?php echo $living_at; ?>" size="65" />
                    </div>
                      
                        

                </fieldset>
                     </div>
                <p>
                    <input type="hidden" name="txtAction" id="txtAction" value="<?php echo $action; ?>"/>
                    <input type="hidden" name="display_order" id="display_order" value="<?php echo $displayorder; ?>"/>
                    <input id="testbutton" type="submit" value="submit" name="Submit" /> 
                    <input id="resetbutton" type="reset"  />
                </p>

            </form>

<div class="clear">
        </div> 

        </div>



        <div style="height:10px;"></div>
    </div>	
    
    
    
    
    
        <script>
CKEDITOR.replace( 'descriptionB' , {
extraPlugins: 'stylesheetparser',
// Stylesheet for the contents.
contentsCss: '<?php print(SITE_BASE_URL."includes/templates/".SITE_TEMPLATE."/");?>contents/css/styles.css',
// Do not load the default Styles configuration.
stylesSet: [],
toolbar: [
{items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
'/',
{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline'] },
	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ] },
	{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
	{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
	'/',
	{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
	{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
],
filebrowserBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html',
filebrowserImageBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html?Type=Images',
//filebrowserFlashBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html?Type=Flash',
filebrowserUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
filebrowserImageUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
//filebrowserFlashUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
height: 200
});
</script>
    
    <script>
CKEDITOR.replace( 'city' , {
extraPlugins: 'stylesheetparser',
// Stylesheet for the contents.
contentsCss: '<?php print(SITE_BASE_URL."includes/templates/".SITE_TEMPLATE."/");?>contents/css/styles.css',
// Do not load the default Styles configuration.
stylesSet: [],
toolbar: [
{items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
'/',
{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline'] },
	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ] },
	{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
	{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
	'/',
	{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
	{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
],
filebrowserBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html',
filebrowserImageBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html?Type=Images',
//filebrowserFlashBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html?Type=Flash',
filebrowserUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
filebrowserImageUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
//filebrowserFlashUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
height: 200
});
</script>
