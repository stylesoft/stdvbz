<?php
require_once '../../../bootstrap.php';
require_once('../../includes/auth.php');



$id                             = '';
$firstName                      = '';
$surname                       = '';
$date_of_birth                          = '';
$email                       = '';
$password                         = '';
$living_at                     = '';
$edu_institute                  = '';
$area_of_study                = '';
$level_of_study ='';
$code                		  = '';
$status               			 = 'pending';
$lang               			 = '';
$created_on                 = getCurrentDateTime();
$created_by                = '';
$last_modified_on               = '';
$last_modified_by                = '';






// listings.....
$_REC_PER_PAGE = ADMIN_RECORDS_PER_PAGE;
$_SEARCH_QUERY = "";


$invalid = false;

$lastInsertedId = "";
$isRecordUpdated = "";

$action = '';
//$arrBrand= array();

$objReg = new Registration();
$objReg->tb_name = 'trn_student_info';


$objInstitute = $objReg->getAllInstitues();
$studyLevel = $objReg->getAllLevelOfStudies();
$studyArea = $objReg->getAllProgramList();

//$result = $objReg->search();
//print_r($result); exit;

if (isset($_GET['action'])) {
    $action = $_GET['action'];
    if (isset($_GET['id'])) { $id = $_GET['id'];}
                //$arrBrand = $objReg->getAllParentByStatus('Enabled',$id);
                
    if ($action == 'edit') {      

    	
    	
            $StudentInfo = $objReg->getsStudent($id);
//print_r($StudentInfo);
            
$id                             = $StudentInfo->id;
$firstName                      = $StudentInfo->firstName;
$surname                       = $StudentInfo->surname ;
$date_of_birth                          = $StudentInfo->date_of_birth;
$email                       = $StudentInfo->email ;
//$postCode = $StudentInfo->postCode;
$password                         = $StudentInfo->password ;
$living_at                     = $StudentInfo->living_at ;
$edu_institute                  = $StudentInfo->edu_institute;
$level_of_study					=$StudentInfo->level_of_study;
$area_of_study                = $StudentInfo->area_of_study;
$code                		  = $StudentInfo->code;
$status               			 =$StudentInfo->status;
$lang               			 = $StudentInfo->lang;
$created_on                 = $StudentInfo->created_on;
$created_by                = $StudentInfo->created_by;
$last_modified_on               = $StudentInfo->last_modified_on;
$last_modified_by                = $StudentInfo->last_modified_by;
        
    }
    
   
}

if ($_POST) {
//print_r($_POST); exit;
    $action = $_POST['txtAction'];

    if(isset($_POST['id'])) $id = $_POST['id'];
    
    
$firstName                    = $_POST['fname'];
$surname                      = $_POST['lname'];
$email                        = $_POST['email'];
$area_of_study                = $_POST['course'];
$code                		  = $_POST['code'];
 $level_of_study     = $_POST['level_of_study'];

 $date_of_birth = $_POST['dob'];
$edu_institute                         = $_POST['edu_institute'];

$living_at                     = $_POST['postCode'];
$edu_institute                  = $_POST['edu_institute'];
$password                		  = $_POST['password'];



//$status               			 = 'pending';
//$lang               			 = '';
//$created_on                 = getCurrentDateTime();
//$created_by                = '';
//$last_modified_on               = '';
//$last_modified_by                = '';


    //validation
  
        

    $objReg->id = $id;
    $objReg->firstName = $firstName;
    
    
    $objReg->surname = $surname;
$objReg->date_of_birth = $date_of_birth;
$objReg->email                       = $email;
$objReg->password                         = $password;
$objReg->living_at                     = $living_at;
$objReg->edu_institute                  = $edu_institute;
$objReg->area_of_study                = $area_of_study;
$objReg->code                		  = $code;

$objReg->level_of_study               = $level_of_study;

//$objReg->status               			 = 'pending';
//$objReg->lang               			 = '';
//$objReg->created_on                 = getCurrentDateTime();
//$objReg->created_by                = '';
//$objReg->last_modified_on               = '';
//$objReg->last_modified_by                = '';
    
    
    
    if ($action == 'add') {
    	 $objReg->id = '';
          $lastInsertedId = $objReg->addBrand();
        
    }elseif ($action == 'edit' && !$invalid) {
        $isRecordUpdated = $objReg->editStudent();
    }

    
    $newPageUrl = "registration.html?action=add";
    $mainPageUrl = "registration.html";

    if ($lastInsertedId) {
        
        print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Brand <span class='red'>$first_name</span> has been added!<br /><br /><a href='$mainPageUrl'  class='mySmallButton'>ok</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
        //echo "<div id='coverit'></div><div id='message'>Category $category_name <span class='red'>$banner_title</span> has been added!<br /><br /><br />Add Another Category?<br /><br /><a href='$newPageUrl'  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    } else if ($isRecordUpdated) {
        
        
         print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
    print("Registration <span class='red'>$first_name</span> details has been added!<br /><br /><a href='$mainPageUrl'  class='mySmallButton'>ok</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
        
       // echo "<div id='coverit'></div><div id='message'>Category $category_name <span class='red'>$banner_title</span> has been updated!<br /><br /><br />Edit Category?<br /><br /><a href=''  class='mySmallButton' style='margin-right:20px'>Yes</a><a href='$mainPageUrl'  class='mySmallButton'>No</a></div>";
    }
} else {
	
 if (isset($_GET['q'])) {
            $_SEARCH_QUERY = $_GET['q'];
        }



        if (isset($_GET['rows'])) {
            $_REC_PER_PAGE = $_GET['rows'];
        }


    $_SEARCH_QUERY = str_replace(" ","",trim($_SEARCH_QUERY));
    // get all the news details

    $objReg->searchStr = $_SEARCH_QUERY;
    $totalNumberOfMenus = $objReg->countRec();
    $pageNumbers = ceil($totalNumberOfMenus / $_REC_PER_PAGE);
}




if ($action == 'edit' || $action == 'add') {
    $CONTENT = MODULE_TEMPLATES_PATH . "form.tpl.php";
}else {
    $CONTENT = MODULE_TEMPLATES_PATH . "index.tpl.php";
}

$LAYOUT = ADMIN_LAYOUT_PATH . "layout.tpl.php";



require_once $LAYOUT;
?>
