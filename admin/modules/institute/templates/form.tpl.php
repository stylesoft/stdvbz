<script src="<?php print(ADMIN_BASE_URL);?>ckeditor/ckeditor.js"></script>
<script type="text/javascript">
	$(document).ready(
		function()
		{
                        
                        // the small image button
			var btnUpload=$('#btnImg');
			new AjaxUpload(btnUpload, {
				action: '../uploadFile.php',
				name: 'uploadfile',
				onSubmit: function(file, ext){
					 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
	                    // extension is not allowed
						status.text('Only JPG, PNG or GIF files are allowed');
						return false;
					}
					 var prg_image = "<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/uploader50.gif";
                                        $("#btnImg<?php print($uploaderX);?>").attr("src",prg_image);
				},
				onComplete: function(file, response){
					//Add uploaded file to list
					if(response!="error"){
						//$('<li></li>').appendTo('#files').html('<img src="../content/uploads/photos/'+file+'" alt="" /><br />'+file).addClass('success');
	                                        var new_image = "<?php print(SITE_BASE_URL); ?>imgs/"+response;
	                                        $("#btnImg").attr("src",new_image);
	                                        $("#txtSmallFileName").attr("value", response);
					} else{
                                                alert("Error, when upload the file");
					}
				}
			});
                       
                        
                        
		}
	);
	</script>

<div id="search_main_wrapper">

                <h2>Add/ Edit Feature</h2>
    </div>


    

    <div id="table_main_wrapper">



        <div id="dashboard" style="background-color:#FFF;">

            <form name="form1" id="form1" action="" method="post">
            
                     <div id="two">
                     
                     <h2>Header Info</h2>
                <div>   
                    <fieldset>

                       

                            <div id="separator">
                        <label for="headline">Institute Name : </label>
                        <input name="name" type="text" id="category_name" value="<?php echo $name; ?>" size="44" />
                    </div>



                       



                      <div id="separator">
                        <label for="headline">Description : </label>
                        <textarea name="description"  id="category_description" tabindex="7"><?php print $description;?></textarea>
  
                    </div>
                    
                        <div class="clear"></div> 

                    </fieldset>
                    
                    
                    
                    
                    
                    
                    



                </div>
                     
                     <div style="display: none;">
                      <h2>Contents</h2>
                     <fieldset style="margin-top: 20px;" >
                     
                      <?php
                     
                        if($categoryPageBanner != ''){
                        $contentImage = SITE_BASE_URL.'imgs/'.$categoryPageBanner;
                    } else {
                         $contentImage = GLOBAL_ADMIN_CONTENT_PATH."images/no-image-icon.png";
                    }
                    ?>
                    <div id="separator">
                
                    <label for="lcol_title">Image </label>
                    <img src="<?php print($contentImage);?>" height="50" width="50" name="btnImg" id="btnImg"/>
                    <input type="hidden" id="txtSmallFileName" name="txtSmallFileName" value="<?php print($categoryPageBanner);?>" />
                    
                </div>
                    
                    
                    <div id="separator">
                        <label for="headline">Link: </label>
                      <input name="link" type="text" id="link" value="<?php print($link); ?>" size="65" />
  
                    </div>
                   <div class="clear"></div> 
                     
                     </fieldset>
                     </div>
                 
   
                     </div>
                <p>
                    <input type="hidden" name="txtAction" id="txtAction" value="<?php echo $action; ?>"/>
                    <input type="hidden" name="txtDisplayOrder" id="txtDisplayOrder" value="<?php echo $displayOrder; ?>"/>
                    <input id="testbutton" type="submit" value="submit" name="Submit" /> 
                    <input id="resetbutton" type="reset"  />
                </p>

            </form>

<div class="clear"></div> 

        </div>



        <div style="height:10px;"></div>
    </div>	
    
                           <script>
CKEDITOR.replace( 'body_description' , {
extraPlugins: 'stylesheetparser',
// Stylesheet for the contents.
contentsCss: '<?php print(SITE_BASE_URL."includes/templates/".SITE_TEMPLATE."/");?>contents/css/styles.css',
// Do not load the default Styles configuration.
stylesSet: [],
toolbar: [
{items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
'/',
{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline'] },
	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ] },
	{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
	{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
	'/',
	{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
	{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
],
filebrowserBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html',
filebrowserImageBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html?Type=Images',
//filebrowserFlashBrowseUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/ckfinder.html?Type=Flash',
filebrowserUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
filebrowserImageUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
//filebrowserFlashUploadUrl : '<?php print(ADMIN_BASE_URL);?>ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
height:200
});
</script>