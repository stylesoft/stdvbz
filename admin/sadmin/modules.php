<?php
require_once '../../bootstrap.php';
require_once('includes/auth.php');


// get all the modules....
$objModules = new Modules();
$modules    = $objModules->getAll();

$objModuleLinks = new ModuleLink();

if($_POST){
    
    $objModules->disableAll();
    $objModuleLinks->disableAll();
 
    
    $modules        = $_POST['chkModule'];
    $moduleLinks    = $_POST['chkModuleLink'];
    
    
    
    foreach($modules As $mIndex=>$module){
        $objModuleData  = new stdClass();
        $objModuleData->id = $module;
        $objModuleData->isEnabled = 'Yes';
        $objModules->updateStatus($objModuleData);
        
    }
    
    
    foreach($moduleLinks As $mlIndex=>$moduleLink){
        $objModuleLinkData  = new stdClass();
        $objModuleLinkData->id = $moduleLink;
        $objModuleLinkData->isEnabled = 'Yes';
        $objModuleLinks->updateStatus($objModuleLinkData);
        
    }
    
    
    $mainPageUrl = 'modules.html';

	print("<div id='boxes'>");
        print("<div style='top: 199.5px; left: 551.5px; display: none;' id='dialog' class='window'>");
        //The message comes here..
        print("Your <span class='red'>Modules status</span> has been updated!<br /><br /><br /><br /><a href='$mainPageUrl'  class='mybutton' style='padding-right:20px;float:none;'>Continue</a>");
        print("</div>");
        print("<div style='width: 1478px; height: 602px; display: none; opacity: 0.8;' id='mask'></div>");
        print("</div>");
    
    
}



$LAYOUT = SUPER_ADMIN_LAYOUT_PATH."layout.tpl.php";
$CONTENT = SUPER_ADMIN_LAYOUT_PATH."tpl/modules/index.tpl";
	
require_once $LAYOUT;
?>
