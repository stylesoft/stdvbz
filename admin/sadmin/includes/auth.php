<?php	
	//Check whether the session variable SESS_MEMBER_ID is present or not
	if(!isset($_SESSION['SUPER_ADMIN_SESS_MEMBER_ID']) || (trim($_SESSION['SUPER_ADMIN_SESS_MEMBER_ID']) == '')) {
		header("location: login.php?ref=denied");
		exit();
	}
?>
