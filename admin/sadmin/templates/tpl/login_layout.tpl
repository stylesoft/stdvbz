<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php print(SITE_NAME); ?> - Content Management</title>
<!-- CSS -->
<link href="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>loginstyle/css/reset.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>loginstyle/css/layout.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>loginstyle/css/jNice.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>loginstyle/css/hack.css" rel="stylesheet" type="text/css" media="screen" />



<link href="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>css/loginmain.css" rel="stylesheet" type="text/css" media="screen" />
<!--[if IE 6]><link rel="stylesheet" type="text/css" media="screen" href="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>loginstyle/css/ie6.css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" media="screen" href="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>loginstyle/css/ie7.css" /><![endif]-->

<!-- JavaScripts-->
<script type="text/javascript" src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>loginstyle/js/jquery.js"></script>
<script type="text/javascript" src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>loginstyle/js/jNice.js"></script>
</head>

<body>
	<div id="wrapper">
    	<div id="top_wrapper">
        	<div align="center"><img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/admin-logo.png" width="232" height="63" alt="" /></div>
        	<div class="clear"></div>
        </div>
     
      	<div id="loginbox_main">
          
        	<div id="loginbox_sub">
            <br />
            	<p align="center">Please Login.</p>
        	<form  name="loginform"   id="loginform" method="post" action="">
        	<div class="inout_small_div">
                <div style="width:23px; float:left; margin:10px 0 0 10px;"><img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/form_nameicon.png" width="23" height="20" alt="" /></div>
                <input type="text" placeholder="Email*" class="validate[required]" name="login" id="login"/>
           </div>
           	<div class="inout_small_div">
                <div style="width:23px; float:left; margin:10px 0 0 10px;"><img src="<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/pass_icon.png" width="23" height="20" alt="" /></div>
                <input placeholder="Password*" class="validate[required]" name="password" type="password" id="tpassword" />
           </div>
              <div class="clear"></div>
           <div style="height:20px;"></div>
           <div class="btn_leftline_div"><hr /></div>
          <div class="btn_div" align="center">
            <input name="submit" type="submit" value="Login" class="form_btn" />
          </div>
          <div class="btn_leftline_div"><hr /></div>
           <div class="clear"></div>
   		<!--   <h2>Admin Area</h2>
        <div class="clear"></div>
		<p align="center">Please Login.</p>
    	<p align="center"><strong>Forgotten Your Password?</strong> <a href="#" class="request_pass">Request a Password Reset</a></p>
		<br />-->
        </form>
         </div>
        </div>
        <p align="center" class="forgotten"><strong>Forgotten Your Password?</strong> <a href="<?php print(ADMIN_BASE_URL);?>lost_password.php" class="req_pass">Request a Password Reset.</a></p>
  <div style="height:100px;"></div>
       




</div>
    <!-- // #wrapper -->
</body>
</html>
