<script type="text/javascript">
	$(document).ready(
		function()
		{
                       
                        // the small image button
			var btnUpload=$('#btnImg');
			new AjaxUpload(btnUpload, {
				action: 'uploadFile.php',
				name: 'uploadfile',
				onSubmit: function(file, ext){
					 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
	                    // extension is not allowed
						status.text('Only JPG, PNG or GIF files are allowed');
						return false;
					}
					 var prg_image = "<?php print(GLOBAL_ADMIN_CONTENT_PATH);?>images/uploader50.gif";
                                        $("#btnImg").attr("src",prg_image);
				},
				onComplete: function(file, response){
					//Add uploaded file to list
					if(response!="error"){
						//$('<li></li>').appendTo('#files').html('<img src="../content/uploads/photos/'+file+'" alt="" /><br />'+file).addClass('success');
	                                        var new_image = "<?php print(SITE_BASE_URL); ?>imgs/"+response;
	                                        $("#btnImg").attr("src",new_image);
	                                        $("#txtSmallFileName").attr("value", response);
					} else{
                                                alert("Error, when upload the file");
					}
				}
			});
                       
                        
		}
	);
	</script>
<div id="search_main_wrapper">


		
			<h2>Social Media Settings</h2>
			

	
</div>


<div style="clear:both;"></div>

<div id="table_main_wrapper">



	<div id="dashboard" style="background-color: #FFF;">


		<form action="" method="post">
			
<div>
<div  id="two">
    
                        <?php 
                         $contentImage = ""; 
                         if($logo != ''){
                                $contentImage = SITE_BASE_URL.'imgs/'.$logo;
                            } else {
                                $contentImage = GLOBAL_ADMIN_CONTENT_PATH."images/no-image-icon.png";
                            }
                   
                        ?>
    
  <h2>Company Profile</h2>
     <div>
				<fieldset>
					<div id="separator">
						<label for="site_name">Logo : </label> 
                                                <img src="<?php print($contentImage);?>" height="50" width="50" name="btnImg" id="btnImg"/>
                                                <input type="hidden" id="txtSmallFileName" name="txtSmallFileName" value="<?php print($logo);?>" />
                                                <span style="color:red;font-size: 10px;">Clcik on image to change</span>
					</div>
					<div id="separator">
						<label for="site_url">Company Name: </label> 
                                                <input name="txtCompanyName" type="text" id="txtCompanyName" value="<?php print($company_name); ?>" size="64"
							maxlength="64" tabindex="2" />
					</div>
					<div id="separator">
						<label for="base_url">Address </label> <input name="txtAddress"
							type="text" id="txtAddress" value="<?php print($address); ?>"
							size="64" maxlength="64" tabindex="3" />
					</div>
                                    
                                        <div id="separator">
						<label for="base_url">Telephone </label> <input name="txtTelephone"
							type="text" id="txtTelephone" value="<?php print($telephone); ?>"
							size="64" maxlength="64" tabindex="3" />
					</div>
                                    
                                    
					<div id="separator">
						<label for="site_email">Google Map : </label> <input
							name="txtGoogleMap" type="text" id="txtGoogleMap"
							value="<?php print($google_map); ?>" size="64" maxlength="64" tabindex="4" />

					</div>
					
				</fieldset>
         </div>
				<p>
                                    
                                    
                                    
                                
                        <h2>Social Web Sites</h2>        
                        <div>
                           
                            <fieldset>
                            
                            <div id="separator">
						<label for="site_email">Facebook Url : </label> <input
							name="txtFacebookUrl" type="text" id="txtFacebookUrl"
							value="<?php print($facebook_url); ?>" size="64" maxlength="64" tabindex="4" />

					</div>
                                    
                                        <div id="separator">
						<label for="site_email">Twitter Url : </label> <input
							name="txtTwitterUrl" type="text" id="txtTwitterUrl"
							value="<?php print($twitter_url); ?>" size="64" maxlength="64" tabindex="4" />

					</div>
                                    
                                        <div id="separator">
						<label for="site_email">LinkedIn Url : </label> <input
							name="txtLinkedIn" type="text" id="txtLinkedIn"
							value="<?php print($linked_in); ?>" size="64" maxlength="64" tabindex="4" />

					</div>
                                    
                                        <div id="separator">
						<label for="site_email">Skype : </label> <input
							name="txtSkypeUrl" type="text" id="txtSkypeUrl"
							value="<?php print($skype_url); ?>" size="64" maxlength="64" tabindex="4" />

					</div>
                                    
                                        <div id="separator">
						<label for="site_email">Rss Url : </label> <input
							name="txtRssUrl" type="text" id="txtRssUrl"
							value="<?php print($rss_url); ?>" size="64" maxlength="64" tabindex="4" />

					</div>
                            
                            
                            </fieldset>
                        
                        
                        </div>   
                                
                                
                                
                                
                                <h2>API Keys</h2>
                <div>   
                    <fieldset>

                       <h2>Facebook</h2>

                           <div id="separator">

                            <label for="pagetitle">App ID : </label>
                            <input name="txtFacebookAppId" type="text" id="txtFacebookAppId" tabindex="7" size="64" style="width:600px;" value="<?php print($facebook_app_id);?>"/>
                        </div>



                          <div id="separator">

                            <label for="keywords">Secret key : </label> 
                            <input name="txtFacebookSecretkey" type="text" id="txtFacebookSecretkey" tabindex="8" style="width:600px;" value="<?php print($facebook_secret_key);?>"/>
                        </div>


 <h2>Twitter</h2>
                        <div id="separator">

                            <label for="pagetitle">Consumer key : </label>
                            <input name="txtTwitterConsumerkey" type="text" id="txtTwitterConsumerkey" tabindex="9" size="64" style="width:600px;" value="<?php print($consumer_key);?>"/>
                        </div>



                          <div id="separator">

                            <label for="keywords">Consumer secret : </label> 
                            <input name="txtTwitterConsumersecret" type="text" id="txtTwitterConsumersecret" tabindex="10" style="width:600px;" value="<?php print($consumer_secret);?>"/>
                        </div>

<div id="separator">

                            <label for="pagetitle">App Access token : </label>
                            <input name="txtTwitterAppAccesstoken" type="text" id="txtTwitterAppAccesstoken" tabindex="9" size="64" style="width:600px;" value="<?php print($user_token);?>"/>
                        </div>



                          <div id="separator">

                            <label for="keywords">App token secret : </label> 
                            <input name="txtTwitterAppAccesstokensecret" type="text" id="txtTwitterAppAccesstokensecret" tabindex="10" style="width:600px;" value="<?php print($user_secret);?>"/>
                        </div>


                         

                    </fieldset>



                </div>
                                    
</div>
					  <input class="testbutton"  id="testbutton" type="submit" value="submit" name="Submit"  onClick="sendRequest()"/> 
  <input id="resetbutton" type="reset"  />
<div style="clear:both;margin-top:10px;"></div>
				</p>
				<div id="show"></div>
		</div>
		</form>

	</div>








</div>
