<?php
class mysqlrestore
{
    var $dumpFile = "" ;
    var $queries = array( ) ;
     
    /**
        Load the SQL dumpfile created by mysqldump
    **/
    function loadDumpFile( $dumpFile )
    {
        if( !file_exists( $dumpFile ) ){
            return false ;
        }
         
        $this->dumpFile = $dumpFile ;
         
        return true ;      
    } // END
     
     
    /**
        Parse the dump file and put all valid
        SQL queries in an array
    **/
    function parseQueries( )
    {
        if( !file_exists( $this->dumpFile ) ){
            return false ;
        }
         
        $lines = file( $this->dumpFile ) ;
        $templine = "" ;               
         
        foreach( $lines as $line ) {
             
            if ( substr( $line, 0, 2 ) == '--' ||
                        $line == '' ||
                        substr( $line, 0, 2 ) == '/*') {
                continue;
            }
                 
            $templine .= $line ;
             
            if ( substr( trim( $line ), -1, 1 ) == ';' ) {
                $this->queries[] = trim( $templine ) ;
                $templine = "" ;
            }
        }      
         
        return true ;
    } // END
     
     
    /**
        Make a connection to the MySQL database
    **/
    function connect( $host, $user, $pword, $db )
    {
        mysql_connect($host, $user, $pword)
        or die('Error connecting to MySQL server: ' . mysql_error());      
         
        mysql_select_db($db)
        or die('Error selecting MySQL database: ' . mysql_error());        
    }// END
         
     
    /**
        Iterate through the parsed queries and run them.
        For script processing timeout limitations, you can
        run blocks of queries at a time. Set the start and end
        pointers to the block of queries in the array you wish
        to run.
    **/
    function runQueries( $startQuery = 0, $endQuery = 0 )
    {
        if( count( $this->queries ) < 1 ) {
            return "ERROR: No queries to run!" ;
        }
         
        if( $endQuery == 0 )
            $endQuery = count( $this->queries ) ;
         
        if( $endQuery > count( $this->queries ) )
            $endQuery = count( $this->queries ) ;
         
        for( $i = $startQuery ; $i < $endQuery ; $i ++ )
        {
            mysql_query( $this->queries[ $i ] )
                or print('Error performing query \'' . $templine . '\': ' . mysql_error() . '<br><br>');
        }
         
        return true ;      
    }// END
}

?>