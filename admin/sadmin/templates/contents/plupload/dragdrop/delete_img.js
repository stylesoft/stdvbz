// JavaScript Document



jQuery(document).ready(function($){
		 
 $("#carnamelist").sortable({
 connectWith: '#cardeleteArea',
 update: function(event, ui){
 //Run this code whenever an item is dragged and dropped out of this list
 var order = $(this).sortable('serialize') + '&action=updateRecordsListings';

 $.ajax({
 url: 'modules/car_sales/updateDB.php',
 type: 'POST',
 data: order
 });
 }
 });

 $("#cardeleteArea").droppable({
 accept: '#carnamelist > li',
 hoverClass: 'dropAreaHover',
 drop: function(event, ui) {
 deleteImage(ui.draggable,ui.helper);
 },
 activeClass: 'dropAreaHover'
 });

 function deleteImage($draggable,$helper){
 var answer = confirm('Permantly delete this item?');
 if (answer == true)
    {
 params = 'PID=' + $draggable.attr('id');
 $.ajax({
 url: 'modules/car_sales/deleteImage.php',
 type: 'POST',
 data: params
 });
 $helper.effect('transfer', { to: '#cardeleteArea', className: 'ui-effects-transfer' },500);
 $draggable.remove();
 }

 
 else {
	 
 }

 }

});