// JavaScript Document
$(document).ready(function(){ 
					   
	$(function() {
            
                // update image order...........
		$("#carimagesDiv ul").sortable({ opacity: 0.6, cursor: 'move', update: function() {
			var order = $(this).sortable("serialize") + '&action=updateRecordsListings'; 
			$.post("includes/updateImageOrder.php", order, function(theResponse){
				$("#carcontentRight").html(theResponse);
			}); 															 
		}								  
		});
                
                // delete image..................
                 $("#cardeleteArea").droppable({
                    accept: '#carnamelist > li',
                    hoverClass: 'dropAreaHover',
                    drop: function(event, ui) {
                    deleteImage(ui.draggable,ui.helper);
                    },
                    activeClass: 'dropAreaHover'
                    });

                    function deleteImage($draggable,$helper){
                    var answer = confirm('Permantly delete this item?');
                    if (answer == true)
                        {
                    params = 'PID=' + $draggable.attr('id');
                    $.ajax({
                    url: 'includes/deleteImage.php',
                    type: 'POST',
                    data: params
                    });
                    $helper.effect('transfer', { to: '#cardeleteArea', className: 'ui-effects-transfer' },500);
                    $draggable.remove();
                    }


                    else {

                    }

                    }
	});

});	